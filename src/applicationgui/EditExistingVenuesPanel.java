package applicationgui;

import AdministrationApplication.Utilities.HTTPRequestHandler;
import AdministrationApplication.Utilities.Validator;
import AdministrationApplication.Venue;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */

public class EditExistingVenuesPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private Venue selectedVenue;
    private HTTPRequestHandler httpHandler;
    private final String loginData;
    private Validator validator;

    public EditExistingVenuesPanel(CardLayout layoutCard, JPanel panel, Venue venue, String data) {
        selectedVenue = venue;
        cardLayout = layoutCard;
        mainPanel = panel;
        initComponents();
        httpHandler = new HTTPRequestHandler();
        fillExistingVenueFields();
        loginData = data;
        validator = new Validator();
    }

    private void fillExistingVenueFields() {
        txtID.setText(String.valueOf(selectedVenue.getVenueID()));
        txtVenueName.setText(selectedVenue.getVenueName());
        txtVenueAddressLine1.setText(selectedVenue.getVenueAddress().getAddressLine1());
        txtVenueAddressLine2.setText(selectedVenue.getVenueAddress().getAddressLine2());
        txtVenueCity.setText(selectedVenue.getVenueAddress().getCity());
        txtVenueCounty.setText(selectedVenue.getVenueAddress().getCounty());
        txtVenuePostcode.setText(selectedVenue.getVenueAddress().getPostcode());
    }

    private String constructActJsonString(Venue newVenue) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("ID", newVenue.getVenueID());
        j.put("NAME", newVenue.getVenueName());
        j.put("ADDRESS_LINE_1", newVenue.getVenueAddress().getAddressLine1());
        j.put("ADDRESS_LINE_2", newVenue.getVenueAddress().getAddressLine2());
        j.put("CITY", newVenue.getVenueAddress().getCity());
        j.put("COUNTY", newVenue.getVenueAddress().getCounty());
        j.put("POSTCODE", newVenue.getVenueAddress().getPostcode());
        return j.toString();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlVenueDetail = new javax.swing.JPanel();
        lblVenueName = new javax.swing.JLabel();
        txtVenueName = new javax.swing.JTextField();
        lblAddressLine3 = new javax.swing.JLabel();
        txtVenueAddressLine1 = new javax.swing.JTextField();
        lblAddressLine4 = new javax.swing.JLabel();
        txtVenueAddressLine2 = new javax.swing.JTextField();
        lblCity1 = new javax.swing.JLabel();
        txtVenueCity = new javax.swing.JTextField();
        lblCounty1 = new javax.swing.JLabel();
        txtVenueCounty = new javax.swing.JTextField();
        lblPostcode = new javax.swing.JLabel();
        txtVenuePostcode = new javax.swing.JTextField();
        lblID = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        lblAddNewVenue1 = new javax.swing.JLabel();
        btnUpdate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        lblVenueName.setText("Venue Name:");

        lblAddressLine3.setText("Address Line 1:");

        lblAddressLine4.setText("Address Line 2:");

        lblCity1.setText("City:");

        lblCounty1.setText("County:");

        lblPostcode.setText("Postcode:");

        lblID.setText("ID:");

        txtID.setEditable(false);

        javax.swing.GroupLayout pnlVenueDetailLayout = new javax.swing.GroupLayout(pnlVenueDetail);
        pnlVenueDetail.setLayout(pnlVenueDetailLayout);
        pnlVenueDetailLayout.setHorizontalGroup(
            pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVenueDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblID)
                    .addComponent(lblPostcode)
                    .addComponent(lblCounty1)
                    .addComponent(lblCity1)
                    .addComponent(lblAddressLine4)
                    .addComponent(lblAddressLine3)
                    .addComponent(lblVenueName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtVenueName, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                    .addComponent(txtVenueAddressLine1)
                    .addComponent(txtVenueAddressLine2)
                    .addComponent(txtVenueCity)
                    .addComponent(txtVenueCounty)
                    .addComponent(txtVenuePostcode, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlVenueDetailLayout.setVerticalGroup(
            pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVenueDetailLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblID)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblVenueName)
                    .addComponent(txtVenueName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAddressLine3)
                    .addComponent(txtVenueAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAddressLine4)
                    .addComponent(txtVenueAddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCity1)
                    .addComponent(txtVenueCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtVenueCounty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCounty1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlVenueDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPostcode)
                    .addComponent(txtVenuePostcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        lblAddNewVenue1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblAddNewVenue1.setText("Edit Existing Venue");

        btnUpdate.setText("Update Venue");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblAddNewVenue1)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(24, 24, 24)
                            .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(56, 56, 56)
                            .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(4, 4, 4)
                            .addComponent(pnlVenueDetail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(294, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddNewVenue1)
                .addGap(35, 35, 35)
                .addComponent(pnlVenueDetail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate)
                    .addComponent(btnCancel))
                .addContainerGap(107, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        if (!txtVenueName.getText().isEmpty()) {
            if (!txtVenueAddressLine1.getText().isEmpty()) {
                if (!txtVenueCity.getText().isEmpty()) {
                    if (!txtVenueCounty.getText().isEmpty()) {
                        if (!txtVenuePostcode.getText().isEmpty()) {
                            if (validator.validateUKPostcode(txtVenuePostcode.getText()) == true) {
                                try {
                                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                                        selectedVenue.setVenueName(txtVenueName.getText());
                                        selectedVenue.getVenueAddress().setAddressLine1(txtVenueAddressLine1.getText());
                                        selectedVenue.getVenueAddress().setAddressLine2(txtVenueAddressLine2.getText());
                                        selectedVenue.getVenueAddress().setCity(txtVenueCity.getText());
                                        selectedVenue.getVenueAddress().setCounty(txtVenueCounty.getText());
                                        selectedVenue.getVenueAddress().setPostcode(txtVenuePostcode.getText().replaceAll(" ", "").toUpperCase());
                                        String data = constructActJsonString(selectedVenue);
                                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/Venues/" + selectedVenue.getVenueID());
                                        String httpInfo = httpHandler.sendPUTRequest(data, url);
                                        JOptionPane.showMessageDialog(this, httpInfo + " Venue successfully updated");
                                        cardLayout.removeLayoutComponent(this);
                                        mainPanel.add("ViewExistingVenues", new ViewExistingVenuesPanel(cardLayout, mainPanel, loginData));
                                        cardLayout.show(mainPanel, "ViewExistingVenues");
                                    } else {
                                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                                        mainPanel.removeAll();
                                        new LogIn().setVisible(true);
                                    }
                                } catch (MalformedURLException ex) {
                                    JOptionPane.showMessageDialog(this, "ERROR Venue NOT Updated \n " + ex);
                                } catch (IOException ex) {
                                    JOptionPane.showMessageDialog(this, "ERROR Venue NOT Updated \n " + ex);
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Please enter a valid UK postcode");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Please provide the Postcode of Venue");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Please provide the County Name of Venue");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please provide the City Name of Venue");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please provide the 1st Address Line of Venue");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please provide the Venue Name.");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel these changes?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            try {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingVenues", new ViewExistingVenuesPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingVenues");
            } catch (IOException | JSONException ex) {
                JOptionPane.showMessageDialog(this, " ERROR \n" + ex);
            }
        }
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel lblAddNewVenue1;
    private javax.swing.JLabel lblAddressLine3;
    private javax.swing.JLabel lblAddressLine4;
    private javax.swing.JLabel lblCity1;
    private javax.swing.JLabel lblCounty1;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblPostcode;
    private javax.swing.JLabel lblVenueName;
    private javax.swing.JPanel pnlVenueDetail;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtVenueAddressLine1;
    private javax.swing.JTextField txtVenueAddressLine2;
    private javax.swing.JTextField txtVenueCity;
    private javax.swing.JTextField txtVenueCounty;
    private javax.swing.JTextField txtVenueName;
    private javax.swing.JTextField txtVenuePostcode;
    // End of variables declaration//GEN-END:variables
}
