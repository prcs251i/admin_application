package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Act;
import AdministrationApplication.Event;
import AdministrationApplication.EventRun;
import AdministrationApplication.Price;
import AdministrationApplication.Tier;
import AdministrationApplication.Venue;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class AdditionalRunsPanel extends javax.swing.JPanel {

    /**
     * Creates new form AdditionalRunsPanel
     */
    private final DefaultComboBoxModel startTimeHoursModel;
    private final DefaultComboBoxModel startTimeMinutesModel;
    private DefaultListModel performersListModel;
    private DefaultComboBoxModel venuesComboModel;
    private JsonClassGenerator generator;
    private HashMap<Integer, Act> performersHash;
    private HashMap<Integer, Venue> venuesHash;
    private List<Act> selectedPerformers;
    private List<Act> performersList;
    private List<Act> filteredList;
    private Act selectedPerformer;
    private Venue selectedVenue;
    private List<JPanel> pricePanels;

    public AdditionalRunsPanel(int run, int totalRuns) throws IOException, JSONException {
        startTimeHoursModel = new DefaultComboBoxModel();
        startTimeMinutesModel = new DefaultComboBoxModel();
        initialiseComboBoxModels();
        performersListModel = new DefaultListModel();
        venuesComboModel = new DefaultComboBoxModel();
        initComponents();
        selectedPerformers = new ArrayList();
        filteredList = new ArrayList();
        performersHash = new HashMap<>();
        venuesHash = new HashMap<>();
        generator = new JsonClassGenerator();
        initialisePerformersList();
        fillVenuesComboBox();
        lblRunX.setText("Run: " + run);
        pricePanels = new ArrayList();
        pricePanels.add(pnlTier1);
        pricePanels.add(pnlTier2);
        pricePanels.add(pnlTier3);
        pricePanels.add(pnlTier4);
        pricePanels.add(pnlTier5);
        pricePanels.add(pnlTier6);
        pricePanels.add(pnlTier7);
        pricePanels.add(pnlTier8);
        pricePanels.add(pnlTier9);
        disablePrices();
        txtAdditionalPerformers.setLineWrap(true);
        cbxEventVenue.setSelectedIndex(1);
        cbxEventVenue.setSelectedIndex(0);
    }

    private void disablePrices() {
        for (JPanel panel : pricePanels) {
            panel.setVisible(false);
        }
    }

    private void initialisePerformersList() throws IOException {
        ListSelectionModel runsSelectionModel = lstAdditionalPerformers.getSelectionModel();
        runsSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (lstAdditionalPerformers.getSelectedIndex() != -1) {
                        String performer = lstAdditionalPerformers.getSelectedValue().toString();
                        String performerId = performer.split(" - ", 2)[0];
                        int id = Integer.parseInt(performerId);
                        selectedPerformer = performersHash.get(id);
                    }
                }
            }
        });
        performersList = generator.generateActs();
        int count = 0;
        for (Act performer : performersList) {
            performersListModel.add(count, performer.getID() + " - " + performer.getActName());
            performersHash.put(performer.getID(), performer);
            count++;
        }
    }

    private void filterPerformersList(List<Act> performersList) {
        performersListModel.clear();
        int count = 0;
        for (Act performer : performersList) {
            performersListModel.add(count, performer.getID() + " - " + performer.getActName());
            count++;
        }
    }

    private void fillVenuesComboBox() throws IOException, JSONException {
        List<Venue> venuesList = generator.generateVenues();
        for (Venue venue : venuesList) {
            venuesComboModel.addElement(venue.getVenueID() + " - " + venue.getVenueName());
            venuesHash.put(venue.getVenueID(), venue);
            cbxEventVenue.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    disablePrices();
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        if (cbxEventVenue.getSelectedIndex() != -1) {
                            String venue = cbxEventVenue.getSelectedItem().toString();
                            String venueId = venue.split(" - ", 2)[0];
                            int id = Integer.parseInt(venueId);
                            selectedVenue = venuesHash.get(id);
                            int i = 1;
                            for (Tier tier : selectedVenue.getTierList()) {

                                pricePanels.get(i - 1).setVisible(true);
                                String seatedStanding;
                                if (tier.getIsSeated() == true) {
                                    seatedStanding = "Seated";
                                } else {
                                    seatedStanding = "Standing";
                                }
                                switch (i) {
                                    case 1:
                                        lblTier1.setText(tier.getTierName());
                                        lblSeatingStanding1.setText(seatedStanding);
                                        break;
                                    case 2:
                                        lblTier2.setText(tier.getTierName());
                                        lblSeatingStanding2.setText(seatedStanding);
                                        break;
                                    case 3:
                                        lblTier3.setText(tier.getTierName());
                                        lblSeatingStanding3.setText(seatedStanding);
                                        break;
                                    case 4:
                                        lblTier4.setText(tier.getTierName());
                                        lblSeatingStanding4.setText(seatedStanding);
                                        break;
                                    case 5:
                                        lblTier5.setText(tier.getTierName());
                                        lblSeatingStanding5.setText(seatedStanding);
                                        break;
                                    case 6:
                                        lblTier6.setText(tier.getTierName());
                                        lblSeatingStanding6.setText(seatedStanding);
                                        break;
                                    case 7:
                                        lblTier7.setText(tier.getTierName());
                                        lblSeatingStanding7.setText(seatedStanding);
                                        break;
                                    case 8:
                                        lblTier8.setText(tier.getTierName());
                                        lblSeatingStanding8.setText(seatedStanding);
                                        break;
                                    case 9:
                                        lblTier9.setText(tier.getTierName());
                                        lblSeatingStanding9.setText(seatedStanding);
                                        break;
                                }
                                i++;
                            }
                        }
                    }
                }
            });
        }
    }

    public List<Price> constructPrices(EventRun run) {
        int tierNo = 1;
        List<Price> priceList = new ArrayList();
        for (Tier tier : selectedVenue.getTierList()) {
            Price price = new Price();
            switch (tierNo) {
                case 1:
                    if (!txtTierPrice1.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice1.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    break;
                case 2:
                    if (!txtTierPrice2.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice2.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    break;
                case 3:
                    if (!txtTierPrice3.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice3.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    break;
                case 4:
                    if (!txtTierPrice4.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice4.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    break;
                case 5:
                    if (!txtTierPrice5.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice5.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    break;
                case 6:
                    if (!txtTierPrice6.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice6.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    break;
                case 7:
                    if (!txtTierPrice7.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice7.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    break;
                case 8:
                    if (!txtTierPrice8.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice8.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    break;
                case 9:
                    if (!txtTierPrice9.getText().isEmpty()) {
                        try {
                            price.setPrice(Double.parseDouble(txtTierPrice9.getText()));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                    + "Please edit the run from the View existing Runs Page to add");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }

            }
            price.setTierID(tier.getTierId());
            price.setId(run.getID());
            priceList.add(price);
            tierNo++;
        }
        return priceList;
    }

    public String[] constructPriceJsonStrings(EventRun run, List<Price> prices) {
        String[] priceJsonStrings = new String[prices.size()];
        if (run != null) {
            int count = 0;
            for (Price price : prices) {
                JSONObject j = new JSONObject();
                j.put("RUN_ID", run.getID());
                j.put("TIER_ID", price.getTierID());
                j.put("PRICE1", price.getPrice());
                priceJsonStrings[count] = j.toString();
                count++;
            }
        } else {
            return null;
        }
        return priceJsonStrings;
    }

    public EventRun constructEventRun() throws ParseException {
        EventRun run = null;
        if (cbxEventVenue.getSelectedIndex() != -1) {
            if (dtpRunDate.getDate() != null) {
                if (cbxStartTimeHours.getSelectedIndex() != -1 && cbxStartTimeMinutes.getSelectedIndex() != -1) {
                    run = new EventRun();
                    String venueString = cbxEventVenue.getSelectedItem().toString();
                    int venueId = Integer.parseInt(venueString.split(" - ", 2)[0]);
                    Venue venue = venuesHash.get(venueId);
                    run.setVenue(venue);
                    Date dateTime = constructDate();
                    run.setRunDate(dateTime);
                    run.setDuration((int) spnDuration.getValue());
                    run.setActsPerforming(selectedPerformers);
                } else {
                    JOptionPane.showMessageDialog(this, "Please select a start Time for the new Run");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please select a Date for the new Run");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Venue for the new Run");
        }
        return run;
    }

    public String constructRunJsonString(EventRun newRun, Event newEvent) throws ParseException, JSONException {
        if (newRun != null) {
            JSONObject j = new JSONObject();
            j.put("EVENT_ID", newEvent.getID());
            j.put("VENUE_ID", newRun.getVenue().getVenueID());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String formattedDate = formatter.format(newRun.getRunDate());
            j.put("RUN_DATE", formattedDate);
            j.put("DURATION", newRun.getDuration());
            return j.toString();
        } else {
            return null;
        }
    }

    public String[] constructPerformanceJsonStrings(EventRun newRun) {
        String[] performancesData = new String[selectedPerformers.size()];
        int i = 0;
        for (Act performer : selectedPerformers) {
            JSONObject j = new JSONObject();
            j.put("ACT_ID", performer.getID());
            j.put("RUN_ID", newRun.getID());
            performancesData[i] = j.toString();
            i++;
        }
        return performancesData;
    }

    private Date constructDate() throws ParseException {
        String date = dtpRunDate.getDate().toString();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dtpRunDate.setFormats(dateFormat);
        DateFormat sysDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date_to_store = sysDate.format(dtpRunDate.getDate());
        Date dateTime = sysDate.parse(date_to_store);
        dateTime.setHours(Integer.parseInt(cbxStartTimeHours.getSelectedItem().toString()));
        dateTime.setMinutes(Integer.parseInt(cbxStartTimeMinutes.getSelectedItem().toString()));
        sysDate.format(dateTime);
        return dateTime;
    }

    private void initialiseComboBoxModels() {
        LocalDateTime now = LocalDateTime.now();
        for (int i = 0; i < 24; i++) {
            Integer.toString(i);
            startTimeHoursModel.addElement(String.format("%1$02d", i));
        }
        for (int i = 0; i < 60; i++) {
            Integer.toString(i);
            startTimeMinutesModel.addElement(String.format("%1$02d", i));
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlAdditionalRun = new javax.swing.JPanel();
        lblEventDate1 = new javax.swing.JLabel();
        lblStartTime1 = new javax.swing.JLabel();
        cbxEventVenue = new javax.swing.JComboBox();
        lvlEventVenue1 = new javax.swing.JLabel();
        lblRunX = new javax.swing.JLabel();
        cbxStartTimeHours = new javax.swing.JComboBox();
        cbxStartTimeMinutes = new javax.swing.JComboBox();
        lblRunInfo = new javax.swing.JLabel();
        dtpRunDate = new org.jdesktop.swingx.JXDatePicker();
        lblDuration = new javax.swing.JLabel();
        spnDuration = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        pnlPerformers = new javax.swing.JPanel();
        pnlSearchFilter = new javax.swing.JPanel();
        lblSearch = new javax.swing.JLabel();
        txtSearchPerformers = new javax.swing.JTextField();
        cbxCriteria = new javax.swing.JComboBox();
        lblPerormerOverview = new javax.swing.JLabel();
        scpPerformerOverview = new javax.swing.JScrollPane();
        txtAdditionalPerformers = new javax.swing.JTextArea();
        scpAdditionalPerformers = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstAdditionalPerformers = new javax.swing.JList();
        btnRemovePerformer = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnResetSearch = new javax.swing.JButton();
        lblPerformers = new javax.swing.JLabel();
        btnAddPerformer = new javax.swing.JButton();
        pnlTier9 = new javax.swing.JPanel();
        lblSeatingStanding9 = new javax.swing.JLabel();
        txtTierPrice9 = new javax.swing.JTextField();
        lbl£10 = new javax.swing.JLabel();
        lblTier9 = new javax.swing.JLabel();
        pnlTier3 = new javax.swing.JPanel();
        lblSeatingStanding3 = new javax.swing.JLabel();
        txtTierPrice3 = new javax.swing.JTextField();
        lbl£5 = new javax.swing.JLabel();
        lblTier3 = new javax.swing.JLabel();
        pnlTier7 = new javax.swing.JPanel();
        lblSeatingStanding7 = new javax.swing.JLabel();
        txtTierPrice7 = new javax.swing.JTextField();
        lbl£9 = new javax.swing.JLabel();
        lblTier7 = new javax.swing.JLabel();
        pnlTier6 = new javax.swing.JPanel();
        lblSeatingStanding6 = new javax.swing.JLabel();
        txtTierPrice6 = new javax.swing.JTextField();
        lbl£8 = new javax.swing.JLabel();
        lblTier6 = new javax.swing.JLabel();
        pnlTier1 = new javax.swing.JPanel();
        lblSeatingStanding1 = new javax.swing.JLabel();
        txtTierPrice1 = new javax.swing.JTextField();
        lbl£ = new javax.swing.JLabel();
        lblTier1 = new javax.swing.JLabel();
        pnlTier5 = new javax.swing.JPanel();
        lblSeatingStanding5 = new javax.swing.JLabel();
        txtTierPrice5 = new javax.swing.JTextField();
        lbl£7 = new javax.swing.JLabel();
        lblTier5 = new javax.swing.JLabel();
        pnlTier4 = new javax.swing.JPanel();
        lblSeatingStanding4 = new javax.swing.JLabel();
        txtTierPrice4 = new javax.swing.JTextField();
        lbl£6 = new javax.swing.JLabel();
        lblTier4 = new javax.swing.JLabel();
        pnlTier8 = new javax.swing.JPanel();
        lblSeatingStanding8 = new javax.swing.JLabel();
        txtTierPrice8 = new javax.swing.JTextField();
        lbl£11 = new javax.swing.JLabel();
        lblTier8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        pnlTier2 = new javax.swing.JPanel();
        lblSeatingStanding2 = new javax.swing.JLabel();
        txtTierPrice2 = new javax.swing.JTextField();
        lbl£4 = new javax.swing.JLabel();
        lblTier2 = new javax.swing.JLabel();

        lblEventDate1.setText("Date:");

        lblStartTime1.setText("Start Time:");

        cbxEventVenue.setModel(venuesComboModel);

        lvlEventVenue1.setText("Venue:");

        lblRunX.setText("Run X");

        cbxStartTimeHours.setModel(startTimeHoursModel);

        cbxStartTimeMinutes.setModel(startTimeMinutesModel);

        lblRunInfo.setText("Run Information");

        lblDuration.setText("Duration :");

        spnDuration.setModel(new javax.swing.SpinnerNumberModel(15, 15, 4320, 15));

        jLabel2.setText("mins");

        javax.swing.GroupLayout pnlAdditionalRunLayout = new javax.swing.GroupLayout(pnlAdditionalRun);
        pnlAdditionalRun.setLayout(pnlAdditionalRunLayout);
        pnlAdditionalRunLayout.setHorizontalGroup(
            pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblRunX)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                        .addGap(127, 127, 127)
                        .addComponent(lblRunInfo))
                    .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                        .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblDuration)
                            .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                    .addGap(32, 32, 32)
                                    .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblEventDate1, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lvlEventVenue1, javax.swing.GroupLayout.Alignment.TRAILING)))
                                .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(lblStartTime1))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                .addComponent(spnDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2))
                            .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                    .addComponent(cbxStartTimeHours, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cbxStartTimeMinutes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addComponent(dtpRunDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cbxEventVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlAdditionalRunLayout.setVerticalGroup(
            pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAdditionalRunLayout.createSequentialGroup()
                .addComponent(lblRunX)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblRunInfo)
                .addGap(18, 18, 18)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lvlEventVenue1)
                    .addComponent(cbxEventVenue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEventDate1)
                    .addComponent(dtpRunDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxStartTimeHours, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxStartTimeMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStartTime1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDuration)
                    .addComponent(spnDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap())
        );

        lblSearch.setText("Search:");

        cbxCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "Title", "Genre" }));

        lblPerormerOverview.setText("Performer Overview: ");

        txtAdditionalPerformers.setEditable(false);
        txtAdditionalPerformers.setBackground(new java.awt.Color(245, 245, 245));
        txtAdditionalPerformers.setColumns(20);
        txtAdditionalPerformers.setRows(5);
        txtAdditionalPerformers.setAutoscrolls(false);
        scpPerformerOverview.setViewportView(txtAdditionalPerformers);

        lstAdditionalPerformers.setForeground(new java.awt.Color(255, 0, 0));
        lstAdditionalPerformers.setModel(performersListModel);
        lstAdditionalPerformers.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstAdditionalPerformersValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstAdditionalPerformers);

        scpAdditionalPerformers.setViewportView(jScrollPane1);

        btnRemovePerformer.setText("Remove Performer");
        btnRemovePerformer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemovePerformerActionPerformed(evt);
            }
        });

        btnReset.setText("Reset");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnResetSearch.setText("Reset");
        btnResetSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetSearchActionPerformed(evt);
            }
        });

        lblPerformers.setText("Performers");

        btnAddPerformer.setText("Add Performer");
        btnAddPerformer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddPerformerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlSearchFilterLayout = new javax.swing.GroupLayout(pnlSearchFilter);
        pnlSearchFilter.setLayout(pnlSearchFilterLayout);
        pnlSearchFilterLayout.setHorizontalGroup(
            pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                                .addComponent(lblSearch)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtSearchPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSearchFilterLayout.createSequentialGroup()
                                .addComponent(lblPerformers)
                                .addGap(131, 131, 131)))
                        .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnResetSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                        .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlSearchFilterLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(scpAdditionalPerformers))
                                .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                                    .addComponent(btnAddPerformer, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnRemovePerformer, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                                .addGap(60, 60, 60)
                                .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(scpPerformerOverview, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 27, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                .addGap(373, 373, 373)
                .addComponent(lblPerormerOverview)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlSearchFilterLayout.setVerticalGroup(
            pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnResetSearch))
                    .addComponent(lblPerformers))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSearch)
                    .addComponent(txtSearchPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPerormerOverview, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scpPerformerOverview, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlSearchFilterLayout.createSequentialGroup()
                        .addComponent(scpAdditionalPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlSearchFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAddPerformer)
                            .addComponent(btnRemovePerformer))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnReset))))
        );

        javax.swing.GroupLayout pnlPerformersLayout = new javax.swing.GroupLayout(pnlPerformers);
        pnlPerformers.setLayout(pnlPerformersLayout);
        pnlPerformersLayout.setHorizontalGroup(
            pnlPerformersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPerformersLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlSearchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlPerformersLayout.setVerticalGroup(
            pnlPerformersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPerformersLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(pnlSearchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        lblSeatingStanding9.setText("Seating/Standing");

        lbl£10.setText("£");

        lblTier9.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier9Layout = new javax.swing.GroupLayout(pnlTier9);
        pnlTier9.setLayout(pnlTier9Layout);
        pnlTier9Layout.setHorizontalGroup(
            pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier9Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(lblTier9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice9, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding9)
                .addContainerGap())
        );
        pnlTier9Layout.setVerticalGroup(
            pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier9)
                    .addComponent(lbl£10)
                    .addComponent(lblSeatingStanding9)))
        );

        lblSeatingStanding3.setText("Seating/Standing");

        lbl£5.setText("£");

        lblTier3.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier3Layout = new javax.swing.GroupLayout(pnlTier3);
        pnlTier3.setLayout(pnlTier3Layout);
        pnlTier3Layout.setHorizontalGroup(
            pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier3Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(lblTier3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding3)
                .addContainerGap())
        );
        pnlTier3Layout.setVerticalGroup(
            pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier3)
                    .addComponent(lbl£5)
                    .addComponent(lblSeatingStanding3)))
        );

        lblSeatingStanding7.setText("Seating/Standing");

        lbl£9.setText("£");

        lblTier7.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier7Layout = new javax.swing.GroupLayout(pnlTier7);
        pnlTier7.setLayout(pnlTier7Layout);
        pnlTier7Layout.setHorizontalGroup(
            pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier7Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(lblTier7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice7, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding7)
                .addContainerGap())
        );
        pnlTier7Layout.setVerticalGroup(
            pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier7)
                    .addComponent(lbl£9)
                    .addComponent(lblSeatingStanding7)))
        );

        lblSeatingStanding6.setText("Seating/Standing");

        lbl£8.setText("£");

        lblTier6.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier6Layout = new javax.swing.GroupLayout(pnlTier6);
        pnlTier6.setLayout(pnlTier6Layout);
        pnlTier6Layout.setHorizontalGroup(
            pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier6Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(lblTier6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice6, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding6)
                .addContainerGap())
        );
        pnlTier6Layout.setVerticalGroup(
            pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier6)
                    .addComponent(lbl£8)
                    .addComponent(lblSeatingStanding6)))
        );

        lblSeatingStanding1.setText("Seating/Standing");

        lbl£.setText("£");

        lblTier1.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier1Layout = new javax.swing.GroupLayout(pnlTier1);
        pnlTier1.setLayout(pnlTier1Layout);
        pnlTier1Layout.setHorizontalGroup(
            pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier1Layout.createSequentialGroup()
                .addContainerGap(71, Short.MAX_VALUE)
                .addComponent(lblTier1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding1))
        );
        pnlTier1Layout.setVerticalGroup(
            pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl£)
                    .addComponent(lblSeatingStanding1)
                    .addComponent(lblTier1)))
        );

        lblSeatingStanding5.setText("Seating/Standing");

        lbl£7.setText("£");

        lblTier5.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier5Layout = new javax.swing.GroupLayout(pnlTier5);
        pnlTier5.setLayout(pnlTier5Layout);
        pnlTier5Layout.setHorizontalGroup(
            pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier5Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(lblTier5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice5, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding5)
                .addContainerGap())
        );
        pnlTier5Layout.setVerticalGroup(
            pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier5)
                    .addComponent(lbl£7)
                    .addComponent(lblSeatingStanding5)))
        );

        lblSeatingStanding4.setText("Seating/Standing");

        lbl£6.setText("£");

        lblTier4.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier4Layout = new javax.swing.GroupLayout(pnlTier4);
        pnlTier4.setLayout(pnlTier4Layout);
        pnlTier4Layout.setHorizontalGroup(
            pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier4Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(lblTier4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice4, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding4)
                .addContainerGap())
        );
        pnlTier4Layout.setVerticalGroup(
            pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier4)
                    .addComponent(lbl£6)
                    .addComponent(lblSeatingStanding4)))
        );

        lblSeatingStanding8.setText("Seating/Standing");

        lbl£11.setText("£");

        lblTier8.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier8Layout = new javax.swing.GroupLayout(pnlTier8);
        pnlTier8.setLayout(pnlTier8Layout);
        pnlTier8Layout.setHorizontalGroup(
            pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier8Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(lblTier8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice8, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding8)
                .addContainerGap())
        );
        pnlTier8Layout.setVerticalGroup(
            pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier8)
                    .addComponent(lbl£11)
                    .addComponent(lblSeatingStanding8)))
        );

        jLabel1.setText("Pricing");

        lblSeatingStanding2.setText("Seating/Standing");

        lbl£4.setText("£");

        lblTier2.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier2Layout = new javax.swing.GroupLayout(pnlTier2);
        pnlTier2.setLayout(pnlTier2Layout);
        pnlTier2Layout.setHorizontalGroup(
            pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier2Layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addComponent(lblTier2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl£4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlTier2Layout.setVerticalGroup(
            pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl£4)
                    .addComponent(lblSeatingStanding2)
                    .addComponent(lblTier2)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlAdditionalRun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(213, 213, 213)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(pnlTier9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTier8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTier7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTier6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTier2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTier3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTier4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTier5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTier1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(pnlTier8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pnlAdditionalRun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lstAdditionalPerformersValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstAdditionalPerformersValueChanged

    }//GEN-LAST:event_lstAdditionalPerformersValueChanged

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        selectedPerformers.clear();
        txtAdditionalPerformers.setText("");
    }//GEN-LAST:event_btnResetActionPerformed

    private void btnAddPerformerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddPerformerActionPerformed
        if (selectedPerformer != null) {
            if (!selectedPerformers.contains(selectedPerformer)) {
                selectedPerformers.add(selectedPerformer);
                txtAdditionalPerformers.append(selectedPerformer.getActName() + ", ");
            } else {
                JOptionPane.showMessageDialog(this, "This Performer has already been added to this run!",
                        "", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnAddPerformerActionPerformed

    private void btnRemovePerformerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemovePerformerActionPerformed
        if (selectedPerformer != null) {
            if (selectedPerformers.contains(selectedPerformer)) {
                selectedPerformers.remove(selectedPerformer);
                txtAdditionalPerformers.setText("");
                for (Act performer : selectedPerformers) {
                    txtAdditionalPerformers.append(performer.getActName() + ", ");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Cannot remove a Performer not already on this run!",
                        "", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnRemovePerformerActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        filteredList.clear();
        String searchCriteria = txtSearchPerformers.getText().toLowerCase();
        for (Act act : performersList) {
            if (cbxCriteria.getSelectedItem().toString().equals("Title")) {
                if (act.getActName().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(act);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("ID")) {
                try {
                    if (searchCriteria.isEmpty() || act.getID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(act);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Performer ID");
                    filterPerformersList(performersList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("Genre")) {
                if (act.getActType().getType().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(act);
                }
            }
        }
        filterPerformersList(filteredList);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnResetSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetSearchActionPerformed
        filterPerformersList(performersList);
    }//GEN-LAST:event_btnResetSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddPerformer;
    private javax.swing.JButton btnRemovePerformer;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnResetSearch;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox cbxCriteria;
    private javax.swing.JComboBox cbxEventVenue;
    private javax.swing.JComboBox cbxStartTimeHours;
    private javax.swing.JComboBox cbxStartTimeMinutes;
    private org.jdesktop.swingx.JXDatePicker dtpRunDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDuration;
    private javax.swing.JLabel lblEventDate1;
    private javax.swing.JLabel lblPerformers;
    private javax.swing.JLabel lblPerormerOverview;
    private javax.swing.JLabel lblRunInfo;
    private javax.swing.JLabel lblRunX;
    private javax.swing.JLabel lblSearch;
    private javax.swing.JLabel lblSeatingStanding1;
    private javax.swing.JLabel lblSeatingStanding2;
    private javax.swing.JLabel lblSeatingStanding3;
    private javax.swing.JLabel lblSeatingStanding4;
    private javax.swing.JLabel lblSeatingStanding5;
    private javax.swing.JLabel lblSeatingStanding6;
    private javax.swing.JLabel lblSeatingStanding7;
    private javax.swing.JLabel lblSeatingStanding8;
    private javax.swing.JLabel lblSeatingStanding9;
    private javax.swing.JLabel lblStartTime1;
    private javax.swing.JLabel lblTier1;
    private javax.swing.JLabel lblTier2;
    private javax.swing.JLabel lblTier3;
    private javax.swing.JLabel lblTier4;
    private javax.swing.JLabel lblTier5;
    private javax.swing.JLabel lblTier6;
    private javax.swing.JLabel lblTier7;
    private javax.swing.JLabel lblTier8;
    private javax.swing.JLabel lblTier9;
    private javax.swing.JLabel lbl£;
    private javax.swing.JLabel lbl£10;
    private javax.swing.JLabel lbl£11;
    private javax.swing.JLabel lbl£4;
    private javax.swing.JLabel lbl£5;
    private javax.swing.JLabel lbl£6;
    private javax.swing.JLabel lbl£7;
    private javax.swing.JLabel lbl£8;
    private javax.swing.JLabel lbl£9;
    private javax.swing.JList lstAdditionalPerformers;
    private javax.swing.JLabel lvlEventVenue1;
    private javax.swing.JPanel pnlAdditionalRun;
    private javax.swing.JPanel pnlPerformers;
    private javax.swing.JPanel pnlSearchFilter;
    private javax.swing.JPanel pnlTier1;
    private javax.swing.JPanel pnlTier2;
    private javax.swing.JPanel pnlTier3;
    private javax.swing.JPanel pnlTier4;
    private javax.swing.JPanel pnlTier5;
    private javax.swing.JPanel pnlTier6;
    private javax.swing.JPanel pnlTier7;
    private javax.swing.JPanel pnlTier8;
    private javax.swing.JPanel pnlTier9;
    private javax.swing.JScrollPane scpAdditionalPerformers;
    private javax.swing.JScrollPane scpPerformerOverview;
    private javax.swing.JSpinner spnDuration;
    private javax.swing.JTextArea txtAdditionalPerformers;
    private javax.swing.JTextField txtSearchPerformers;
    private javax.swing.JTextField txtTierPrice1;
    private javax.swing.JTextField txtTierPrice2;
    private javax.swing.JTextField txtTierPrice3;
    private javax.swing.JTextField txtTierPrice4;
    private javax.swing.JTextField txtTierPrice5;
    private javax.swing.JTextField txtTierPrice6;
    private javax.swing.JTextField txtTierPrice7;
    private javax.swing.JTextField txtTierPrice8;
    private javax.swing.JTextField txtTierPrice9;
    // End of variables declaration//GEN-END:variables
}
