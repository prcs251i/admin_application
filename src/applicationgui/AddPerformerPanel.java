package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Act;
import AdministrationApplication.EventType;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class AddPerformerPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private DefaultComboBoxModel PerformerTypeComboBoxModel;
    private HashMap<String, EventType> eventTypesHash;
    private List<EventType> eventTypes;
    private HTTPRequestHandler httpHandler;
    private final String loginData;

    /**
     * Creates new form AddPerformerPanel
     */
    public AddPerformerPanel(CardLayout layoutCard, JPanel panel, String data) throws IOException, JSONException {
        PerformerTypeComboBoxModel = new DefaultComboBoxModel();
        initComponents();
        cardLayout = layoutCard;
        mainPanel = panel;
        eventTypesHash = new HashMap<>();
        JsonClassGenerator generator = new JsonClassGenerator();
        eventTypes = generator.generateEventTypes();
        List<EventType> eventTypes = generator.generateEventTypes();
        for (EventType eventType : eventTypes) {
            eventTypesHash.put(eventType.getType(), eventType);
        }
        httpHandler = new HTTPRequestHandler();
        initComboModel();
        loginData = data;
        txtDesc.setLineWrap(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblAddNewPerformer = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        cbxGenre = new javax.swing.JComboBox();
        lblGenre = new javax.swing.JLabel();
        btnAddPerformer = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblDescription = new javax.swing.JLabel();
        scpDesc = new javax.swing.JScrollPane();
        txtDesc = new javax.swing.JTextArea();

        lblAddNewPerformer.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblAddNewPerformer.setText("Add New Performer");

        lblName.setText("Name:");

        cbxGenre.setModel(PerformerTypeComboBoxModel);

        lblGenre.setText("Genre:");

        btnAddPerformer.setText("Add Performer");
        btnAddPerformer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddPerformerActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblDescription.setText("Description:");

        txtDesc.setColumns(20);
        txtDesc.setRows(5);
        scpDesc.setViewportView(txtDesc);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblAddNewPerformer))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lblGenre)
                                        .addComponent(lblName))
                                    .addComponent(lblDescription))
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(scpDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(cbxGenre, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(btnAddPerformer, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(136, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddNewPerformer)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblDescription)
                        .addGap(87, 87, 87)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblGenre)
                            .addComponent(cbxGenre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(scpDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddPerformer)
                    .addComponent(btnCancel))
                .addGap(66, 66, 66))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void initComboModel() {
        PerformerTypeComboBoxModel.removeAllElements();
        for (EventType type : eventTypes) {
            PerformerTypeComboBoxModel.addElement(type.getType());
        }
    }

    private void resetGUI() {
        cbxGenre.setSelectedIndex(0);
        txtName.setText("");
        txtDesc.setText("");
    }

    private String constructActJsonString(Act newAct) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("TITLE", newAct.getActName());
        j.put("DESCRIPTION", newAct.getActDesc());
        j.put("ACT_TYPE", newAct.getActType().getID());
        return j.toString();
    }

    private void btnAddPerformerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddPerformerActionPerformed
        if (!txtName.getText().isEmpty()) {
            if (!txtDesc.getText().isEmpty()) {
                if (cbxGenre.getSelectedIndex() != -1) {
                    try {
                        if (httpHandler.authenticate(loginData)[0].contains("20")) {
                            Act newAct = new Act(txtName.getText(), txtDesc.getText(), eventTypesHash.get(cbxGenre.getSelectedItem().toString()));
                            String data = constructActJsonString(newAct);
                            URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/acts");
                            String httpInfo[] = httpHandler.sendPOSTRequest(data, url);
                            if (httpInfo[0].contains("20")) {
                                int response = JOptionPane.showConfirmDialog(null, httpInfo[0] + "\n Performer was successfully added!  \n Would you like to add another?", "Addition Successful",
                                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                                if (response == JOptionPane.YES_OPTION) {
                                    resetGUI();
                                } else {
                                    cardLayout.removeLayoutComponent(this);
                                    mainPanel.add("ViewExistingPerformers", new ViewExistingPerformersPanel(cardLayout, mainPanel, loginData));
                                    cardLayout.show(mainPanel, "ViewExistingPerformers");
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, httpInfo[0] + "ERROR, Performer NOT added");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                            mainPanel.removeAll();
                            new LogIn().setVisible(true);
                        }
                    } catch (MalformedURLException ex) {
                        JOptionPane.showMessageDialog(this, "ERROR Performer NOT Added \n" + ex);
                    } catch (IOException | JSONException ex) {
                        JOptionPane.showMessageDialog(this, "ERROR Performer NOT Added \n" + ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please select a genre for the new Performer");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please enter a description for the new Performer");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please enter a name for the new Performer");
        }

    }//GEN-LAST:event_btnAddPerformerActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingPerformers", new ViewExistingPerformersPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingPerformers");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddPerformer;
    private javax.swing.JButton btnCancel;
    private javax.swing.JComboBox cbxGenre;
    private javax.swing.JLabel lblAddNewPerformer;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblGenre;
    private javax.swing.JLabel lblName;
    private javax.swing.JScrollPane scpDesc;
    private javax.swing.JTextArea txtDesc;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
