package applicationgui;

import AdministrationApplication.Booking;
import AdministrationApplication.GuestBooking;
import AdministrationApplication.Utilities.JsonClassGenerator;
import java.awt.CardLayout;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Joel Gooch
 */
public class HomePanel extends javax.swing.JPanel {

    private DefaultTableModel recentlyBooked;
    private JsonClassGenerator generator;

    public HomePanel(CardLayout layoutCard, JPanel main, String data) throws IOException {
        recentlyBooked = new DefaultTableModel();
        initComponents();
        generator = new JsonClassGenerator();
        populateHomeDashboard();
    }

    private void populateHomeDashboard() throws IOException {
        Date now = new Date();
        List<Booking> bookings = generator.generateBookings();
        List<GuestBooking> guestBookings = generator.generateGuestBookings();
        
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(new Date());

        int bookedToday = 0;
        int guestBookedToday = 0;
        double grossToday = 0.0;
        int bookedThisWeek = 0;
        int guestBookedThisWeek = 0;
        double grossThisWeek = 0.0;
        int bookedThisMonth = 0;
        int guestBookedThisMonth = 0;
        double grossThisMonth = 0.0;
        int bookedThisYear = 0;
        int guestBookedThisYear = 0;
        double grossThisYear = 0.0;
        int bookedAllTime = 0;
        int guestBookedAllTime = 0;
        double grossAllTime = 0.0;

        for (Booking booking : bookings) {
            cal2.setTime(booking.getDatePlaced());
            if (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) {
                if (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)) {
                    if (cal1.get(Calendar.WEEK_OF_MONTH) == cal2.get(Calendar.WEEK_OF_MONTH)) {
                        if (cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE)) {
                            bookedToday++;
                            grossToday += booking.getTotalCost();
                        }
                        bookedThisWeek++;
                        grossThisWeek += booking.getTotalCost();
                    }
                    bookedThisMonth++;
                    grossThisMonth += booking.getTotalCost();
                }
                bookedThisYear++;
                grossThisYear += booking.getTotalCost();
            }
            bookedAllTime++;
            grossAllTime += booking.getTotalCost();
        }
        
        for (GuestBooking booking : guestBookings) {
            cal2.setTime(booking.getDatePlaced());
            if (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) {
                if (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)) {
                    if (cal1.get(Calendar.WEEK_OF_MONTH) == cal2.get(Calendar.WEEK_OF_MONTH)) {
                        if (cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE)) {
                            guestBookedToday++;
                            grossToday += booking.getTotalCost();
                        }
                        guestBookedThisWeek++;
                        grossThisWeek += booking.getTotalCost();
                    }
                    guestBookedThisMonth++;
                    grossThisMonth += booking.getTotalCost();
                }
                guestBookedThisYear++;
                grossThisYear += booking.getTotalCost();
            }
            guestBookedAllTime++;
            grossAllTime += booking.getTotalCost();
        }

        txtBookedToday.setText(String.valueOf(bookedToday));
        txtGuestBookedToday.setText(String.valueOf(guestBookedToday));
        txtBookedThisWeek.setText(String.valueOf(bookedThisWeek));
        txtGuestBookedThisWeek.setText(String.valueOf(guestBookedThisWeek));
        txtBookedThisMonth.setText(String.valueOf(bookedThisMonth));
        txtGuestBookedThisMonth.setText(String.valueOf(guestBookedThisMonth));
        txtBookedThisYear.setText(String.valueOf(bookedThisYear));
        txtGuestBookedThisYear.setText(String.valueOf(guestBookedThisYear));
        txtBookedAllTime.setText(String.valueOf(bookedAllTime));
        txtGuestBookedAllTime.setText(String.valueOf(guestBookedAllTime));
        txtGrossToday.setText("£ " + String.valueOf(grossToday));
        txtGrossThisWeek.setText("£ " + String.valueOf(grossThisWeek));
        txtGrossThisMonth.setText("£ " + String.valueOf(grossThisMonth));
        txtGrossThisYear.setText("£ " + String.valueOf(grossThisYear));
        txtGrossAllTime.setText("£ " + String.valueOf(grossAllTime));
    }

        // number booked today
    // number booked last 7 days
    // number booked this month
    // number booked this year
    // number bookings all time
    // cost totals for all above
    //guest bookings all same as above
    // global for all above
    // maybe same stuff for tickets as above
    // how many event of each genre there is
    // how many performers of each genre there is
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblHomePage = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtBookedToday = new javax.swing.JTextField();
        txtBookedThisWeek = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtBookedThisYear = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtBookedThisMonth = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtBookedAllTime = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtGrossToday = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtGrossThisWeek = new javax.swing.JTextField();
        txtGrossThisMonth = new javax.swing.JTextField();
        txtGrossThisYear = new javax.swing.JTextField();
        txtGrossAllTime = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtGuestBookedToday = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtGuestBookedThisWeek = new javax.swing.JTextField();
        txtGuestBookedThisMonth = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtGuestBookedThisYear = new javax.swing.JTextField();
        txtGuestBookedAllTime = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();

        lblHomePage.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblHomePage.setText("Ticket Stack");

        jLabel2.setText("Booked Today:");

        jLabel3.setText("Booked This Week:");

        txtBookedToday.setEditable(false);

        txtBookedThisWeek.setEditable(false);

        jLabel4.setText("Booked This Year:");

        txtBookedThisYear.setEditable(false);

        jLabel5.setText("Booked This Month:");

        txtBookedThisMonth.setEditable(false);

        jLabel6.setText("Booked All Time:");

        txtBookedAllTime.setEditable(false);

        jLabel7.setText("Gross Income Today:");

        jLabel8.setText("Gross Income This Week:");

        jLabel9.setText("Gross Income This Month:");

        jLabel10.setText("Gross Income Year:");

        jLabel11.setText("Gross Income All Time:");

        txtGrossToday.setEditable(false);

        jLabel1.setText("Number of Customer Bookings:");

        jLabel12.setText("Income:");

        txtGrossThisWeek.setEditable(false);

        txtGrossThisMonth.setEditable(false);

        txtGrossThisYear.setEditable(false);

        txtGrossAllTime.setEditable(false);

        jLabel13.setText("Number of Guest Bookings:");

        txtGuestBookedToday.setEditable(false);

        jLabel14.setText("Booked Today:");

        jLabel15.setText("Booked This Week:");

        txtGuestBookedThisWeek.setEditable(false);

        txtGuestBookedThisMonth.setEditable(false);

        jLabel16.setText("Booked This Month:");

        jLabel17.setText("Booked This Year:");

        txtGuestBookedThisYear.setEditable(false);

        txtGuestBookedAllTime.setEditable(false);

        jLabel18.setText("Booked All Time:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel5)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(jLabel6))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtBookedThisYear)
                    .addComponent(txtBookedAllTime, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                    .addComponent(txtBookedToday)
                    .addComponent(txtBookedThisWeek)
                    .addComponent(txtBookedThisMonth))
                .addGap(132, 132, 132)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel16)
                        .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(jLabel18))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtGuestBookedThisYear)
                    .addComponent(txtGuestBookedAllTime)
                    .addComponent(txtGuestBookedToday)
                    .addComponent(txtGuestBookedThisWeek)
                    .addComponent(txtGuestBookedThisMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(97, 97, 97)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtGrossToday)
                    .addComponent(txtGrossThisWeek)
                    .addComponent(txtGrossThisMonth)
                    .addComponent(txtGrossThisYear)
                    .addComponent(txtGrossAllTime, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE))
                .addContainerGap(38, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(jLabel1)
                .addGap(230, 230, 230)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel12)
                .addGap(161, 161, 161))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel13)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtBookedToday, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)
                            .addComponent(txtGrossToday, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtBookedThisWeek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(jLabel3)
                            .addComponent(txtGrossThisWeek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtBookedThisMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)
                            .addComponent(txtGrossThisMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtBookedThisYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)
                            .addComponent(txtGrossThisYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtBookedAllTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(txtGrossAllTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(txtGuestBookedToday, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtGuestBookedThisWeek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(txtGuestBookedThisMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(txtGuestBookedThisYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(txtGuestBookedAllTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(77, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblHomePage, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(lblHomePage)
                .addGap(38, 38, 38)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblHomePage;
    private javax.swing.JTextField txtBookedAllTime;
    private javax.swing.JTextField txtBookedThisMonth;
    private javax.swing.JTextField txtBookedThisWeek;
    private javax.swing.JTextField txtBookedThisYear;
    private javax.swing.JTextField txtBookedToday;
    private javax.swing.JTextField txtGrossAllTime;
    private javax.swing.JTextField txtGrossThisMonth;
    private javax.swing.JTextField txtGrossThisWeek;
    private javax.swing.JTextField txtGrossThisYear;
    private javax.swing.JTextField txtGrossToday;
    private javax.swing.JTextField txtGuestBookedAllTime;
    private javax.swing.JTextField txtGuestBookedThisMonth;
    private javax.swing.JTextField txtGuestBookedThisWeek;
    private javax.swing.JTextField txtGuestBookedThisYear;
    private javax.swing.JTextField txtGuestBookedToday;
    // End of variables declaration//GEN-END:variables
}
