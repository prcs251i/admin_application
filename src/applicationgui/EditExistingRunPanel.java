package applicationgui;

import AdministrationApplication.Act;
import AdministrationApplication.Event;
import AdministrationApplication.EventRun;
import AdministrationApplication.Price;
import AdministrationApplication.Tier;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import AdministrationApplication.Venue;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class EditExistingRunPanel extends javax.swing.JPanel {

    private final DefaultComboBoxModel startTimeHoursModel;
    private final DefaultComboBoxModel startTimeMinutesModel;
    private HashMap<Integer, Venue> venuesHash;
    private EventRun selectedRun;
    private final String loginData;
    private HTTPRequestHandler httpHandler;
    private CardLayout cardLayout;
    private JPanel mainPanel;
    private List<JPanel> pricePanels;
    private Act selectedPerformer;
    private Event selectedEvent;
    private List<Double> priceIDs;
    private int noOfCurrentPricesSet;

    public EditExistingRunPanel(CardLayout layoutCard, JPanel main, EventRun run, Event event, String data) throws IOException {
        selectedRun = run;
        startTimeHoursModel = new DefaultComboBoxModel();
        startTimeMinutesModel = new DefaultComboBoxModel();
        initialiseComboBoxModels();
        venuesHash = new HashMap<>();
        initComponents();
        selectedEvent = event;
        loginData = data;
        mainPanel = main;
        cardLayout = layoutCard;
        httpHandler = new HTTPRequestHandler();
        pricePanels = new ArrayList();
        pricePanels.add(pnlTier1);
        pricePanels.add(pnlTier2);
        pricePanels.add(pnlTier3);
        pricePanels.add(pnlTier4);
        pricePanels.add(pnlTier5);
        pricePanels.add(pnlTier6);
        pricePanels.add(pnlTier7);
        pricePanels.add(pnlTier8);
        pricePanels.add(pnlTier9);
        disablePrices();
        fillExistingRunFields();
        noOfCurrentPricesSet = 0;
    }

    private void fillExistingRunFields() {
        txtID.setText(String.valueOf(selectedRun.getID()));
        txtVenue.setText(selectedRun.getVenue().getVenueName());
        dtpRunDate.setDate(selectedRun.getRunDate());
        cbxStartTimeHours.setSelectedItem(String.valueOf(selectedRun.getRunDate().getHours()));
        cbxStartTimeMinutes.setSelectedItem(String.valueOf(selectedRun.getRunDate().getMinutes()));
        spnDuration.setValue(selectedRun.getDuration());
        int tierNo = 0;
        for (Tier tier : selectedRun.getVenue().getTierList()) {
            String seatedStanding;
            if (tier.getIsSeated() == true) {
                seatedStanding = "This Tier is Seated";
            } else {
                seatedStanding = "This Tier is Standing";
            }
            switch (tierNo) {
                case 0:
                    pricePanels.get(0).setVisible(true);
                    lblTier1.setText(tier.getTierName());
                    lblSeatingStanding1.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice1.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
                case 1:
                    pricePanels.get(1).setVisible(true);
                    lblTier2.setText(tier.getTierName());
                    lblSeatingStanding2.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice2.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
                case 2:
                    pricePanels.get(2).setVisible(true);
                    lblTier3.setText(tier.getTierName());
                    lblSeatingStanding3.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice3.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
                case 3:
                    pricePanels.get(3).setVisible(true);
                    lblTier4.setText(tier.getTierName());
                    lblSeatingStanding4.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice4.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
                case 4:
                    pricePanels.get(4).setVisible(true);
                    lblTier5.setText(tier.getTierName());
                    lblSeatingStanding5.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice5.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
                case 5:
                    pricePanels.get(5).setVisible(true);
                    lblTier6.setText(tier.getTierName());
                    lblSeatingStanding6.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice6.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
                case 6:
                    pricePanels.get(6).setVisible(true);
                    lblTier7.setText(tier.getTierName());
                    lblSeatingStanding7.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice7.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
                case 7:
                    pricePanels.get(7).setVisible(true);
                    lblTier8.setText(tier.getTierName());
                    lblSeatingStanding8.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice8.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
                case 8:
                    pricePanels.get(8).setVisible(true);
                    lblTier9.setText(tier.getTierName());
                    lblSeatingStanding9.setText(seatedStanding);
                    if (tier.getTierTicketCost() != null) {
                        txtTierPrice9.setText(String.valueOf(tier.getTierTicketCost().getPrice()));
                        noOfCurrentPricesSet++;
                    }
                    break;
            }
            tierNo++;
        }
    }

    private void initialiseComboBoxModels() {
        LocalDateTime now = LocalDateTime.now();
        for (int i = 0; i < 24; i++) {
            Integer.toString(i);
            startTimeHoursModel.addElement(String.format("%1$02d", i));
        }
        for (int i = 0; i < 60; i++) {
            Integer.toString(i);
            startTimeMinutesModel.addElement(String.format("%1$02d", i));
        }
    }

    private void disablePrices() {
        for (JPanel panel : pricePanels) {
            panel.setVisible(false);
        }
    }

    private Date constructDate() throws ParseException {
        String date = dtpRunDate.getDate().toString();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dtpRunDate.setFormats(dateFormat);
        DateFormat sysDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date_to_store = sysDate.format(dtpRunDate.getDate());
        Date dateTime = sysDate.parse(date_to_store);
        dateTime.setHours(Integer.parseInt(cbxStartTimeHours.getSelectedItem().toString()));
        dateTime.setMinutes(Integer.parseInt(cbxStartTimeMinutes.getSelectedItem().toString()));
        sysDate.format(dateTime);
        return dateTime;
    }

    public String constructUpdatePriceJsonString(EventRun run, Price price) {
        if (run != null) {
            JSONObject j = new JSONObject();
            j.put("ID", price.getId());
            j.put("RUN_ID", run.getID());
            j.put("TIER_ID", price.getTierID());
            j.put("PRICE1", price.getPrice());
            return j.toString();
        } else {
            return null;
        }
    }
    
        public String constructNewPriceJsonString(EventRun run, Price price) {
        if (run != null) {
            JSONObject j = new JSONObject();
            j.put("RUN_ID", run.getID());
            j.put("TIER_ID", price.getTierID());
            j.put("PRICE1", price.getPrice());
            return j.toString();
        } else {
            return null;
        }
    }

    public String constructRunJsonString(EventRun run) throws ParseException, JSONException {
        if (run != null) {
            JSONObject j = new JSONObject();
            j.put("ID", run.getID());
            j.put("EVENT_ID", selectedEvent.getID());
            j.put("VENUE_ID", run.getVenue().getVenueID());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String formattedDate = formatter.format(run.getRunDate());
            j.put("RUN_DATE", formattedDate);
            j.put("DURATION", run.getDuration());
            return j.toString();
        } else {
            return null;
        }
    }

    public Price constructPrice(int tierNo, Tier tier) {
        Price price = new Price();
        switch (tierNo) {
            case 1:
                if (!txtTierPrice1.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice1.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    price.setTierID(tier.getTierId());
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 2:
                if (!txtTierPrice2.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice2.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 3:
                if (!txtTierPrice3.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice3.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 4:
                if (!txtTierPrice4.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice4.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 5:
                if (!txtTierPrice5.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice5.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 6:
                if (!txtTierPrice6.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice6.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 7:
                if (!txtTierPrice7.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice7.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 8:
                if (!txtTierPrice8.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice8.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 9:
                if (!txtTierPrice9.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice9.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
        }
        price.setRunID(selectedRun.getID());
        price.setTierID(tier.getTierId());
        return price;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlAdditionalRun = new javax.swing.JPanel();
        lblEventDate1 = new javax.swing.JLabel();
        lblStartTime1 = new javax.swing.JLabel();
        lvlEventVenue1 = new javax.swing.JLabel();
        cbxStartTimeHours = new javax.swing.JComboBox();
        cbxStartTimeMinutes = new javax.swing.JComboBox();
        lblRunInfo = new javax.swing.JLabel();
        dtpRunDate = new org.jdesktop.swingx.JXDatePicker();
        lblDuration = new javax.swing.JLabel();
        spnDuration = new javax.swing.JSpinner();
        lblID = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtVenue = new javax.swing.JTextField();
        pnlTier7 = new javax.swing.JPanel();
        lblSeatingStanding7 = new javax.swing.JLabel();
        txtTierPrice7 = new javax.swing.JTextField();
        lbl£9 = new javax.swing.JLabel();
        lblTier7 = new javax.swing.JLabel();
        pnlTier5 = new javax.swing.JPanel();
        lblSeatingStanding5 = new javax.swing.JLabel();
        txtTierPrice5 = new javax.swing.JTextField();
        lbl£7 = new javax.swing.JLabel();
        lblTier5 = new javax.swing.JLabel();
        pnlTier3 = new javax.swing.JPanel();
        lblSeatingStanding3 = new javax.swing.JLabel();
        txtTierPrice3 = new javax.swing.JTextField();
        lbl£5 = new javax.swing.JLabel();
        lblTier3 = new javax.swing.JLabel();
        pnlTier1 = new javax.swing.JPanel();
        lblSeatingStanding1 = new javax.swing.JLabel();
        txtTierPrice1 = new javax.swing.JTextField();
        lbl£ = new javax.swing.JLabel();
        lblTier1 = new javax.swing.JLabel();
        pnlTier9 = new javax.swing.JPanel();
        lblSeatingStanding9 = new javax.swing.JLabel();
        txtTierPrice9 = new javax.swing.JTextField();
        lbl£10 = new javax.swing.JLabel();
        lblTier9 = new javax.swing.JLabel();
        pnlTier6 = new javax.swing.JPanel();
        lblSeatingStanding6 = new javax.swing.JLabel();
        txtTierPrice6 = new javax.swing.JTextField();
        lbl£8 = new javax.swing.JLabel();
        lblTier6 = new javax.swing.JLabel();
        pnlTier4 = new javax.swing.JPanel();
        lblSeatingStanding4 = new javax.swing.JLabel();
        txtTierPrice4 = new javax.swing.JTextField();
        lbl£6 = new javax.swing.JLabel();
        lblTier4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        pnlTier2 = new javax.swing.JPanel();
        lblSeatingStanding2 = new javax.swing.JLabel();
        txtTierPrice2 = new javax.swing.JTextField();
        lbl£4 = new javax.swing.JLabel();
        lblTier2 = new javax.swing.JLabel();
        pnlTier8 = new javax.swing.JPanel();
        lblSeatingStanding8 = new javax.swing.JLabel();
        txtTierPrice8 = new javax.swing.JTextField();
        lbl£11 = new javax.swing.JLabel();
        lblTier8 = new javax.swing.JLabel();
        btnEditRun = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        lblEventDate1.setText("Date:");

        lblStartTime1.setText("Start Time:");

        lvlEventVenue1.setText("Venue:");

        cbxStartTimeHours.setModel(startTimeHoursModel);

        cbxStartTimeMinutes.setModel(startTimeMinutesModel);

        lblRunInfo.setText("Run Information");

        lblDuration.setText("Duration (mins) :");

        spnDuration.setModel(new javax.swing.SpinnerNumberModel(15, 15, 4320, 15));

        lblID.setText("ID:");

        txtID.setEditable(false);

        jLabel2.setText("To Change the Venue please Create a new Run");

        txtVenue.setEditable(false);

        javax.swing.GroupLayout pnlAdditionalRunLayout = new javax.swing.GroupLayout(pnlAdditionalRun);
        pnlAdditionalRun.setLayout(pnlAdditionalRunLayout);
        pnlAdditionalRunLayout.setHorizontalGroup(
            pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblDuration)
                        .addGap(18, 18, 18)
                        .addComponent(spnDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblStartTime1)
                            .addComponent(lblEventDate1)
                            .addComponent(lvlEventVenue1))
                        .addGap(18, 18, 18)
                        .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dtpRunDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                .addComponent(cbxStartTimeHours, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbxStartTimeMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtVenue)))
                    .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                        .addGap(124, 124, 124)
                        .addComponent(lblRunInfo))
                    .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(lblID)
                        .addGap(18, 18, 18)
                        .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel2))
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        pnlAdditionalRunLayout.setVerticalGroup(
            pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAdditionalRunLayout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addComponent(lblRunInfo)
                .addGap(22, 22, 22)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(12, 12, 12)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lvlEventVenue1)
                    .addComponent(txtVenue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dtpRunDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEventDate1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxStartTimeHours, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStartTime1)
                    .addComponent(cbxStartTimeMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDuration)
                    .addComponent(spnDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        lblSeatingStanding7.setText("This Tier is Seating/Standing");

        lbl£9.setText("£");

        lblTier7.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier7Layout = new javax.swing.GroupLayout(pnlTier7);
        pnlTier7.setLayout(pnlTier7Layout);
        pnlTier7Layout.setHorizontalGroup(
            pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier7)
                .addGap(73, 73, 73)
                .addComponent(lbl£9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice7, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding7)
                .addContainerGap())
        );
        pnlTier7Layout.setVerticalGroup(
            pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier7)
                    .addComponent(lbl£9)
                    .addComponent(lblSeatingStanding7)))
        );

        lblSeatingStanding5.setText("This Tier is Seating/Standing");

        lbl£7.setText("£");

        lblTier5.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier5Layout = new javax.swing.GroupLayout(pnlTier5);
        pnlTier5.setLayout(pnlTier5Layout);
        pnlTier5Layout.setHorizontalGroup(
            pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier5)
                .addGap(73, 73, 73)
                .addComponent(lbl£7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice5, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding5)
                .addContainerGap())
        );
        pnlTier5Layout.setVerticalGroup(
            pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier5)
                    .addComponent(lbl£7)
                    .addComponent(lblSeatingStanding5)))
        );

        lblSeatingStanding3.setText("This Tier is Seating/Standing");

        lbl£5.setText("£");

        lblTier3.setText("Tier 3:");

        javax.swing.GroupLayout pnlTier3Layout = new javax.swing.GroupLayout(pnlTier3);
        pnlTier3.setLayout(pnlTier3Layout);
        pnlTier3Layout.setHorizontalGroup(
            pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier3)
                .addGap(73, 73, 73)
                .addComponent(lbl£5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding3)
                .addContainerGap())
        );
        pnlTier3Layout.setVerticalGroup(
            pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier3)
                    .addComponent(lbl£5)
                    .addComponent(lblSeatingStanding3)))
        );

        lblSeatingStanding1.setText("This Tier is Seating/Standing");

        lbl£.setText("£");

        lblTier1.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier1Layout = new javax.swing.GroupLayout(pnlTier1);
        pnlTier1.setLayout(pnlTier1Layout);
        pnlTier1Layout.setHorizontalGroup(
            pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier1)
                .addGap(71, 71, 71)
                .addComponent(lbl£)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlTier1Layout.setVerticalGroup(
            pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl£)
                    .addComponent(lblSeatingStanding1)
                    .addComponent(lblTier1)))
        );

        lblSeatingStanding9.setText("This Tier is Seating/Standing");

        lbl£10.setText("£");

        lblTier9.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier9Layout = new javax.swing.GroupLayout(pnlTier9);
        pnlTier9.setLayout(pnlTier9Layout);
        pnlTier9Layout.setHorizontalGroup(
            pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier9)
                .addGap(73, 73, 73)
                .addComponent(lbl£10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice9, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding9)
                .addContainerGap())
        );
        pnlTier9Layout.setVerticalGroup(
            pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier9)
                    .addComponent(lbl£10)
                    .addComponent(lblSeatingStanding9)))
        );

        lblSeatingStanding6.setText("This Tier is Seating/Standing");

        lbl£8.setText("£");

        lblTier6.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier6Layout = new javax.swing.GroupLayout(pnlTier6);
        pnlTier6.setLayout(pnlTier6Layout);
        pnlTier6Layout.setHorizontalGroup(
            pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier6)
                .addGap(73, 73, 73)
                .addComponent(lbl£8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice6, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding6)
                .addContainerGap())
        );
        pnlTier6Layout.setVerticalGroup(
            pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier6)
                    .addComponent(lbl£8)
                    .addComponent(lblSeatingStanding6)))
        );

        lblSeatingStanding4.setText("This Tier is Seating/Standing");

        lbl£6.setText("£");

        lblTier4.setText("Tier 4:");

        javax.swing.GroupLayout pnlTier4Layout = new javax.swing.GroupLayout(pnlTier4);
        pnlTier4.setLayout(pnlTier4Layout);
        pnlTier4Layout.setHorizontalGroup(
            pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier4)
                .addGap(73, 73, 73)
                .addComponent(lbl£6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice4, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding4)
                .addContainerGap())
        );
        pnlTier4Layout.setVerticalGroup(
            pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier4)
                    .addComponent(lbl£6)
                    .addComponent(lblSeatingStanding4)))
        );

        jLabel1.setText("Pricing:");

        lblSeatingStanding2.setText("This Tier is Seating/Standing");

        lbl£4.setText("£");

        lblTier2.setText("Tier 2:");

        javax.swing.GroupLayout pnlTier2Layout = new javax.swing.GroupLayout(pnlTier2);
        pnlTier2.setLayout(pnlTier2Layout);
        pnlTier2Layout.setHorizontalGroup(
            pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier2)
                .addGap(73, 73, 73)
                .addComponent(lbl£4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlTier2Layout.setVerticalGroup(
            pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl£4)
                    .addComponent(lblSeatingStanding2)
                    .addComponent(lblTier2)))
        );

        lblSeatingStanding8.setText("This Tier is Seating/Standing");

        lbl£11.setText("£");

        lblTier8.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier8Layout = new javax.swing.GroupLayout(pnlTier8);
        pnlTier8.setLayout(pnlTier8Layout);
        pnlTier8Layout.setHorizontalGroup(
            pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier8)
                .addGap(73, 73, 73)
                .addComponent(lbl£11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice8, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding8)
                .addContainerGap())
        );
        pnlTier8Layout.setVerticalGroup(
            pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier8)
                    .addComponent(lbl£11)
                    .addComponent(lblSeatingStanding8)))
        );

        btnEditRun.setText("Edit Run");
        btnEditRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditRunActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlAdditionalRun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEditRun, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTier3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTier2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTier1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTier5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTier4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTier6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTier9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTier7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTier8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(216, 216, 216)
                                .addComponent(jLabel1)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnlAdditionalRun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(pnlTier8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditRun)
                    .addComponent(btnCancel))
                .addGap(73, 73, 73))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel these changes?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            try {
                mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingEvents");
            } catch (IOException | JSONException ex) {
                JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
            }
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEditRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditRunActionPerformed
        try {
            if (httpHandler.authenticate(loginData)[0].contains("20")) {
                selectedRun.setDuration((int) spnDuration.getValue());
                selectedRun.setRunDate(constructDate());
                String runData = constructRunJsonString(selectedRun);
                URL runUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/runs/" + selectedRun.getID());
                String httpInfo = httpHandler.sendPUTRequest(runData, runUrl);
                if (httpInfo.contains("20")) {
                    String[] priceInfo = new String[selectedRun.getVenue().getTierList().size()];
                    int count = 0;
                    for (Tier tier : selectedRun.getVenue().getTierList()) {
                        if (tier.getTierTicketCost() != null) {
                            URL priceUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/prices/" + tier.getTierTicketCost().getId());
                            switch (count) {
                                case 0:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice1.getText()));
                                    break;
                                case 1:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice2.getText()));
                                    break;
                                case 2:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice3.getText()));
                                    break;
                                case 3:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice4.getText()));
                                    break;
                                case 4:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice5.getText()));
                                    break;
                                case 5:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice6.getText()));
                                    break;
                                case 6:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice7.getText()));
                                    break;
                                case 7:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice8.getText()));
                                    break;
                                case 8:
                                    tier.getTierTicketCost().setPrice(Double.parseDouble(txtTierPrice9.getText()));
                                    break;
                            }
                            String priceData = constructUpdatePriceJsonString(selectedRun, tier.getTierTicketCost());
                            String priceHttpInfo = httpHandler.sendPUTRequest(priceData, priceUrl);
                            priceInfo[count] = priceHttpInfo;
                            count++;
                            if (priceHttpInfo.contains("20")) {
                            } else {
                                JOptionPane.showMessageDialog(this, httpInfo + "ERROR, Prices have not been updated, please try again");
                            }
                        } else {
                            URL priceUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/prices");
                            Price price = constructPrice(count + 1, tier);
                            String priceData = constructNewPriceJsonString(selectedRun, price);
                            String[] priceHttpInfo = httpHandler.sendPOSTRequest(priceData, priceUrl);
                            if (priceHttpInfo[0].contains("20")) {
                            } else {
                                JOptionPane.showMessageDialog(this, httpInfo + "ERROR, a Price could not be added");
                            }
                        }
                    }
                    JOptionPane.showMessageDialog(this, httpInfo + "Run Successfully Edited");
                    cardLayout.removeLayoutComponent(this);
                    mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                    cardLayout.show(mainPanel, "ViewExistingEvents");
                } else {
                    JOptionPane.showMessageDialog(this, httpInfo + "ERROR, Run not updated");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                mainPanel.removeAll();
                new LogIn().setVisible(true);
            }
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this, "ERROR Run NOT Updated \n " + ex);
        } catch (MalformedURLException ex) {
            JOptionPane.showMessageDialog(this, "ERROR Run NOT Updated \n " + ex);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR Run NOT Updated \n " + ex);
        }

    }//GEN-LAST:event_btnEditRunActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEditRun;
    private javax.swing.JComboBox cbxStartTimeHours;
    private javax.swing.JComboBox cbxStartTimeMinutes;
    private org.jdesktop.swingx.JXDatePicker dtpRunDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblDuration;
    private javax.swing.JLabel lblEventDate1;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblRunInfo;
    private javax.swing.JLabel lblSeatingStanding1;
    private javax.swing.JLabel lblSeatingStanding2;
    private javax.swing.JLabel lblSeatingStanding3;
    private javax.swing.JLabel lblSeatingStanding4;
    private javax.swing.JLabel lblSeatingStanding5;
    private javax.swing.JLabel lblSeatingStanding6;
    private javax.swing.JLabel lblSeatingStanding7;
    private javax.swing.JLabel lblSeatingStanding8;
    private javax.swing.JLabel lblSeatingStanding9;
    private javax.swing.JLabel lblStartTime1;
    private javax.swing.JLabel lblTier1;
    private javax.swing.JLabel lblTier2;
    private javax.swing.JLabel lblTier3;
    private javax.swing.JLabel lblTier4;
    private javax.swing.JLabel lblTier5;
    private javax.swing.JLabel lblTier6;
    private javax.swing.JLabel lblTier7;
    private javax.swing.JLabel lblTier8;
    private javax.swing.JLabel lblTier9;
    private javax.swing.JLabel lbl£;
    private javax.swing.JLabel lbl£10;
    private javax.swing.JLabel lbl£11;
    private javax.swing.JLabel lbl£4;
    private javax.swing.JLabel lbl£5;
    private javax.swing.JLabel lbl£6;
    private javax.swing.JLabel lbl£7;
    private javax.swing.JLabel lbl£8;
    private javax.swing.JLabel lbl£9;
    private javax.swing.JLabel lvlEventVenue1;
    private javax.swing.JPanel pnlAdditionalRun;
    private javax.swing.JPanel pnlTier1;
    private javax.swing.JPanel pnlTier2;
    private javax.swing.JPanel pnlTier3;
    private javax.swing.JPanel pnlTier4;
    private javax.swing.JPanel pnlTier5;
    private javax.swing.JPanel pnlTier6;
    private javax.swing.JPanel pnlTier7;
    private javax.swing.JPanel pnlTier8;
    private javax.swing.JPanel pnlTier9;
    private javax.swing.JSpinner spnDuration;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtTierPrice1;
    private javax.swing.JTextField txtTierPrice2;
    private javax.swing.JTextField txtTierPrice3;
    private javax.swing.JTextField txtTierPrice4;
    private javax.swing.JTextField txtTierPrice5;
    private javax.swing.JTextField txtTierPrice6;
    private javax.swing.JTextField txtTierPrice7;
    private javax.swing.JTextField txtTierPrice8;
    private javax.swing.JTextField txtTierPrice9;
    private javax.swing.JTextField txtVenue;
    // End of variables declaration//GEN-END:variables
}
