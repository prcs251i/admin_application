package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Act;
import AdministrationApplication.Event;
import AdministrationApplication.EventRun;
import AdministrationApplication.Tier;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;

/**
 *
 * @author Joel Gooch
 */
public class ViewExistingEventsPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private HashMap<Integer, Event> eventHashMap;
    private HashMap<Integer, EventRun> runHashMap;
    private HashMap<Integer, Act> performersHashMap;
    private List<Event> eventsList;
    private List<Event> filteredList;
    private DefaultTableModel eventsModel;
    private DefaultTableModel runsModel;
    private DefaultTableModel performersModel;
    private Event selectedEvent;
    private EventRun selectedRun;
    private Act selectedPerformer;
    private final String loginData;
    private HTTPRequestHandler httpHandler;

    public ViewExistingEventsPanel(CardLayout layoutCard, JPanel main, String data) throws IOException, JSONException {
        cardLayout = layoutCard;
        mainPanel = main;
        initComponents();
        initialiseTables();
        eventHashMap = new HashMap<>();
        runHashMap = new HashMap<>();
        performersHashMap = new HashMap<>();
        JsonClassGenerator generator = new JsonClassGenerator();
        filteredList = new ArrayList();
        eventsList = generator.generateEvents();
        for (Event event : eventsList) {
            eventHashMap.put(event.getID(), event);
        }
        populateEventsTable(eventsList);
        loginData = data;
        httpHandler = new HTTPRequestHandler();
    }

    private void initialiseTables() {
        eventsModel = (DefaultTableModel) tblEvents.getModel();
        runsModel = (DefaultTableModel) tblRuns.getModel();
        performersModel = (DefaultTableModel) tblPerformers.getModel();
        ListSelectionModel eventsSelectionModel = tblEvents.getSelectionModel();
        eventsSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    runsModel.setNumRows(0);
                    performersModel.setNumRows(0);
                    if (tblEvents.getSelectedRow() != -1) {
                        int selectedEventID = (int) tblEvents.getValueAt(tblEvents.getSelectedRow(), 0);
                        selectedEvent = eventHashMap.get(selectedEventID);
                        selectedRun = null;
                        for (EventRun run : selectedEvent.getEventRuns()) {
                            runHashMap.put(run.getID(), run);
                        }
                        populateRunsTable(selectedEvent);
                    }
                }
            }
        });
        ListSelectionModel runsSelectionModel = tblRuns.getSelectionModel();
        runsSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    performersModel.setNumRows(0);
                    if (tblRuns.getSelectedRow() != -1) {
                        int selectedRunID = (int) tblRuns.getValueAt(tblRuns.getSelectedRow(), 0);
                        selectedRun = runHashMap.get(selectedRunID);
                        for (Act performer : selectedRun.getActsPerforming()) {
                            performersHashMap.put(performer.getID(), performer);
                        }
                        populatePerformersTable(selectedRun);
                    }
                }
            }
        });
        ListSelectionModel actsSelectionModel = tblPerformers.getSelectionModel();
        actsSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (tblPerformers.getSelectedRow() != -1) {
                        int selectedPerformerID = (int) tblPerformers.getValueAt(tblPerformers.getSelectedRow(), 1);
                        selectedPerformer = performersHashMap.get(selectedPerformerID);
                    }
                }
            }
        });
        setTableColumnWidths();
    }

    private void populateEventsTable(List<Event> eventsList) {
        eventsModel.setNumRows(0);
        Object[] row = new Object[7];
        for (int i = 0; i < eventsList.size(); i++) {
            row[0] = eventsList.get(i).getID();
            row[1] = eventsList.get(i).getEventTitle();
            row[2] = eventsList.get(i).getEventDescription();
            row[3] = eventsList.get(i).getEventType().getType();
            row[4] = eventsList.get(i).getAgeRating();
            row[5] = eventsList.get(i).getAverageRating();
            row[6] = eventsList.get(i).getImageString();
            eventsModel.addRow(row);
        }
    }

    private void populateRunsTable(Event selectedEvent) {
        runsModel.setNumRows(0);
        List<EventRun> runList = selectedEvent.getEventRuns();
        Object[] row = new Object[4];
        for (int i = 0; i < runList.size(); i++) {
            row[0] = runList.get(i).getID();
            row[1] = runList.get(i).getVenue().getVenueName();
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(runList.get(i).getRunDate());
            row[2] = formattedDate;
            row[3] = runList.get(i).getDuration();
            runsModel.addRow(row);
        }
    }

    private void populatePerformersTable(EventRun selectedRun) {
        performersModel.setNumRows(0);
        List<Act> actList = selectedRun.getActsPerforming();
        Object[] row = new Object[4];
        for (int i = 0; i < actList.size(); i++) {
            row[0] = actList.get(i).getPerformanceID();
            row[1] = actList.get(i).getID();
            row[2] = actList.get(i).getActName();
            row[3] = actList.get(i).getActType().getType();
            performersModel.addRow(row);
        }
    }

    private void setTableColumnWidths() {
        tblEvents.getColumnModel().getColumn(0).setPreferredWidth(60);
        tblEvents.getColumnModel().getColumn(1).setPreferredWidth(180);
        tblEvents.getColumnModel().getColumn(2).setPreferredWidth(180);
        tblEvents.getColumnModel().getColumn(3).setPreferredWidth(100);
        tblEvents.getColumnModel().getColumn(4).setPreferredWidth(60);
        tblEvents.getColumnModel().getColumn(5).setPreferredWidth(60);
        tblEvents.getColumnModel().getColumn(6).setPreferredWidth(135);
        tblRuns.getColumnModel().getColumn(0).setPreferredWidth(60);
        tblRuns.getColumnModel().getColumn(1).setPreferredWidth(180);
        tblRuns.getColumnModel().getColumn(2).setPreferredWidth(195);
        tblRuns.getColumnModel().getColumn(3).setPreferredWidth(70);
        tblPerformers.getColumnModel().getColumn(0).setPreferredWidth(60);
        tblPerformers.getColumnModel().getColumn(1).setPreferredWidth(60);
        tblPerformers.getColumnModel().getColumn(2).setPreferredWidth(180);
        tblPerformers.getColumnModel().getColumn(3).setPreferredWidth(195);
    }

    private void refreshPage() {
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "ViewExistingEvents");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblViewExistingCustomers = new javax.swing.JLabel();
        lblSearchCustomer = new javax.swing.JLabel();
        txtSearchEvent = new javax.swing.JTextField();
        cbxCriteria = new javax.swing.JComboBox();
        scpEvents = new javax.swing.JScrollPane();
        tblEvents = new javax.swing.JTable();
        btnAddEvent = new javax.swing.JButton();
        btnDeleteEvent = new javax.swing.JButton();
        scpRuns = new javax.swing.JScrollPane();
        tblRuns = new javax.swing.JTable();
        scpPerformers = new javax.swing.JScrollPane();
        tblPerformers = new javax.swing.JTable();
        lblEvents = new javax.swing.JLabel();
        lblRuns = new javax.swing.JLabel();
        lblPerformers = new javax.swing.JLabel();
        btnSearch = new javax.swing.JButton();
        btnEditEvent = new javax.swing.JButton();
        lblRunInformation = new javax.swing.JLabel();
        lblPerformerInformation = new javax.swing.JLabel();
        btnAddRun = new javax.swing.JButton();
        btnDeleteRun = new javax.swing.JButton();
        btnEditRun = new javax.swing.JButton();
        btnDeletePerformer = new javax.swing.JButton();
        btnAddPerformer = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        btnRefresh = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();

        lblViewExistingCustomers.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblViewExistingCustomers.setText("View Events");

        lblSearchCustomer.setText("Search:");

        cbxCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "TITLE", "EVENT TYPE", "AGE RATING" }));
        cbxCriteria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxCriteriaActionPerformed(evt);
            }
        });

        tblEvents.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Title", "Description", "Event Type", "Age", "Avg", "Image link"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        scpEvents.setViewportView(tblEvents);

        btnAddEvent.setText("Add Event");
        btnAddEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddEventActionPerformed(evt);
            }
        });

        btnDeleteEvent.setText("Delete Selected Event");
        btnDeleteEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteEventActionPerformed(evt);
            }
        });

        tblRuns.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Venue", "Run Date", "Duration (mins)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        scpRuns.setViewportView(tblRuns);

        tblPerformers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Act ID", "Name", "Genre"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        scpPerformers.setViewportView(tblPerformers);

        lblEvents.setText("Events:");

        lblRuns.setText("Runs for Selected Event:");

        lblPerformers.setText("Performers for Selected Run:");

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnEditEvent.setText("Edit Selected Event");
        btnEditEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditEventActionPerformed(evt);
            }
        });

        lblRunInformation.setText("Select Event to View Run Information ");

        lblPerformerInformation.setText("Select Run to View Performer Information ");

        btnAddRun.setText("Add Run to Selected Event");
        btnAddRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddRunActionPerformed(evt);
            }
        });

        btnDeleteRun.setText("Delete Selected Run");
        btnDeleteRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteRunActionPerformed(evt);
            }
        });

        btnEditRun.setText("Edit Selected Run");
        btnEditRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditRunActionPerformed(evt);
            }
        });

        btnDeletePerformer.setText("Remove Performer from Selected Run");
        btnDeletePerformer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeletePerformerActionPerformed(evt);
            }
        });

        btnAddPerformer.setText("Add Performer to Selected Run");
        btnAddPerformer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddPerformerActionPerformed(evt);
            }
        });

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/refresh-icon-2.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnReset.setText("Reset Search");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(lblViewExistingCustomers)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(lblSearchCustomer)
                                .addGap(18, 18, 18)
                                .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtSearchEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(175, 175, 175))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblEvents)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnAddEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(31, 31, 31)
                                    .addComponent(btnEditEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnDeleteEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(scpEvents, javax.swing.GroupLayout.PREFERRED_SIZE, 707, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(128, 128, 128)
                        .addComponent(lblRunInformation))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addComponent(lblPerformerInformation))
                    .addComponent(lblPerformers)
                    .addComponent(scpPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, 496, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddRun)
                    .addComponent(lblRuns)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(152, 152, 152)
                        .addComponent(btnDeleteRun, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(284, 284, 284)
                        .addComponent(btnEditRun, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAddPerformer)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDeletePerformer, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnRefresh)
                            .addComponent(scpRuns, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblViewExistingCustomers)
                            .addComponent(btnReset))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSearchCustomer)
                            .addComponent(txtSearchEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearch)
                            .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEvents)
                    .addComponent(lblRunInformation))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(scpEvents, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnEditEvent)
                            .addComponent(btnAddEvent)
                            .addComponent(btnDeleteEvent)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblRuns)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scpRuns, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAddRun)
                            .addComponent(btnEditRun))
                        .addGap(3, 3, 3)
                        .addComponent(btnDeleteRun)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblPerformerInformation, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblPerformers)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scpPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddPerformer)
                    .addComponent(btnDeletePerformer))
                .addContainerGap(11, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cbxCriteriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxCriteriaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxCriteriaActionPerformed

    private void btnDeleteRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteRunActionPerformed
        if (selectedRun != null) {
            try {
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete " + selectedEvent.getEventTitle()
                        + " at " + selectedRun.getVenue().getVenueName() + "?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.YES_OPTION) {
                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                        if (!selectedRun.getActsPerforming().isEmpty()) {
                            for (Act performer : selectedRun.getActsPerforming()) {
                                URL performerUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/performances/" + performer.getPerformanceID());
                                String performerHttpInfo = httpHandler.sendDELETERequest(performerUrl);
                                if (performerHttpInfo.contains("20")) {
                                } else {
                                    JOptionPane.showMessageDialog(this, performerHttpInfo + "\n ERROR a performer has not been deleted");
                                }
                            }
                        }
                        for (Tier tier : selectedRun.getVenue().getTierList()) {
                            if (tier.getTierTicketCost() != null) {
                                URL priceUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/prices/" + tier.getTierTicketCost().getId());
                                String priceHttpInfo = httpHandler.sendDELETERequest(priceUrl);
                                if (priceHttpInfo.contains("20")) {
                                } else {
                                    JOptionPane.showMessageDialog(this, priceHttpInfo + "\n ERROR a price has not been deleted");
                                }
                            }
                        }
                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/runs/" + selectedRun.getID());
                        String httpInfo = httpHandler.sendDELETERequest(url);
                        if (httpInfo.contains("20")) {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n Run was successfully deleted!");
                            refreshPage();
                        } else {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n ERROR Run was NOT deleted! If there have been bookings made for this run then it can not be deleted,"
                                    + "refunds must be given or new tickets on an alternative run distributed (free of charge)");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                        mainPanel.removeAll();
                        new LogIn().setVisible(true);
                    }
                }
            } catch (JSONException | IOException ex) {
                JOptionPane.showMessageDialog(this, "ERROR Run NOT Deleted \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Run to Delete");
        }
    }//GEN-LAST:event_btnDeleteRunActionPerformed

    private void btnDeletePerformerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeletePerformerActionPerformed
        if (selectedRun != null) {
            try {
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete " + selectedPerformer.getActName()
                        + " at " + selectedEvent.getEventTitle() + " at " + selectedRun.getVenue().getVenueName() + "?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.YES_OPTION) {
                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/performances/" + selectedPerformer.getPerformanceID());
                        String httpInfo = httpHandler.sendDELETERequest(url);
                        if (httpInfo.contains("20")) {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n \"Performer was successfully removed!\"");
                            refreshPage();
                        } else {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n \"ERROR performer was not removed!\"");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                        mainPanel.removeAll();
                        new LogIn().setVisible(true);
                    }
                }
            } catch (JSONException | IOException ex) {
                JOptionPane.showMessageDialog(this, "ERROR Peformer NOT Deleted \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Performer to Remove");
        }
    }//GEN-LAST:event_btnDeletePerformerActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshPage();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnEditEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditEventActionPerformed
        if (selectedEvent != null) {
            try {
                mainPanel.add("EditExistingEvent", new EditExistingEventsPanel(cardLayout, mainPanel, selectedEvent, loginData));
                cardLayout.show(mainPanel, "EditExistingEvent");
            } catch (IOException | JSONException ex) {
                JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select an Event to Edit");
        }
    }//GEN-LAST:event_btnEditEventActionPerformed

    private void btnEditRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditRunActionPerformed
        if (selectedRun != null) {
            try {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("EditExistingRun", new EditExistingRunPanel(cardLayout, mainPanel, selectedRun, selectedEvent, loginData));
                cardLayout.show(mainPanel, "EditExistingRun");
            } catch (IOException | JSONException ex) {
                JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select an Run to Edit");
        }
    }//GEN-LAST:event_btnEditRunActionPerformed

    private void btnAddEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddEventActionPerformed
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("AddNewEvents", new AddEventPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "AddNewEvents");
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_btnAddEventActionPerformed

    private void btnAddRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddRunActionPerformed
        try {
            if (selectedEvent != null) {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("AddNewRun", new AddRunPanel(selectedEvent, cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "AddNewRun");
            } else {
                JOptionPane.showMessageDialog(this, "Please select an Event to Add a Run to");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_btnAddRunActionPerformed

    private void btnDeleteEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteEventActionPerformed
        if (selectedEvent != null) {
            try {
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete " + selectedEvent.getEventTitle() + "?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.YES_OPTION) {
                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/events/" + selectedEvent.getID());
                        String httpInfo = httpHandler.sendDELETERequest(url);
                        if (httpInfo.contains("20")) {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n Event was successfully deleted!");
                            refreshPage();
                        } else {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n ERROR Event was not deleted!, \n"
                                    + "Please check there is no runs attached to the selected Event");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                        mainPanel.removeAll();
                        new LogIn().setVisible(true);
                    }
                }
            } catch (JSONException | IOException ex) {
                JOptionPane.showMessageDialog(this, "ERROR Event NOT Deleted \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select an Event to Delete");
        }
    }//GEN-LAST:event_btnDeleteEventActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        filteredList.clear();
        String searchCriteria = txtSearchEvent.getText().toLowerCase();
        for (Event event : eventsList) {
            if (cbxCriteria.getSelectedItem().toString().equals("ID")) {
                try {
                    if (searchCriteria.isEmpty() || event.getID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(event);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Event ID");
                    populateEventsTable(eventsList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("TITLE")) {
                if (event.getEventTitle().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(event);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("EVENT TYPE")) {
                if (event.getEventType().getType().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(event);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("AGE RATING")) {
                if (event.getAgeRating().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(event);
                }
            }
        }
        populateEventsTable(filteredList);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        populateEventsTable(eventsList);
        txtSearchEvent.setText("");
    }//GEN-LAST:event_btnResetActionPerformed

    private void btnAddPerformerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddPerformerActionPerformed
        try {
            if (selectedEvent != null) {
                if (selectedRun != null) {
                    cardLayout.removeLayoutComponent(this);
                    mainPanel.add("AddNewPerformer", new AddPerformerToRunPanel(cardLayout, mainPanel, loginData, selectedEvent, selectedRun));
                    cardLayout.show(mainPanel, "AddNewPerformer");
                } else {
                    JOptionPane.showMessageDialog(this, "Please select a run to add performer to");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please select an event to add performer to");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_btnAddPerformerActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddEvent;
    private javax.swing.JButton btnAddPerformer;
    private javax.swing.JButton btnAddRun;
    private javax.swing.JButton btnDeleteEvent;
    private javax.swing.JButton btnDeletePerformer;
    private javax.swing.JButton btnDeleteRun;
    private javax.swing.JButton btnEditEvent;
    private javax.swing.JButton btnEditRun;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox cbxCriteria;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblEvents;
    private javax.swing.JLabel lblPerformerInformation;
    private javax.swing.JLabel lblPerformers;
    private javax.swing.JLabel lblRunInformation;
    private javax.swing.JLabel lblRuns;
    private javax.swing.JLabel lblSearchCustomer;
    private javax.swing.JLabel lblViewExistingCustomers;
    private javax.swing.JScrollPane scpEvents;
    private javax.swing.JScrollPane scpPerformers;
    private javax.swing.JScrollPane scpRuns;
    private javax.swing.JTable tblEvents;
    private javax.swing.JTable tblPerformers;
    private javax.swing.JTable tblRuns;
    private javax.swing.JTextField txtSearchEvent;
    // End of variables declaration//GEN-END:variables
}
