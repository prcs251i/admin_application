package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.GuestBooking;
import AdministrationApplication.SeatedTicket;
import AdministrationApplication.Ticket;
import java.awt.CardLayout;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Joel Gooch
 */

public class ViewGuestBookingsPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private HashMap<Integer, GuestBooking> bookingsHashMap;
    private HashMap<Integer, Ticket> ticketsHashMap;
    private List<GuestBooking> bookingsList;
    private List<GuestBooking> filteredList;
    private DefaultTableModel bookingsModel;
    private DefaultTableModel ticketsModel;
    private GuestBooking selectedBooking;
    private Ticket selectedTicket;
    private final String loginData;

    public ViewGuestBookingsPanel(CardLayout layoutCard, JPanel main, String data) throws IOException {
        cardLayout = layoutCard;
        mainPanel = main;
        initComponents();
        initialiseTables();
        bookingsHashMap = new HashMap<>();
        ticketsHashMap = new HashMap<>();
        JsonClassGenerator generator = new JsonClassGenerator();
        filteredList = new ArrayList();
        bookingsList = generator.generateGuestBookings();
        for (GuestBooking booking : bookingsList) {
            bookingsHashMap.put(booking.getBookingID(), booking);
        }
        populateBookingsTable(bookingsList);
        loginData = data;
    }

    private void initialiseTables() {
        bookingsModel = (DefaultTableModel) tblGuestBookings.getModel(); // if i want to stop auto size
        ticketsModel = (DefaultTableModel) tblTickets.getModel();
        ListSelectionModel bookingSelectionModel = tblGuestBookings.getSelectionModel();
        bookingSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    ticketsModel.setNumRows(0);
                    if (tblGuestBookings.getSelectedRow() != -1) {
                        int selectedBookingID = (int) tblGuestBookings.getValueAt(tblGuestBookings.getSelectedRow(), 0);
                        selectedBooking = bookingsHashMap.get(selectedBookingID);
                        for (Ticket ticket : selectedBooking.getTicketList()) {
                            ticketsHashMap.put(ticket.getTicketRef(), ticket);
                        }
                        populateTicketsTable(selectedBooking);
                    }
                }
            }
        });
        ListSelectionModel ticketSelectionModel = tblTickets.getSelectionModel();
        ticketSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (tblTickets.getSelectedRow() != -1) {
                        int selectedTicketID = (int) tblTickets.getValueAt(tblTickets.getSelectedRow(), 0);
                        selectedTicket = ticketsHashMap.get(selectedTicketID);
                    }
                }
            }
        });
        setTableColumnWidths();
    }

    private void populateBookingsTable(List<GuestBooking> bookingsList) {
        bookingsModel.setNumRows(0);
        Object[] row = new Object[8];
        for (int i = 0; i < bookingsList.size(); i++) {
            row[0] = bookingsList.get(i).getBookingID();
            row[1] = bookingsList.get(i).getRun().getID();
            row[2] = bookingsList.get(i).getFirstName();
            row[3] = bookingsList.get(i).getLastName();
            row[4] = bookingsList.get(i).getEmail();
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(bookingsList.get(i).getDatePlaced());
            row[5] = formattedDate;
            row[6] = bookingsList.get(i).getNoOfTickets();
            row[7] = bookingsList.get(i).getTotalCost();
            bookingsModel.addRow(row);
        }
    }

    private void populateTicketsTable(GuestBooking selectedBooking) {
        ticketsModel.setNumRows(0);
        List<Ticket> ticketList = selectedBooking.getTicketList();
        Object[] row = new Object[4];
        for (int i = 0; i < ticketList.size(); i++) {
            row[0] = ticketList.get(i).getTicketRef();
            row[1] = ticketList.get(i).getAllocatedTier().getTierName();
            if (ticketList.get(i) instanceof SeatedTicket) {
                SeatedTicket ticket = (SeatedTicket) (ticketList.get(i));
                row[2] = ticket.getSeatRow();
                row[3] = ticket.getSeatNo();
            }
            ticketsModel.addRow(row);
        }
    }

    private void setTableColumnWidths() {
        tblGuestBookings.getColumnModel().getColumn(0).setPreferredWidth(60);
        tblGuestBookings.getColumnModel().getColumn(1).setPreferredWidth(60);
        tblGuestBookings.getColumnModel().getColumn(2).setPreferredWidth(120);
        tblGuestBookings.getColumnModel().getColumn(3).setPreferredWidth(120);
        tblGuestBookings.getColumnModel().getColumn(4).setPreferredWidth(220);
        tblGuestBookings.getColumnModel().getColumn(5).setPreferredWidth(100);
        tblGuestBookings.getColumnModel().getColumn(6).setPreferredWidth(120);
        tblGuestBookings.getColumnModel().getColumn(7).setPreferredWidth(100);
        tblTickets.getColumnModel().getColumn(0).setPreferredWidth(80);
        tblTickets.getColumnModel().getColumn(1).setPreferredWidth(200);
        tblTickets.getColumnModel().getColumn(2).setPreferredWidth(100);
        tblTickets.getColumnModel().getColumn(3).setPreferredWidth(100);
    }
    
    private void refreshPage() {
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("ViewGuestBookings", new ViewGuestBookingsPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "ViewGuestBookings");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane8 = new javax.swing.JScrollPane();
        tblGuestBookings = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTickets = new javax.swing.JTable();
        lblSearchBookings = new javax.swing.JLabel();
        lblViewBookings = new javax.swing.JLabel();
        txtSearchBookings = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        cbxCriteria = new javax.swing.JComboBox();
        btnAddBooking = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblRunInformation = new javax.swing.JLabel();
        lblTickets = new javax.swing.JLabel();
        btnRefresh = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();

        tblGuestBookings.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Run ID", "First Name", "Last Name", "Email", "Order Date", "Ticket Quantity", "Total Cost (£)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane8.setViewportView(tblGuestBookings);

        tblTickets.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Tier Name", "Seat Row", "Seat No"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblTickets);

        lblSearchBookings.setText("Search");

        lblViewBookings.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblViewBookings.setText("View Guest Bookings");

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        cbxCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "RUN ID", "EMAIL", "ORDER DATE" }));

        btnAddBooking.setText("Add Guest Booking");
        btnAddBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddBookingActionPerformed(evt);
            }
        });

        jLabel1.setText("Guest Bookings:");

        lblRunInformation.setText("Select Booking to View Ticket Information ");

        lblTickets.setText("Tickets in Selected Booking:");

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/refresh-icon-2.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnReset.setText("Reset Search");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblSearchBookings)
                                .addGap(18, 18, 18)
                                .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(43, 43, 43)
                                        .addComponent(btnAddBooking, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(txtSearchBookings, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblViewBookings)
                                .addGap(40, 40, 40)
                                .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(648, 648, 648)
                        .addComponent(btnRefresh))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 794, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTickets)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(127, 127, 127)
                                .addComponent(lblRunInformation))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnRefresh)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnReset)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblViewBookings)
                                .addGap(30, 30, 30)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSearchBookings)
                            .addComponent(txtSearchBookings, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearch))))
                .addGap(22, 22, 22)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblRunInformation)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTickets)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(110, 110, 110)))
                .addComponent(btnAddBooking)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddBookingActionPerformed
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("NewGuestBooking", new AddGuestBookingPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "NewGuestBooking");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_btnAddBookingActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        filteredList.clear();
        String searchCriteria = txtSearchBookings.getText().toLowerCase();
        for (GuestBooking booking : bookingsList) {
            if (cbxCriteria.getSelectedItem().toString().equals("ID")) {
                try {
                    if (searchCriteria.isEmpty() || booking.getBookingID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(booking);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Booking ID");
                    populateBookingsTable(bookingsList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("RUN ID")) {
                try {
                    if (searchCriteria.isEmpty() || booking.getRun().getID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(booking);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Run ID");
                    populateBookingsTable(bookingsList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("EMAIL")) {
                if (booking.getEmail().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(booking);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("ORDER DATE")) {
                String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(booking.getDatePlaced());
                if (formattedDate.toLowerCase().contains(searchCriteria)) {
                    filteredList.add(booking);
                }
            }
        }
        populateBookingsTable(filteredList);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshPage();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        populateBookingsTable(bookingsList);
        txtSearchBookings.setText("");
    }//GEN-LAST:event_btnResetActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddBooking;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox cbxCriteria;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JLabel lblRunInformation;
    private javax.swing.JLabel lblSearchBookings;
    private javax.swing.JLabel lblTickets;
    private javax.swing.JLabel lblViewBookings;
    private javax.swing.JTable tblGuestBookings;
    private javax.swing.JTable tblTickets;
    private javax.swing.JTextField txtSearchBookings;
    // End of variables declaration//GEN-END:variables
}
