package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Act;
import AdministrationApplication.Event;
import AdministrationApplication.EventRun;
import AdministrationApplication.Price;
import AdministrationApplication.Tier;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import AdministrationApplication.Venue;
import java.awt.CardLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class AddRunPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private HTTPRequestHandler httpHandler;
    private final String loginData;
    private Event event;
    private final DefaultComboBoxModel startTimeHoursModel;
    private final DefaultComboBoxModel startTimeMinutesModel;
    private DefaultListModel performersListModel;
    private DefaultComboBoxModel venuesComboModel;
    private JsonClassGenerator generator;
    private HashMap<Integer, Act> performersHash;
    private HashMap<Integer, Venue> venuesHash;
    private List<Act> selectedPerformers;
    private List<Act> performersList;
    private List<Act> filteredList;
    private Act selectedPerformer;
    private Venue selectedVenue;
    private List<JPanel> pricePanels;

    public AddRunPanel(Event selectedEvent, CardLayout layoutCard, JPanel main, String data) throws IOException {
        cardLayout = layoutCard;
        mainPanel = main;
        loginData = data;
        event = selectedEvent;
        httpHandler = new HTTPRequestHandler();
        startTimeHoursModel = new DefaultComboBoxModel();
        startTimeMinutesModel = new DefaultComboBoxModel();
        initialiseComboBoxModels();
        performersListModel = new DefaultListModel();
        venuesComboModel = new DefaultComboBoxModel();
        initComponents();
        filteredList = new ArrayList();
        selectedPerformers = new ArrayList();
        performersHash = new HashMap<>();
        venuesHash = new HashMap<>();
        generator = new JsonClassGenerator();
        fillVenuesComboBox();
        initialisePerformersList();
        txtEvent.setText(event.getEventTitle());
        txtAdditionalPerformers.setLineWrap(true);
        pricePanels = new ArrayList();
        pricePanels.add(pnlTier1);
        pricePanels.add(pnlTier2);
        pricePanels.add(pnlTier3);
        pricePanels.add(pnlTier4);
        pricePanels.add(pnlTier5);
        pricePanels.add(pnlTier6);
        pricePanels.add(pnlTier7);
        pricePanels.add(pnlTier8);
        pricePanels.add(pnlTier9);
        cbxEventVenue.setSelectedIndex(1);
        cbxEventVenue.setSelectedIndex(0);
    }

    private void disablePrices() {
        for (JPanel panel : pricePanels) {
            panel.setVisible(false);
        }
    }

    private void filterPerformersList(List<Act> performersList) {
        performersListModel.clear();
        int count = 0;
        for (Act performer : performersList) {
            performersListModel.add(count, performer.getID() + " - " + performer.getActName());
            count++;
        }
    }

    private void initialisePerformersList() throws IOException {
        ListSelectionModel runsSelectionModel = lstAdditionalPerformers2.getSelectionModel();
        runsSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (lstAdditionalPerformers2.getSelectedIndex() != -1) {
                        String performer = lstAdditionalPerformers2.getSelectedValue().toString();
                        String performerId = performer.split(" - ", 2)[0];
                        int id = Integer.parseInt(performerId);
                        selectedPerformer = performersHash.get(id);
                    }
                }
            }
        });
        performersList = generator.generateActs();
        int count = 0;
        for (Act performer : performersList) {
            performersHash.put(performer.getID(), performer);
            performersListModel.add(count, performer.getID() + " - " + performer.getActName());
        }
    }

    private void fillVenuesComboBox() throws IOException, JSONException {
        List<Venue> venuesList = generator.generateVenues();
        for (Venue venue : venuesList) {
            venuesComboModel.addElement(venue.getVenueID() + " - " + venue.getVenueName());
            venuesHash.put(venue.getVenueID(), venue);
            cbxEventVenue.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    disablePrices();
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        if (cbxEventVenue.getSelectedIndex() != -1) {
                            String venue = cbxEventVenue.getSelectedItem().toString();
                            String venueId = venue.split(" - ", 2)[0];
                            int id = Integer.parseInt(venueId);
                            selectedVenue = venuesHash.get(id);
                            int i = 1;
                            for (Tier tier : selectedVenue.getTierList()) {
                                pricePanels.get(i - 1).setVisible(true);
                                String seatedStanding;
                                if (tier.getIsSeated() == true) {
                                    seatedStanding = "This Tier is Seated";
                                } else {
                                    seatedStanding = "This Tier is Standing";
                                }
                                switch (i) {
                                    case 1:
                                        lblTier1.setText(tier.getTierName());
                                        lblSeatingStanding1.setText(seatedStanding);
                                        break;
                                    case 2:
                                        lblTier2.setText(tier.getTierName());
                                        lblSeatingStanding2.setText(seatedStanding);
                                        break;
                                    case 3:
                                        lblTier3.setText(tier.getTierName());
                                        lblSeatingStanding3.setText(seatedStanding);
                                        break;
                                    case 4:
                                        lblTier4.setText(tier.getTierName());
                                        lblSeatingStanding4.setText(seatedStanding);
                                        break;
                                    case 5:
                                        lblTier5.setText(tier.getTierName());
                                        lblSeatingStanding5.setText(seatedStanding);
                                        break;
                                    case 6:
                                        lblTier6.setText(tier.getTierName());
                                        lblSeatingStanding6.setText(seatedStanding);
                                        break;
                                    case 7:
                                        lblTier7.setText(tier.getTierName());
                                        lblSeatingStanding7.setText(seatedStanding);
                                        break;
                                    case 8:
                                        lblTier8.setText(tier.getTierName());
                                        lblSeatingStanding8.setText(seatedStanding);
                                        break;
                                    case 9:
                                        lblTier9.setText(tier.getTierName());
                                        lblSeatingStanding9.setText(seatedStanding);
                                        break;
                                }
                                i++;
                            }
                        }
                    }
                }
            });
        }
    }

    private EventRun constructEventRun() throws ParseException {
        EventRun run = null;
        if (cbxEventVenue.getSelectedIndex() != -1) {
            if (dtpRunDate.getDate() != null) {
                if (cbxStartTimeHours.getSelectedIndex() != -1 && cbxStartTimeMinutes.getSelectedIndex() != -1) {
                    run = new EventRun();
                    String venueString = cbxEventVenue.getSelectedItem().toString();
                    int venueId = Integer.parseInt(venueString.split(" - ", 2)[0]);
                    Venue venue = venuesHash.get(venueId);
                    run.setVenue(venue);
                    Date dateTime = constructDate();
                    run.setRunDate(dateTime);
                    run.setDuration((int) spnDuration.getValue());
                    run.setActsPerforming(selectedPerformers);
                } else {
                    JOptionPane.showMessageDialog(this, "Please select a start Time for the new Run");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please select a Date for the new Run");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Venue for the new Run");
        }
        return run;
    }

    public Price constructPrice(int tierNo, Tier tier) {
        Price price = new Price();
        switch (tierNo) {
            case 1:
                if (!txtTierPrice1.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice1.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                    price.setTierID(tier.getTierId());

                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 2:
                if (!txtTierPrice2.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice2.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 3:
                if (!txtTierPrice3.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice3.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 4:
                if (!txtTierPrice4.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice4.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 5:
                if (!txtTierPrice5.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice5.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 6:
                if (!txtTierPrice6.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice6.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 7:
                if (!txtTierPrice7.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice7.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 8:
                if (!txtTierPrice8.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice8.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
                break;
            case 9:
                if (!txtTierPrice9.getText().isEmpty()) {
                    try {
                        price.setPrice(Double.parseDouble(txtTierPrice9.getText()));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Invalid Price entered " + tier.getTierName() + "Price set to £0 \n"
                                + "Please edit the run from the View existing Runs Page to add");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "No Price enter for " + tier.getTierName() + "Price set to £0 \n"
                            + "Please edit the run from the View existing Runs Page to add");
                }
        }
        price.setTierID(tier.getTierId());
        return price;
    }

    public String constructPriceJsonString(EventRun run, Price price) {
        if (run != null) {
            JSONObject j = new JSONObject();
            j.put("RUN_ID", run.getID());
            j.put("TIER_ID", price.getTierID());
            j.put("PRICE1", price.getPrice());
            return j.toString();
        } else {
            return null;
        }
    }

    public String constructRunJsonString(EventRun run, Event newEvent) throws ParseException, JSONException {
        if (run != null) {
            JSONObject j = new JSONObject();
            j.put("EVENT_ID", newEvent.getID());
            j.put("VENUE_ID", run.getVenue().getVenueID());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String formattedDate = formatter.format(run.getRunDate());
            j.put("RUN_DATE", formattedDate);
            j.put("DURATION", run.getDuration());
            return j.toString();
        } else {
            return null;
        }
    }

    public String constructPerformanceJsonString(Act performer, EventRun run) {
        JSONObject j = new JSONObject();
        j.put("ACT_ID", performer.getID());
        j.put("RUN_ID", run.getID());
        return j.toString();
    }

    private Date constructDate() throws ParseException {
        String date = dtpRunDate.getDate().toString();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dtpRunDate.setFormats(dateFormat);
        DateFormat sysDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date_to_store = sysDate.format(dtpRunDate.getDate());
        Date dateTime = sysDate.parse(date_to_store);
        dateTime.setHours(Integer.parseInt(cbxStartTimeHours.getSelectedItem().toString()));
        dateTime.setMinutes(Integer.parseInt(cbxStartTimeMinutes.getSelectedItem().toString()));
        sysDate.format(dateTime);
        return dateTime;
    }

    private void initialiseComboBoxModels() {
        LocalDateTime now = LocalDateTime.now();
        for (int i = 0; i < 24; i++) {
            Integer.toString(i);
            startTimeHoursModel.addElement(String.format("%1$02d", i));
        }
        for (int i = 0; i < 60; i++) {
            Integer.toString(i);
            startTimeMinutesModel.addElement(String.format("%1$02d", i));
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblAddNewTier = new javax.swing.JLabel();
        lblEvent = new javax.swing.JLabel();
        txtEvent = new javax.swing.JTextField();
        btnAddRun = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        pnlAdditionalRun = new javax.swing.JPanel();
        lblEventDate1 = new javax.swing.JLabel();
        lblStartTime1 = new javax.swing.JLabel();
        cbxEventVenue = new javax.swing.JComboBox();
        lvlEventVenue1 = new javax.swing.JLabel();
        cbxStartTimeHours = new javax.swing.JComboBox();
        cbxStartTimeMinutes = new javax.swing.JComboBox();
        lblRunInfo = new javax.swing.JLabel();
        dtpRunDate = new org.jdesktop.swingx.JXDatePicker();
        lblDuration = new javax.swing.JLabel();
        spnDuration = new javax.swing.JSpinner();
        pnlPerformers2 = new javax.swing.JPanel();
        scpAdditionalPerformers2 = new javax.swing.JScrollPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        lstAdditionalPerformers2 = new javax.swing.JList();
        pnlSearchFilter2 = new javax.swing.JPanel();
        lblSearch2 = new javax.swing.JLabel();
        txtSearchPerformers = new javax.swing.JTextField();
        cbxCriteria = new javax.swing.JComboBox();
        lblPerormerOverview2 = new javax.swing.JLabel();
        btnSearch = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        lblPerformers2 = new javax.swing.JLabel();
        scpPerformerOverview = new javax.swing.JScrollPane();
        txtAdditionalPerformers = new javax.swing.JTextArea();
        btnAddPerformer = new javax.swing.JButton();
        btnRemovePerformer = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        pnlTier1 = new javax.swing.JPanel();
        lblSeatingStanding1 = new javax.swing.JLabel();
        txtTierPrice1 = new javax.swing.JTextField();
        lbl£ = new javax.swing.JLabel();
        lblTier1 = new javax.swing.JLabel();
        pnlTier2 = new javax.swing.JPanel();
        lblSeatingStanding2 = new javax.swing.JLabel();
        txtTierPrice2 = new javax.swing.JTextField();
        lbl£4 = new javax.swing.JLabel();
        lblTier2 = new javax.swing.JLabel();
        pnlTier3 = new javax.swing.JPanel();
        lblSeatingStanding3 = new javax.swing.JLabel();
        txtTierPrice3 = new javax.swing.JTextField();
        lbl£5 = new javax.swing.JLabel();
        lblTier3 = new javax.swing.JLabel();
        pnlTier4 = new javax.swing.JPanel();
        lblSeatingStanding4 = new javax.swing.JLabel();
        txtTierPrice4 = new javax.swing.JTextField();
        lbl£6 = new javax.swing.JLabel();
        lblTier4 = new javax.swing.JLabel();
        pnlTier5 = new javax.swing.JPanel();
        lblSeatingStanding5 = new javax.swing.JLabel();
        txtTierPrice5 = new javax.swing.JTextField();
        lbl£7 = new javax.swing.JLabel();
        lblTier5 = new javax.swing.JLabel();
        pnlTier6 = new javax.swing.JPanel();
        lblSeatingStanding6 = new javax.swing.JLabel();
        txtTierPrice6 = new javax.swing.JTextField();
        lbl£8 = new javax.swing.JLabel();
        lblTier6 = new javax.swing.JLabel();
        pnlTier7 = new javax.swing.JPanel();
        lblSeatingStanding7 = new javax.swing.JLabel();
        txtTierPrice7 = new javax.swing.JTextField();
        lbl£9 = new javax.swing.JLabel();
        lblTier7 = new javax.swing.JLabel();
        pnlTier9 = new javax.swing.JPanel();
        lblSeatingStanding9 = new javax.swing.JLabel();
        txtTierPrice9 = new javax.swing.JTextField();
        lbl£10 = new javax.swing.JLabel();
        lblTier9 = new javax.swing.JLabel();
        pnlTier8 = new javax.swing.JPanel();
        lblSeatingStanding8 = new javax.swing.JLabel();
        txtTierPrice8 = new javax.swing.JTextField();
        lbl£11 = new javax.swing.JLabel();
        lblTier8 = new javax.swing.JLabel();

        lblAddNewTier.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblAddNewTier.setText("Add New Run");

        lblEvent.setText("In Event:");

        txtEvent.setEditable(false);

        btnAddRun.setText("Add Run");
        btnAddRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddRunActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel1.setText("Pricing:");

        lblEventDate1.setText("Date:");

        lblStartTime1.setText("Start Time:");

        cbxEventVenue.setModel(venuesComboModel);

        lvlEventVenue1.setText("Venue:");

        cbxStartTimeHours.setModel(startTimeHoursModel);

        cbxStartTimeMinutes.setModel(startTimeMinutesModel);

        lblRunInfo.setText("Run Information");

        lblDuration.setText("Duration (mins) :");

        spnDuration.setModel(new javax.swing.SpinnerNumberModel(15, 15, 4320, 15));

        javax.swing.GroupLayout pnlAdditionalRunLayout = new javax.swing.GroupLayout(pnlAdditionalRun);
        pnlAdditionalRun.setLayout(pnlAdditionalRunLayout);
        pnlAdditionalRunLayout.setHorizontalGroup(
            pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                        .addGap(127, 127, 127)
                        .addComponent(lblRunInfo))
                    .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                        .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblDuration)
                            .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                    .addGap(32, 32, 32)
                                    .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblEventDate1, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lvlEventVenue1, javax.swing.GroupLayout.Alignment.TRAILING)))
                                .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(lblStartTime1))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dtpRunDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxEventVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(spnDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlAdditionalRunLayout.createSequentialGroup()
                                .addComponent(cbxStartTimeHours, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbxStartTimeMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlAdditionalRunLayout.setVerticalGroup(
            pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAdditionalRunLayout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addComponent(lblRunInfo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lvlEventVenue1)
                    .addComponent(cbxEventVenue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEventDate1)
                    .addComponent(dtpRunDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxStartTimeHours, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxStartTimeMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStartTime1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlAdditionalRunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDuration)
                    .addComponent(spnDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        lstAdditionalPerformers2.setForeground(new java.awt.Color(255, 0, 0));
        lstAdditionalPerformers2.setModel(performersListModel);
        jScrollPane3.setViewportView(lstAdditionalPerformers2);

        scpAdditionalPerformers2.setViewportView(jScrollPane3);

        lblSearch2.setText("Search:");

        cbxCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "Title", "Genre" }));

        lblPerormerOverview2.setText("Performer Overview: ");

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnReset.setText("Reset");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        lblPerformers2.setText("Performers");

        javax.swing.GroupLayout pnlSearchFilter2Layout = new javax.swing.GroupLayout(pnlSearchFilter2);
        pnlSearchFilter2.setLayout(pnlSearchFilter2Layout);
        pnlSearchFilter2Layout.setHorizontalGroup(
            pnlSearchFilter2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSearchFilter2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSearchFilter2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSearchFilter2Layout.createSequentialGroup()
                        .addComponent(lblSearch2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtSearchPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSearchFilter2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblPerformers2)
                        .addGap(122, 122, 122)))
                .addGroup(pnlSearchFilter2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnReset, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSearchFilter2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblPerormerOverview2)
                .addGap(38, 38, 38))
        );
        pnlSearchFilter2Layout.setVerticalGroup(
            pnlSearchFilter2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSearchFilter2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSearchFilter2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnReset)
                    .addComponent(lblPerformers2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlSearchFilter2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSearch2)
                    .addComponent(txtSearchPerformers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(lblPerormerOverview2))
        );

        txtAdditionalPerformers.setEditable(false);
        txtAdditionalPerformers.setBackground(new java.awt.Color(245, 245, 245));
        txtAdditionalPerformers.setColumns(20);
        txtAdditionalPerformers.setRows(5);
        txtAdditionalPerformers.setAutoscrolls(false);
        scpPerformerOverview.setViewportView(txtAdditionalPerformers);

        btnAddPerformer.setText("Add Performer");
        btnAddPerformer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddPerformerActionPerformed(evt);
            }
        });

        btnRemovePerformer.setText("Remove Performer");
        btnRemovePerformer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemovePerformerActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlPerformers2Layout = new javax.swing.GroupLayout(pnlPerformers2);
        pnlPerformers2.setLayout(pnlPerformers2Layout);
        pnlPerformers2Layout.setHorizontalGroup(
            pnlPerformers2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPerformers2Layout.createSequentialGroup()
                .addGroup(pnlPerformers2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPerformers2Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(scpAdditionalPerformers2, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(scpPerformerOverview, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlPerformers2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlPerformers2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlPerformers2Layout.createSequentialGroup()
                                .addComponent(btnAddPerformer, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnRemovePerformer)
                                .addGap(18, 18, 18)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(pnlSearchFilter2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        pnlPerformers2Layout.setVerticalGroup(
            pnlPerformers2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPerformers2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(pnlSearchFilter2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPerformers2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scpPerformerOverview)
                    .addGroup(pnlPerformers2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(scpAdditionalPerformers2, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(19, 19, 19)
                .addGroup(pnlPerformers2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddPerformer)
                    .addComponent(btnRemovePerformer)
                    .addComponent(btnClear))
                .addContainerGap())
        );

        lblSeatingStanding1.setText("Seating/Standing");

        lbl£.setText("£");

        lblTier1.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier1Layout = new javax.swing.GroupLayout(pnlTier1);
        pnlTier1.setLayout(pnlTier1Layout);
        pnlTier1Layout.setHorizontalGroup(
            pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlTier1Layout.setVerticalGroup(
            pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl£)
                    .addComponent(lblSeatingStanding1)
                    .addComponent(lblTier1)))
        );

        lblSeatingStanding2.setText("Seating/Standing");

        lbl£4.setText("£");

        lblTier2.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier2Layout = new javax.swing.GroupLayout(pnlTier2);
        pnlTier2.setLayout(pnlTier2Layout);
        pnlTier2Layout.setHorizontalGroup(
            pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding2)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        pnlTier2Layout.setVerticalGroup(
            pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl£4)
                    .addComponent(lblSeatingStanding2)
                    .addComponent(lblTier2)))
        );

        lblSeatingStanding3.setText("Seating/Standing");

        lbl£5.setText("£");

        lblTier3.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier3Layout = new javax.swing.GroupLayout(pnlTier3);
        pnlTier3.setLayout(pnlTier3Layout);
        pnlTier3Layout.setHorizontalGroup(
            pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding3)
                .addContainerGap())
        );
        pnlTier3Layout.setVerticalGroup(
            pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier3)
                    .addComponent(lbl£5)
                    .addComponent(lblSeatingStanding3)))
        );

        lblSeatingStanding4.setText("Seating/Standing");

        lbl£6.setText("£");

        lblTier4.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier4Layout = new javax.swing.GroupLayout(pnlTier4);
        pnlTier4.setLayout(pnlTier4Layout);
        pnlTier4Layout.setHorizontalGroup(
            pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice4, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding4)
                .addContainerGap())
        );
        pnlTier4Layout.setVerticalGroup(
            pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier4)
                    .addComponent(lbl£6)
                    .addComponent(lblSeatingStanding4)))
        );

        lblSeatingStanding5.setText(" Seating/Standing");

        lbl£7.setText("£");

        lblTier5.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier5Layout = new javax.swing.GroupLayout(pnlTier5);
        pnlTier5.setLayout(pnlTier5Layout);
        pnlTier5Layout.setHorizontalGroup(
            pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice5, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding5)
                .addContainerGap())
        );
        pnlTier5Layout.setVerticalGroup(
            pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier5)
                    .addComponent(lbl£7)
                    .addComponent(lblSeatingStanding5)))
        );

        lblSeatingStanding6.setText("Seating/Standing");

        lbl£8.setText("£");

        lblTier6.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier6Layout = new javax.swing.GroupLayout(pnlTier6);
        pnlTier6.setLayout(pnlTier6Layout);
        pnlTier6Layout.setHorizontalGroup(
            pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice6, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding6)
                .addContainerGap())
        );
        pnlTier6Layout.setVerticalGroup(
            pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier6)
                    .addComponent(lbl£8)
                    .addComponent(lblSeatingStanding6)))
        );

        lblSeatingStanding7.setText("Seating/Standing");

        lbl£9.setText("£");

        lblTier7.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier7Layout = new javax.swing.GroupLayout(pnlTier7);
        pnlTier7.setLayout(pnlTier7Layout);
        pnlTier7Layout.setHorizontalGroup(
            pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice7, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding7)
                .addContainerGap())
        );
        pnlTier7Layout.setVerticalGroup(
            pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier7)
                    .addComponent(lbl£9)
                    .addComponent(lblSeatingStanding7)))
        );

        lblSeatingStanding9.setText("Seating/Standing");

        lbl£10.setText("£");

        lblTier9.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier9Layout = new javax.swing.GroupLayout(pnlTier9);
        pnlTier9.setLayout(pnlTier9Layout);
        pnlTier9Layout.setHorizontalGroup(
            pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice9, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding9)
                .addContainerGap())
        );
        pnlTier9Layout.setVerticalGroup(
            pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier9)
                    .addComponent(lbl£10)
                    .addComponent(lblSeatingStanding9)))
        );

        lblSeatingStanding8.setText("Seating/Standing");

        lbl£11.setText("£");

        lblTier8.setText("Tier 1:");

        javax.swing.GroupLayout pnlTier8Layout = new javax.swing.GroupLayout(pnlTier8);
        pnlTier8.setLayout(pnlTier8Layout);
        pnlTier8Layout.setHorizontalGroup(
            pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTier8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl£11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTierPrice8, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSeatingStanding8)
                .addContainerGap())
        );
        pnlTier8Layout.setVerticalGroup(
            pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTier8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTier8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTierPrice8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTier8)
                    .addComponent(lbl£11)
                    .addComponent(lblSeatingStanding8)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(pnlAdditionalRun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pnlPerformers2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(222, 222, 222)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pnlTier3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlTier2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlTier1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pnlTier5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlTier4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlTier6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pnlTier9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlTier7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlTier8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(lblAddNewTier, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(113, 113, 113)
                                .addComponent(lblEvent)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(35, 35, 35))
            .addGroup(layout.createSequentialGroup()
                .addGap(484, 484, 484)
                .addComponent(btnAddRun, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddNewTier)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEvent)
                    .addComponent(txtEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(pnlTier8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlTier9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(pnlAdditionalRun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(288, 288, 288))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(pnlPerformers2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(31, 31, 31)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAddRun)
                                .addComponent(btnCancel))
                            .addGap(9, 9, 9))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddRunActionPerformed
        try {
            if (httpHandler.authenticate(loginData)[0].contains("20")) {
                EventRun newRun = constructEventRun();
                String runData = constructRunJsonString(newRun, event);
                URL runUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/runs");
                String[] httpInfo = httpHandler.sendPOSTRequest(runData, runUrl);
                JSONObject runInfo = new JSONObject(httpInfo[1]);
                int runId = runInfo.getInt("ID");
                newRun.setID(runId);
                if (httpInfo[0].contains("20")) {
                    URL actUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/performances");
                    String[] performanceInfo = new String[selectedPerformers.size()];
                    int count = 0;
                    for (Act performer : selectedPerformers) {
                        String actData = constructPerformanceJsonString(performer, newRun);
                        String[] actHttpInfo = httpHandler.sendPOSTRequest(actData, actUrl);
                        performanceInfo[count] = actHttpInfo[0];

                        if (performanceInfo[count].contains("20")) {
                            count++;
                        } else {
                            JOptionPane.showMessageDialog(this, "ERROR Adding Performer to Run, please do this manually from edit events page");
                            break;
                        }
                    }
                    URL priceUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/prices");
                    String[] priceInfo = new String[selectedVenue.getTierList().size()];
                    int tierNo = 1;
                    int httpCount = 0;
                    for (Tier tier : selectedVenue.getTierList()) {
                        Price price = constructPrice(tierNo, tier);
                        String priceData = constructPriceJsonString(newRun, price);
                        String[] priceHttpInfo = httpHandler.sendPOSTRequest(priceData, priceUrl);
                        priceInfo[httpCount] = priceHttpInfo[0];
                        tierNo++;
                        if (priceInfo[httpCount].contains("20")) {
                        } else {
                            JOptionPane.showMessageDialog(this, "ERROR Adding Prices to Tiers, please do this manually from edit events page");
                            break;
                        }
                        httpCount++;
                    }

                    JOptionPane.showMessageDialog(this, httpInfo[0] + "\n Run was successfully added to " + event.getEventTitle());
                    cardLayout.removeLayoutComponent(this);
                    mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                    cardLayout.show(mainPanel, "ViewExistingEvents");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                mainPanel.removeAll();
                new LogIn().setVisible(true);
            }
        } catch (MalformedURLException ex) {
            JOptionPane.showMessageDialog(this, "ERROR, Run NOT Added \n" + ex);
        } catch (IOException | JSONException | ParseException ex) {
            JOptionPane.showMessageDialog(this, "ERROR, Run NOT Added \n" + ex);
        }
    }//GEN-LAST:event_btnAddRunActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel Adding new Run to " + event.getEventTitle() + "?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                mainPanel.remove(this);
                mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingEvents");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnAddPerformerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddPerformerActionPerformed
        if (selectedPerformer != null) {
            if (!selectedPerformers.contains(selectedPerformer)) {
                selectedPerformers.add(selectedPerformer);
                txtAdditionalPerformers.append(selectedPerformer.getActName() + ", ");
            } else {
                JOptionPane.showMessageDialog(this, "This Performer has already been added to this run!",
                        "", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please selected  Performer to Add to Run");
        }
    }//GEN-LAST:event_btnAddPerformerActionPerformed

    private void btnRemovePerformerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemovePerformerActionPerformed
        if (selectedPerformer != null) {
            if (selectedPerformers.contains(selectedPerformer)) {
                selectedPerformers.remove(selectedPerformer);
                txtAdditionalPerformers.setText("");
                for (Act performer : selectedPerformers) {
                    txtAdditionalPerformers.append(performer.getActName() + ", ");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Cannot remove a Performer not already on this run!",
                        "", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnRemovePerformerActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed

    }//GEN-LAST:event_btnClearActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        filteredList.clear();
        String searchCriteria = txtSearchPerformers.getText().toLowerCase();
        for (Act act : performersList) {
            if (cbxCriteria.getSelectedItem().toString().equals("Title")) {
                if (act.getActName().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(act);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("ID")) {
                try {
                    if (searchCriteria.isEmpty() || act.getID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(act);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Performer ID");
                    filterPerformersList(performersList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("Genre")) {
                if (act.getActType().getType().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(act);
                }
            }
        }
        filterPerformersList(filteredList);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        filterPerformersList(performersList);
    }//GEN-LAST:event_btnResetActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddPerformer;
    private javax.swing.JButton btnAddRun;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnRemovePerformer;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox cbxCriteria;
    private javax.swing.JComboBox cbxEventVenue;
    private javax.swing.JComboBox cbxStartTimeHours;
    private javax.swing.JComboBox cbxStartTimeMinutes;
    private org.jdesktop.swingx.JXDatePicker dtpRunDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblAddNewTier;
    private javax.swing.JLabel lblDuration;
    private javax.swing.JLabel lblEvent;
    private javax.swing.JLabel lblEventDate1;
    private javax.swing.JLabel lblPerformers2;
    private javax.swing.JLabel lblPerormerOverview2;
    private javax.swing.JLabel lblRunInfo;
    private javax.swing.JLabel lblSearch2;
    private javax.swing.JLabel lblSeatingStanding1;
    private javax.swing.JLabel lblSeatingStanding2;
    private javax.swing.JLabel lblSeatingStanding3;
    private javax.swing.JLabel lblSeatingStanding4;
    private javax.swing.JLabel lblSeatingStanding5;
    private javax.swing.JLabel lblSeatingStanding6;
    private javax.swing.JLabel lblSeatingStanding7;
    private javax.swing.JLabel lblSeatingStanding8;
    private javax.swing.JLabel lblSeatingStanding9;
    private javax.swing.JLabel lblStartTime1;
    private javax.swing.JLabel lblTier1;
    private javax.swing.JLabel lblTier2;
    private javax.swing.JLabel lblTier3;
    private javax.swing.JLabel lblTier4;
    private javax.swing.JLabel lblTier5;
    private javax.swing.JLabel lblTier6;
    private javax.swing.JLabel lblTier7;
    private javax.swing.JLabel lblTier8;
    private javax.swing.JLabel lblTier9;
    private javax.swing.JLabel lbl£;
    private javax.swing.JLabel lbl£10;
    private javax.swing.JLabel lbl£11;
    private javax.swing.JLabel lbl£4;
    private javax.swing.JLabel lbl£5;
    private javax.swing.JLabel lbl£6;
    private javax.swing.JLabel lbl£7;
    private javax.swing.JLabel lbl£8;
    private javax.swing.JLabel lbl£9;
    private javax.swing.JList lstAdditionalPerformers2;
    private javax.swing.JLabel lvlEventVenue1;
    private javax.swing.JPanel pnlAdditionalRun;
    private javax.swing.JPanel pnlPerformers2;
    private javax.swing.JPanel pnlSearchFilter2;
    private javax.swing.JPanel pnlTier1;
    private javax.swing.JPanel pnlTier2;
    private javax.swing.JPanel pnlTier3;
    private javax.swing.JPanel pnlTier4;
    private javax.swing.JPanel pnlTier5;
    private javax.swing.JPanel pnlTier6;
    private javax.swing.JPanel pnlTier7;
    private javax.swing.JPanel pnlTier8;
    private javax.swing.JPanel pnlTier9;
    private javax.swing.JScrollPane scpAdditionalPerformers2;
    private javax.swing.JScrollPane scpPerformerOverview;
    private javax.swing.JSpinner spnDuration;
    private javax.swing.JTextArea txtAdditionalPerformers;
    private javax.swing.JTextField txtEvent;
    private javax.swing.JTextField txtSearchPerformers;
    private javax.swing.JTextField txtTierPrice1;
    private javax.swing.JTextField txtTierPrice2;
    private javax.swing.JTextField txtTierPrice3;
    private javax.swing.JTextField txtTierPrice4;
    private javax.swing.JTextField txtTierPrice5;
    private javax.swing.JTextField txtTierPrice6;
    private javax.swing.JTextField txtTierPrice7;
    private javax.swing.JTextField txtTierPrice8;
    private javax.swing.JTextField txtTierPrice9;
    // End of variables declaration//GEN-END:variables
}
