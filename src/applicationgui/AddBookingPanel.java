package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Act;
import AdministrationApplication.Booking;
import AdministrationApplication.Customer;
import AdministrationApplication.Event;
import AdministrationApplication.EventRun;
import AdministrationApplication.SeatedTicket;
import AdministrationApplication.StandingTicket;
import AdministrationApplication.Tier;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */

public class AddBookingPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private DefaultComboBoxModel userComboBoxModel;
    private DefaultComboBoxModel eventComboBoxModel;
    private DefaultComboBoxModel runComboBoxModel;
    private DefaultComboBoxModel tierComboBoxModel;
    private HashMap<Integer, Event> eventsHash;
    private HashMap<Integer, Customer> customerHash;
    private HashMap<Integer, EventRun> runHash;
    private HashMap<Integer, Tier> tierHash;
    private List<Customer> customersList;
    private List<Customer> filteredCustomerList;
    private List<Event> eventsList;
    private List<Event> filteredEventList;
    private EventRun selectedRun;
    private Customer selectedCustomer;
    private Tier selectedTier;
    private JsonClassGenerator generator;
    private final String loginData;
    private HTTPRequestHandler httpHandler;

    public AddBookingPanel(CardLayout layoutCard, JPanel panel, String data) throws IOException {
        cardLayout = layoutCard;
        mainPanel = panel;
        userComboBoxModel = new DefaultComboBoxModel();
        eventComboBoxModel = new DefaultComboBoxModel();
        runComboBoxModel = new DefaultComboBoxModel();
        tierComboBoxModel = new DefaultComboBoxModel();
        filteredCustomerList = new ArrayList();
        filteredEventList = new ArrayList();
        eventsHash = new HashMap<>();
        customerHash = new HashMap<>();
        runHash = new HashMap<>();
        tierHash = new HashMap<>();
        initComponents();
        generator = new JsonClassGenerator();
        customersList = generator.generateCustomers();
        fillUsersComboBox(customersList);
        eventsList = generator.generateEvents();
        fillEventsComboBox(eventsList);
        loginData = data;
        httpHandler = new HTTPRequestHandler();
        txtPerformers.setLineWrap(true);
    }

    private void fillUsersComboBox(List<Customer> customersList) throws IOException {
        userComboBoxModel.removeAllElements();
        for (Customer customer : customersList) {
            customerHash.put(customer.getID(), customer);
            userComboBoxModel.addElement(customer.getID() + " - " + customer.getForename() + " " + customer.getSurname() + " - "
                    + customer.getEmail());
        }
    }

    private void fillEventsComboBox(List<Event> eventsList) throws IOException {
        eventComboBoxModel.removeAllElements();
        for (Event event : eventsList) {
            eventsHash.put(event.getID(), event);
            eventComboBoxModel.addElement(event.getID() + " - " + event.getEventTitle());
        }
    }

    private String constructBookingJsonString(Booking newBooking) {
        JSONObject j = new JSONObject();
        j.put("runID", newBooking.getRun().getID());
        j.put("personID", newBooking.getCustomer().getID());
        j.put("tierID", selectedTier.getTierId());
        j.put("quantity", newBooking.getNoOfTickets());
        j.put("totalCost", newBooking.getTotalCost());
        return j.toString();
    }

    private void refresh() {
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("AddNewBooking", new AddBookingPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "AddNewBooking");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btngrpSeatingType = new javax.swing.ButtonGroup();
        lblCreateBooking = new javax.swing.JLabel();
        lblUsername = new javax.swing.JLabel();
        lblEvent = new javax.swing.JLabel();
        lblQuantity = new javax.swing.JLabel();
        lblTotalPrice = new javax.swing.JLabel();
        txtCost = new javax.swing.JTextField();
        btnCancel = new javax.swing.JButton();
        btnMakeBooking = new javax.swing.JButton();
        lblRegister = new javax.swing.JLabel();
        btnRegisterCustomer = new javax.swing.JButton();
        lblGuest = new javax.swing.JLabel();
        lblTier = new javax.swing.JLabel();
        txtSearchCustomer = new javax.swing.JTextField();
        txtSearchEvent = new javax.swing.JTextField();
        spnQuantity = new javax.swing.JSpinner();
        btnGuestBooking = new javax.swing.JButton();
        cbxUser = new javax.swing.JComboBox();
        cbxEvent = new javax.swing.JComboBox();
        cbxTier = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        lblTierType = new javax.swing.JLabel();
        btnCustSearch = new javax.swing.JButton();
        btnEventSearch = new javax.swing.JButton();
        lblRun = new javax.swing.JLabel();
        cbxRun = new javax.swing.JComboBox();
        pnlRunDetail = new javax.swing.JPanel();
        txtVenue = new javax.swing.JTextField();
        lblVenue = new javax.swing.JLabel();
        lblPerformances = new javax.swing.JLabel();
        scpPerformances = new javax.swing.JScrollPane();
        txtPerformers = new javax.swing.JTextArea();
        pnlCustDetail = new javax.swing.JPanel();
        txtAddressLine1 = new javax.swing.JTextField();
        lblAddrLine1 = new javax.swing.JLabel();
        lblAddrLine2 = new javax.swing.JLabel();
        txtAddressLine2 = new javax.swing.JTextField();
        lblAddrCity = new javax.swing.JLabel();
        lblAddrCounty = new javax.swing.JLabel();
        lblAddrPostcode = new javax.swing.JLabel();
        txtCity = new javax.swing.JTextField();
        txtCounty = new javax.swing.JTextField();
        txtPostcode = new javax.swing.JTextField();
        btnRefresh = new javax.swing.JButton();
        cbxEventCriteria = new javax.swing.JComboBox();
        cbxCustomerCriteria = new javax.swing.JComboBox();
        btnResetCustomerSearch = new javax.swing.JButton();
        btnResetEventSearch = new javax.swing.JButton();

        lblCreateBooking.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCreateBooking.setText("Create new booking");

        lblUsername.setText("Customer:");

        lblEvent.setText("Event:");

        lblQuantity.setText("Quantity:");

        lblTotalPrice.setText("Total Cost:");

        txtCost.setEditable(false);
        txtCost.setBackground(new java.awt.Color(238, 238, 238));
        txtCost.setText("£0.00");

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnMakeBooking.setText("Make Booking");
        btnMakeBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMakeBookingActionPerformed(evt);
            }
        });

        lblRegister.setText("Does this Customer already have an Account? If not Click here   --->");

        btnRegisterCustomer.setText("Register New Customer");
        btnRegisterCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterCustomerActionPerformed(evt);
            }
        });

        lblGuest.setText("Or Continue as Guest....");

        lblTier.setText("Tier:");

        spnQuantity.setModel(new javax.swing.SpinnerNumberModel(0, 0, 9, 1));
        spnQuantity.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spnQuantityStateChanged(evt);
            }
        });

        btnGuestBooking.setText("Book as Guest");
        btnGuestBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuestBookingActionPerformed(evt);
            }
        });

        cbxUser.setModel(userComboBoxModel);
        cbxUser.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxUserItemStateChanged(evt);
            }
        });

        cbxEvent.setModel(eventComboBoxModel);
        cbxEvent.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxEventItemStateChanged(evt);
            }
        });

        cbxTier.setModel(tierComboBoxModel);
        cbxTier.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxTierItemStateChanged(evt);
            }
        });

        lblTierType.setText("This Tier is Seated/Standing");

        btnCustSearch.setText("Search");
        btnCustSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustSearchActionPerformed(evt);
            }
        });

        btnEventSearch.setText("Search");
        btnEventSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventSearchActionPerformed(evt);
            }
        });

        lblRun.setText("Run:");

        cbxRun.setModel(runComboBoxModel);
        cbxRun.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxRunItemStateChanged(evt);
            }
        });

        pnlRunDetail.setBorder(javax.swing.BorderFactory.createTitledBorder("Run Detail"));

        txtVenue.setEditable(false);
        txtVenue.setBackground(new java.awt.Color(238, 238, 238));

        lblVenue.setText("Venue:");

        lblPerformances.setText("Performances:");

        txtPerformers.setBackground(new java.awt.Color(245, 245, 245));
        txtPerformers.setColumns(20);
        txtPerformers.setRows(5);
        scpPerformances.setViewportView(txtPerformers);

        javax.swing.GroupLayout pnlRunDetailLayout = new javax.swing.GroupLayout(pnlRunDetail);
        pnlRunDetail.setLayout(pnlRunDetailLayout);
        pnlRunDetailLayout.setHorizontalGroup(
            pnlRunDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRunDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblVenue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblPerformances)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scpPerformances, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlRunDetailLayout.setVerticalGroup(
            pnlRunDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRunDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRunDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRunDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtVenue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblVenue)
                        .addComponent(lblPerformances))
                    .addComponent(scpPerformances, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(9, Short.MAX_VALUE))
        );

        pnlCustDetail.setBorder(javax.swing.BorderFactory.createTitledBorder("Customer Detail"));

        txtAddressLine1.setEditable(false);
        txtAddressLine1.setBackground(new java.awt.Color(238, 238, 238));

        lblAddrLine1.setText("Address Line 1 :");

        lblAddrLine2.setText("Address Line 2:");

        txtAddressLine2.setEditable(false);
        txtAddressLine2.setBackground(new java.awt.Color(238, 238, 238));

        lblAddrCity.setText("City:");

        lblAddrCounty.setText("County:");

        lblAddrPostcode.setText("Postcode:");

        txtCity.setEditable(false);
        txtCity.setBackground(new java.awt.Color(238, 238, 238));

        txtCounty.setEditable(false);
        txtCounty.setBackground(new java.awt.Color(238, 238, 238));

        txtPostcode.setEditable(false);
        txtPostcode.setBackground(new java.awt.Color(238, 238, 238));

        javax.swing.GroupLayout pnlCustDetailLayout = new javax.swing.GroupLayout(pnlCustDetail);
        pnlCustDetail.setLayout(pnlCustDetailLayout);
        pnlCustDetailLayout.setHorizontalGroup(
            pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCustDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlCustDetailLayout.createSequentialGroup()
                        .addGroup(pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlCustDetailLayout.createSequentialGroup()
                                .addComponent(lblAddrLine1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlCustDetailLayout.createSequentialGroup()
                                .addComponent(lblAddrLine2)
                                .addGap(18, 18, 18)
                                .addComponent(txtAddressLine2)))
                        .addGap(18, 18, 18)
                        .addGroup(pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblAddrCounty)
                            .addComponent(lblAddrCity)))
                    .addComponent(lblAddrPostcode))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtCity, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                    .addComponent(txtCounty)
                    .addComponent(txtPostcode))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlCustDetailLayout.setVerticalGroup(
            pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCustDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlCustDetailLayout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addGroup(pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAddrPostcode)
                            .addComponent(txtPostcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlCustDetailLayout.createSequentialGroup()
                        .addGroup(pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblAddrLine1)
                            .addComponent(lblAddrCity)
                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlCustDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAddrLine2)
                            .addComponent(txtAddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblAddrCounty)
                            .addComponent(txtCounty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/refresh-icon-2.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        cbxEventCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "EVENT TITLE" }));

        cbxCustomerCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "FIRST NAME", "LAST NAME" }));

        btnResetCustomerSearch.setText("Reset Search");
        btnResetCustomerSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetCustomerSearchActionPerformed(evt);
            }
        });

        btnResetEventSearch.setText("Reset Search");
        btnResetEventSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetEventSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblUsername)
                    .addComponent(lblEvent)
                    .addComponent(lblRun)
                    .addComponent(lblTier)
                    .addComponent(lblQuantity))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(cbxUser, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSearchCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cbxCustomerCriteria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(pnlCustDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 688, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbxEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbxRun, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtSearchEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cbxEventCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnResetCustomerSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCustSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnEventSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnResetEventSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(pnlRunDetail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(lblRegister)
                        .addGap(18, 18, 18)
                        .addComponent(btnRegisterCustomer)
                        .addGap(18, 18, 18)
                        .addComponent(lblGuest)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnGuestBooking, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblCreateBooking)))
                .addGap(100, 100, 100)
                .addComponent(btnRefresh)
                .addGap(127, 127, 127))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(110, 110, 110)
                        .addComponent(spnQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(lblTotalPrice)
                        .addGap(18, 18, 18)
                        .addComponent(txtCost, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(256, 256, 256)
                        .addComponent(btnMakeBooking, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(69, 69, 69)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(110, 110, 110)
                        .addComponent(cbxTier, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblTierType))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 1268, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblCreateBooking)
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblRegister)
                            .addComponent(btnRegisterCustomer)
                            .addComponent(lblGuest)
                            .addComponent(btnGuestBooking)))
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearchCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCustSearch)
                    .addComponent(lblUsername)
                    .addComponent(cbxCustomerCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnResetCustomerSearch)
                    .addComponent(pnlCustDetail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEvent)
                    .addComponent(cbxEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearchEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxEventCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEventSearch))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxRun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRun)
                    .addComponent(btnResetEventSearch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlRunDetail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTier)
                    .addComponent(cbxTier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTierType))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spnQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotalPrice)
                    .addComponent(txtCost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMakeBooking)
                    .addComponent(btnCancel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingBookings", new ViewExistingBookingsPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingBookings");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnMakeBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMakeBookingActionPerformed
        if (cbxUser.getSelectedIndex() != -1) {
            if (cbxEvent.getSelectedIndex() != -1) {
                if (cbxRun.getSelectedIndex() != -1) {
                    if (cbxTier.getSelectedIndex() != -1) {
                        if ((int) spnQuantity.getValue() > 0 && (int) spnQuantity.getValue() < 10) {
                            try {
                                if (httpHandler.authenticate(loginData)[0].contains("20")) {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                                    Date date = new Date();
                                    double cost = Double.parseDouble(txtCost.getText().replace("£", ""));
                                    Booking newBooking = new Booking(selectedRun, selectedCustomer, date, (int) spnQuantity.getValue(), cost);
                                    String data = constructBookingJsonString(newBooking);
                                    URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/bookings/CreateBooking");
                                    String[] httpInfo = httpHandler.sendPOSTRequest(data, url);
                                    if (httpInfo[0].contains("20")) {
                                        JSONObject booking = new JSONObject(httpInfo[1]);
                                        List<String> ticketDetails = new ArrayList();
                                        for (int i = 0; i < booking.getJSONArray("TICKETS").length(); i++) {
                                            JSONObject ticketsObject = booking.getJSONArray("TICKETS").getJSONObject(i);
                                            if (selectedTier.getIsSeated() == true) {
                                                SeatedTicket ticket = new SeatedTicket();
                                                ticket.setTicketRef(ticketsObject.getInt("ID"));
                                                try {
                                                    ticket.setBookingID(ticketsObject.getInt("BOOKING_ID"));
                                                } catch (JSONException ex) {
                                                }
                                                ticket.setAllocatedTier(selectedTier);
                                                ticket.setSeatRow(ticketsObject.getString("SEAT_ROW"));
                                                ticket.setSeatNo(ticketsObject.getInt("SEAT_NUM"));
                                                ticketDetails.add("Ticket Ref: " + ticket.getTicketRef() + ", Seat Row: " + ticket.getSeatRow()
                                                        + ", Seat Number: " + ticket.getSeatNo() + ", in Tier: " + ticket.getAllocatedTier().getTierName() + "\n");
                                            } else {
                                                StandingTicket ticket = new StandingTicket();
                                                ticket.setTicketRef(ticketsObject.getInt("ID"));
                                                try {
                                                    ticket.setBookingID(ticketsObject.getInt("BOOKING_ID"));
                                                } catch (JSONException ex) {
                                                }
                                                ticket.setAllocatedTier(selectedTier);
                                                ticketDetails.add("Ticket Ref: " + ticket.getTicketRef()
                                                        + " in Tier: " + ticket.getAllocatedTier().getTierName() + "\n");
                                            }
                                        }
                                        StringBuilder ticketOverview = new StringBuilder();
                                        for (String ticket : ticketDetails) {
                                            ticketOverview.append(ticket);
                                        }
                                        JOptionPane.showMessageDialog(this, ticketOverview);
                                        int confirmationMessage = JOptionPane.showConfirmDialog(this, httpInfo[0]
                                                + "Booking successfully Made, Would you like to make another? ", "Booking Successful", JOptionPane.OK_CANCEL_OPTION);
                                        if (confirmationMessage == JOptionPane.YES_OPTION) {
                                            refresh();
                                        } else {
                                            cardLayout.removeLayoutComponent(this);
                                            mainPanel.add("ViewExistingBookings", new ViewExistingBookingsPanel(cardLayout, mainPanel, loginData));
                                            cardLayout.show(mainPanel, "ViewExistingBookings");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(this, httpInfo[0] + "\n Booking NOT added");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                                    mainPanel.removeAll();
                                    new LogIn().setVisible(true);
                                }
                            } catch (IOException ex) {
                                JOptionPane.showMessageDialog(this, "ERROR Booking NOT Made \n" + ex);
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Must have a ticket quantity between 1 and 9");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Please select a Tier for the new Booking");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please select a Run for the new Booking");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please select an Event for the new Booking");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Customer for the new Booking");
        }
    }//GEN-LAST:event_btnMakeBookingActionPerformed

    private void btnRegisterCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterCustomerActionPerformed
        cardLayout.removeLayoutComponent(this);
        mainPanel.add("RegisterNewCustomer", new AddCustomerPanel(cardLayout, mainPanel, loginData));
        cardLayout.show(mainPanel, "RegisterNewCustomer");
    }//GEN-LAST:event_btnRegisterCustomerActionPerformed

    private void cbxEventItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxEventItemStateChanged
        lblTierType.setText("This Tier is Seated");
        txtPerformers.setText("");
        txtVenue.setText("");
        spnQuantity.setValue(0);
        runComboBoxModel.removeAllElements();
        tierComboBoxModel.removeAllElements();
        runHash.clear();
        if (eventComboBoxModel.getSize() != 0) {
            String selectedEventString = cbxEvent.getSelectedItem().toString();
            int eventId = Integer.parseInt(selectedEventString.split(" - ", 2)[0]);
            Event selectedEvent = eventsHash.get(eventId);
            for (EventRun run : selectedEvent.getEventRuns()) {
                runHash.put(run.getID(), run);
                runComboBoxModel.addElement(run.getID() + " - " + run.getRunDate());
            }
        }
        if (runComboBoxModel.getSize() == 0) {
            runComboBoxModel.addElement("Sorry there are currently no Runs for this Event");
            spnQuantity.setEnabled(false);
            cbxTier.setEnabled(false);
        }
    }//GEN-LAST:event_cbxEventItemStateChanged

    private void cbxRunItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxRunItemStateChanged
        lblTierType.setText("This Tier is Seated");
        tierComboBoxModel.removeAllElements();
        txtPerformers.setText("");
        txtVenue.setText("");
        cbxTier.setEnabled(true);
        spnQuantity.setEnabled(true);
        spnQuantity.setValue(0);
        tierComboBoxModel.removeAllElements();
        if (runComboBoxModel.getSize() != 0) {
            if (!runComboBoxModel.getElementAt(0).toString().equals("Sorry there are currently no Runs for this Event")) {
                String selectedRunString = cbxRun.getSelectedItem().toString();
                int runId = Integer.parseInt(selectedRunString.split(" - ", 2)[0]);
                selectedRun = runHash.get(runId);
                txtVenue.setText(selectedRun.getVenue().getVenueName());
                for (Act act : selectedRun.getActsPerforming()) {
                    txtPerformers.append(act.getActName() + ", ");
                }
                if (!selectedRun.getVenue().getTierList().isEmpty()) {
                    for (Tier tier : selectedRun.getVenue().getTierList()) {
                        tierHash.put(tier.getTierId(), tier);
                        if (tier.getTierTicketCost() != null) {
                            tierComboBoxModel.addElement(tier.getTierId() + " - " + tier.getTierName() + " @ £" + tier.getTierTicketCost().getPrice());
                        }
                    }
                } 
                cbxTier.setEnabled(true);
            } else {
                cbxTier.setEnabled(false);
            }
        }

    }//GEN-LAST:event_cbxRunItemStateChanged

    private void cbxUserItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxUserItemStateChanged
        txtAddressLine1.setText("");
        txtAddressLine2.setText("");
        txtCity.setText("");
        txtCounty.setText("");
        txtPostcode.setText("");
        if (cbxUser.getSelectedIndex() != -1) {
            String selectedCustomerString = cbxUser.getSelectedItem().toString();
            int custId = Integer.parseInt(selectedCustomerString.split(" - ", 2)[0]);
            selectedCustomer = customerHash.get(custId);
            txtAddressLine1.setText(selectedCustomer.getAddress().getAddressLine1());
            txtAddressLine2.setText(selectedCustomer.getAddress().getAddressLine2());
            txtCity.setText(selectedCustomer.getAddress().getCity());
            txtCounty.setText(selectedCustomer.getAddress().getCounty());
            txtPostcode.setText(selectedCustomer.getAddress().getPostcode());
        }
    }//GEN-LAST:event_cbxUserItemStateChanged

    private void cbxTierItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxTierItemStateChanged
        spnQuantity.setValue(0);
        if (cbxTier.getSelectedIndex() != -1) {
            String selectedTierString = cbxTier.getSelectedItem().toString();
            try {
                int tierId = Integer.parseInt(selectedTierString.split(" - ", 2)[0]);
                selectedTier = tierHash.get(tierId);
                if (selectedTier.getIsSeated() == true) {
                    lblTierType.setText("This Tier is Seated");
                } else {
                    lblTierType.setText("This Tier is Standing");
                }
            } catch (NumberFormatException ex) {
            }
        }
    }//GEN-LAST:event_cbxTierItemStateChanged

    private void spnQuantityStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnQuantityStateChanged
        txtCost.setText("£0.00");
        double totalCost = (int) spnQuantity.getValue() * selectedTier.getTierTicketCost().getPrice();
        txtCost.setText("£" + totalCost);
    }//GEN-LAST:event_spnQuantityStateChanged

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("NewBooking", new AddBookingPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "NewBooking");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnCustSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustSearchActionPerformed
        try {
            filteredCustomerList.clear();
            String searchCriteria = txtSearchCustomer.getText().toLowerCase().replaceAll(" ", "");
            for (Customer customer : customersList) {
                if (cbxCustomerCriteria.getSelectedItem().toString().equals("FIRST NAME")) {
                    if (customer.getForename().toLowerCase().contains(searchCriteria)) {
                        filteredCustomerList.add(customer);
                    }
                } else if (cbxCustomerCriteria.getSelectedItem().toString().equals("LAST NAME")) {
                    if (customer.getSurname().toLowerCase().contains(searchCriteria)) {
                        filteredCustomerList.add(customer);
                    }
                } else if (searchCriteria.isEmpty() || cbxCustomerCriteria.getSelectedItem().toString().equals("ID")) {
                    try {
                        if (customer.getID() == Integer.parseInt(searchCriteria)) {
                            filteredCustomerList.add(customer);
                        }
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(this, "Please enter a number to search Performer ID");
                        fillUsersComboBox(customersList);
                        break;
                    }
                }
            }
            fillUsersComboBox(filteredCustomerList);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnCustSearchActionPerformed

    private void btnEventSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventSearchActionPerformed
        try {
            filteredEventList.clear();
            String searchCriteria = txtSearchEvent.getText().toLowerCase();
            for (Event event : eventsList) {
                if (cbxEventCriteria.getSelectedItem().toString().equals("ID")) {
                    if (searchCriteria.isEmpty() || event.getID() == Integer.parseInt(searchCriteria)) {
                        filteredEventList.add(event);
                    }
                } else if (cbxEventCriteria.getSelectedItem().toString().equals("EVENT TITLE")) {
                    if (event.getEventTitle().toLowerCase().contains(searchCriteria)) {
                        filteredEventList.add(event);
                    }
                }
            }
            fillEventsComboBox(filteredEventList);
        } catch (IOException ex) {
            Logger.getLogger(AddBookingPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnEventSearchActionPerformed

    private void btnResetCustomerSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetCustomerSearchActionPerformed
        try {
            fillUsersComboBox(customersList);
            txtSearchCustomer.setText("");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnResetCustomerSearchActionPerformed

    private void btnResetEventSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetEventSearchActionPerformed
        try {
            fillEventsComboBox(eventsList);
            txtSearchEvent.setText("");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnResetEventSearchActionPerformed

    private void btnGuestBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuestBookingActionPerformed
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("NewGuestBooking", new AddGuestBookingPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "NewGuestBooking");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnGuestBookingActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCustSearch;
    private javax.swing.JButton btnEventSearch;
    private javax.swing.JButton btnGuestBooking;
    private javax.swing.JButton btnMakeBooking;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnRegisterCustomer;
    private javax.swing.JButton btnResetCustomerSearch;
    private javax.swing.JButton btnResetEventSearch;
    private javax.swing.ButtonGroup btngrpSeatingType;
    private javax.swing.JComboBox cbxCustomerCriteria;
    private javax.swing.JComboBox cbxEvent;
    private javax.swing.JComboBox cbxEventCriteria;
    private javax.swing.JComboBox cbxRun;
    private javax.swing.JComboBox cbxTier;
    private javax.swing.JComboBox cbxUser;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblAddrCity;
    private javax.swing.JLabel lblAddrCounty;
    private javax.swing.JLabel lblAddrLine1;
    private javax.swing.JLabel lblAddrLine2;
    private javax.swing.JLabel lblAddrPostcode;
    private javax.swing.JLabel lblCreateBooking;
    private javax.swing.JLabel lblEvent;
    private javax.swing.JLabel lblGuest;
    private javax.swing.JLabel lblPerformances;
    private javax.swing.JLabel lblQuantity;
    private javax.swing.JLabel lblRegister;
    private javax.swing.JLabel lblRun;
    private javax.swing.JLabel lblTier;
    private javax.swing.JLabel lblTierType;
    private javax.swing.JLabel lblTotalPrice;
    private javax.swing.JLabel lblUsername;
    private javax.swing.JLabel lblVenue;
    private javax.swing.JPanel pnlCustDetail;
    private javax.swing.JPanel pnlRunDetail;
    private javax.swing.JScrollPane scpPerformances;
    private javax.swing.JSpinner spnQuantity;
    private javax.swing.JTextField txtAddressLine1;
    private javax.swing.JTextField txtAddressLine2;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtCost;
    private javax.swing.JTextField txtCounty;
    private javax.swing.JTextArea txtPerformers;
    private javax.swing.JTextField txtPostcode;
    private javax.swing.JTextField txtSearchCustomer;
    private javax.swing.JTextField txtSearchEvent;
    private javax.swing.JTextField txtVenue;
    // End of variables declaration//GEN-END:variables
}
