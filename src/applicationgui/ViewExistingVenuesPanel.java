package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Tier;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import AdministrationApplication.Venue;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;

/**
 *
 * @author Joel Gooch
 */

public class ViewExistingVenuesPanel extends javax.swing.JPanel {

    private HashMap<Integer, Venue> venuesHashMap;
    private HashMap<Integer, Tier> tiersHashMap;
    private List<Venue> venueList;
    private List<Venue> filteredList;
    private DefaultTableModel venuesModel;
    private DefaultTableModel tiersModel;
    private CardLayout cardLayout;
    private JPanel mainPanel;
    private Venue selectedVenue;
    private Tier selectedTier;
    private final String loginData;
    private HTTPRequestHandler httpHandler;

    public ViewExistingVenuesPanel(CardLayout layoutCard, JPanel main, String data) throws IOException, JSONException {
        cardLayout = layoutCard;
        mainPanel = main;
        initComponents();
        initialiseTables();
        venuesHashMap = new HashMap<>();
        tiersHashMap = new HashMap<>();
        JsonClassGenerator generator = new JsonClassGenerator();
        filteredList = new ArrayList();
        venueList = generator.generateVenues();
        for (Venue venue : venueList) {
            venuesHashMap.put(venue.getVenueID(), venue);
        }
        populateVenuesTable(venueList);
        loginData = data;
        httpHandler = new HTTPRequestHandler();
    }

    private void initialiseTables() {
        venuesModel = (DefaultTableModel) tblVenues.getModel();
        tiersModel = (DefaultTableModel) tblTiers.getModel();
        ListSelectionModel venueSelectionModel = tblVenues.getSelectionModel();
        venueSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    tiersModel.setNumRows(0);
                    if (tblVenues.getSelectedRow() != -1) {
                        int selectedVenueID = (int) tblVenues.getValueAt(tblVenues.getSelectedRow(), 0);
                        selectedVenue = venuesHashMap.get(selectedVenueID);
                        tiersHashMap.clear();
                        for (Tier tier : selectedVenue.getTierList()) {
                            tiersHashMap.put(tier.getTierId(), tier);
                        }
                        populateTiersTable(selectedVenue);
                    }
                }
            }
        });
        ListSelectionModel tierSelectionModel = tblTiers.getSelectionModel();
        tierSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (tblTiers.getSelectedRow() != -1) {
                        int selectedTierID = (int) tblTiers.getValueAt(tblTiers.getSelectedRow(), 0);
                        selectedTier = tiersHashMap.get(selectedTierID);
                    }
                }
            }
        });

        setTableColumnWidths();
    }

    private void populateVenuesTable(List<Venue> venueList) {
        venuesModel.setNumRows(0);
        Object[] row = new Object[9];
        for (int i = 0; i < venueList.size(); i++) {
            row[0] = venueList.get(i).getVenueID();
            row[1] = venueList.get(i).getVenueName();
            row[2] = venueList.get(i).getVenueAddress().getAddressLine1();
            row[3] = venueList.get(i).getVenueAddress().getCity();
            row[4] = venueList.get(i).getVenueAddress().getCounty();
            row[5] = venueList.get(i).getVenueAddress().getPostcode();
            row[6] = venueList.get(i).getCapacity();
            row[7] = venueList.get(i).getSeatedCapacity();
            row[8] = venueList.get(i).getStandingCapacity();
            venuesModel.addRow(row);
        }
    }

    private void populateTiersTable(Venue selectedVenue) {
        tiersModel.setNumRows(0);
        List<Tier> tierList = selectedVenue.getTierList();
        Object[] row = new Object[6];
        for (int i = 0; i < tierList.size(); i++) {
            row[0] = tierList.get(i).getTierId();
            row[1] = tierList.get(i).getTierName();
            if (tierList.get(i).getIsSeated() == true) {
                row[2] = "Yes";
            } else {
                row[2] = "No";
            }
            row[3] = tierList.get(i).getCapacity();
            row[4] = tierList.get(i).getSeatRows();
            row[5] = tierList.get(i).getSeatColumns();
            tiersModel.addRow(row);
        }
    }

    private void setTableColumnWidths() {
        tblVenues.getColumnModel().getColumn(0).setPreferredWidth(40);
        tblVenues.getColumnModel().getColumn(1).setPreferredWidth(160);
        tblVenues.getColumnModel().getColumn(2).setPreferredWidth(160);
        tblVenues.getColumnModel().getColumn(3).setPreferredWidth(80);
        tblVenues.getColumnModel().getColumn(4).setPreferredWidth(80);
        tblVenues.getColumnModel().getColumn(5).setPreferredWidth(80);
        tblVenues.getColumnModel().getColumn(6).setPreferredWidth(60);
        tblVenues.getColumnModel().getColumn(7).setPreferredWidth(50);
        tblVenues.getColumnModel().getColumn(8).setPreferredWidth(60);
        tblTiers.getColumnModel().getColumn(0).setPreferredWidth(40);
        tblTiers.getColumnModel().getColumn(1).setPreferredWidth(150);
        tblTiers.getColumnModel().getColumn(2).setPreferredWidth(70);
        tblTiers.getColumnModel().getColumn(3).setPreferredWidth(70);
        tblTiers.getColumnModel().getColumn(4).setPreferredWidth(70);
        tblTiers.getColumnModel().getColumn(5).setPreferredWidth(90);
    }

    private void refreshPage() {
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("ViewExistingVenues", new ViewExistingVenuesPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "ViewExistingVenues");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblVenues = new javax.swing.JLabel();
        lblSearchVenues = new javax.swing.JLabel();
        txtSearchVenues = new javax.swing.JTextField();
        cbxCriteria = new javax.swing.JComboBox();
        scpVenues = new javax.swing.JScrollPane();
        tblVenues = new javax.swing.JTable();
        scpTiers = new javax.swing.JScrollPane();
        tblTiers = new javax.swing.JTable();
        txtTiers = new javax.swing.JLabel();
        txtVenues = new javax.swing.JLabel();
        btnAddTier = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnEditTier = new javax.swing.JButton();
        btnDeleteTier = new javax.swing.JButton();
        btnAddVenue = new javax.swing.JButton();
        btnEditVenue = new javax.swing.JButton();
        btnDeleteVenue = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();

        lblVenues.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblVenues.setText("View Venues");

        lblSearchVenues.setText("Search");

        cbxCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "VENUE NAME", "ADDRESS LINE 1", "CITY", "COUNTY", "POSTCODE" }));

        tblVenues.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name", "Address Line 1", "City", "County", "Postcode", "Capacity", "Seats", "Standing"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        scpVenues.setViewportView(tblVenues);

        tblTiers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name", "Seated", "Capacity", "Rows", "Columns"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        scpTiers.setViewportView(tblTiers);

        txtTiers.setText("Tiers in Selected Venue:");

        txtVenues.setText("Venues:");

        btnAddTier.setText("Add Tier to Selected Venue");
        btnAddTier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddTierActionPerformed(evt);
            }
        });

        jLabel1.setText("Select Venue to View Tier Information ");

        btnEditTier.setText("Edit Selected Tier");
        btnEditTier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditTierActionPerformed(evt);
            }
        });

        btnDeleteTier.setText("Delete Selected Tier");
        btnDeleteTier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteTierActionPerformed(evt);
            }
        });

        btnAddVenue.setText("Add Venue");
        btnAddVenue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddVenueActionPerformed(evt);
            }
        });

        btnEditVenue.setText("Edit Selected Venue");
        btnEditVenue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditVenueActionPerformed(evt);
            }
        });

        btnDeleteVenue.setText("Delete Selected Venue");
        btnDeleteVenue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteVenueActionPerformed(evt);
            }
        });

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/refresh-icon-2.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnReset.setText("Reset Search");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(txtVenues)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(lblVenues)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGap(29, 29, 29)
                                        .addComponent(lblSearchVenues)
                                        .addGap(18, 18, 18)
                                        .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(27, 27, 27)
                                        .addComponent(txtSearchVenues, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(scpVenues, javax.swing.GroupLayout.PREFERRED_SIZE, 802, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtTiers)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(btnRefresh)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                        .addComponent(btnAddTier)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(btnEditTier, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addComponent(scpTiers, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(135, 135, 135)
                                        .addComponent(jLabel1))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(141, 141, 141)
                                        .addComponent(btnDeleteTier, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(btnAddVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79)
                .addComponent(btnEditVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(85, 85, 85)
                .addComponent(btnDeleteVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblVenues)
                                .addGap(18, 18, 18))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnReset)
                                .addGap(2, 2, 2)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSearchVenues)
                            .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSearchVenues, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearch)))
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtVenues)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(scpVenues, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnEditVenue)
                            .addComponent(btnDeleteVenue)
                            .addComponent(btnAddVenue)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtTiers)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scpTiers, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAddTier)
                            .addComponent(btnEditTier))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDeleteTier)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteTierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteTierActionPerformed
        if (selectedTier != null) {
            try {
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete " + selectedTier.getTierName()
                        + " in " + selectedVenue.getVenueName() + "?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.YES_OPTION) {
                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/tiers/" + selectedTier.getTierId());
                        String httpInfo = httpHandler.sendDELETERequest(url);
                        if (httpInfo.contains("20")) {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n Tier was successfully deleted! ");
                            refreshPage();
                        } else {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n ERROR Tier was not deleted! \n "
                                    + "Please check there are no runs on the selected venue");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                        mainPanel.removeAll();
                        new LogIn().setVisible(true);
                    }
                }
            } catch (JSONException | IOException ex) {
                JOptionPane.showMessageDialog(this, ex + "ERROR Venue NOT Deleted \n "
                        + "Please check there are no runs on the selected venue");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Tier to Delete");
        }
    }//GEN-LAST:event_btnDeleteTierActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshPage();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnEditVenueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditVenueActionPerformed
        if (selectedVenue != null) {
            try {
                mainPanel.add("EditExistingVenue", new EditExistingVenuesPanel(cardLayout, mainPanel, selectedVenue, loginData));
                cardLayout.show(mainPanel, "EditExistingVenue");
            } catch (JSONException ex) {
                JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Venue to Edit");
        }
    }//GEN-LAST:event_btnEditVenueActionPerformed

    private void btnEditTierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditTierActionPerformed
        if (selectedTier != null) {
            try {
                mainPanel.add("EditExistingTier", new EditExistingTierPanel(cardLayout, mainPanel, selectedTier, loginData));
                cardLayout.show(mainPanel, "EditExistingTier");
            } catch (JSONException ex) {
                JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Tier to Edit");
        }
    }//GEN-LAST:event_btnEditTierActionPerformed

    private void btnAddVenueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddVenueActionPerformed
        cardLayout.removeLayoutComponent(this);
        mainPanel.add("AddNewVenue", new AddVenuePanel(cardLayout, mainPanel, loginData));
        cardLayout.show(mainPanel, "AddNewVenue");
    }//GEN-LAST:event_btnAddVenueActionPerformed

    private void btnAddTierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddTierActionPerformed
        if (selectedVenue != null) {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("AddNewTier", new AddTierPanel(selectedVenue, cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "AddNewTier");
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Venue to add Tier to");
        }
    }//GEN-LAST:event_btnAddTierActionPerformed

    private void btnDeleteVenueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteVenueActionPerformed
        if (selectedVenue != null) {
            try {
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete " + selectedVenue.getVenueName() + "?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.YES_OPTION) {
                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                        for (Tier tier : selectedVenue.getTierList()) {
                            URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/tiers/" + tier.getTierId());
                            String httpInfo = httpHandler.sendDELETERequest(url);
                            if (httpInfo.contains("20")) {
                            } else {
                                JOptionPane.showMessageDialog(this, httpInfo + "\n ERROR a Tier was not deleted, "
                                        + "please check there are no runs at this venue!");
                            }
                        }
                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/venues/" + selectedVenue.getVenueID());
                        String httpInfo = httpHandler.sendDELETERequest(url);
                        if (httpInfo.contains("20")) {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n Venue was successfully deleted!");
                            refreshPage();
                        } else {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n ERROR Venue was not deleted!");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                        mainPanel.removeAll();
                        new LogIn().setVisible(true);
                    }
                }
            } catch (JSONException | IOException ex) {
                JOptionPane.showMessageDialog(this, "ERROR Venue NOT Deleted \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Venue to Delete");
        }
    }//GEN-LAST:event_btnDeleteVenueActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        filteredList.clear();
        String searchCriteria = txtSearchVenues.getText().toLowerCase().replaceAll(" ", "");
        for (Venue venue : venueList) {
            if (cbxCriteria.getSelectedItem().toString().equals("VENUE NAME")) {
                if (venue.getVenueName().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(venue);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("ID")) {
                try {
                    if (searchCriteria.isEmpty() || venue.getVenueID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(venue);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Venue ID");
                    populateVenuesTable(venueList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("ADDRESS LINE 1")) {
                if (venue.getVenueAddress().getAddressLine1().toLowerCase().replaceAll(" ", "").contains(searchCriteria)) {
                    filteredList.add(venue);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("CITY")) {
                if (venue.getVenueAddress().getCity().toLowerCase().replaceAll(" ", "").contains(searchCriteria)) {
                    filteredList.add(venue);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("COUNTY")) {
                if (venue.getVenueAddress().getCounty().toLowerCase().replaceAll(" ", "").contains(searchCriteria)) {
                    filteredList.add(venue);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("POSTCODE")) {
                if (venue.getVenueAddress().getPostcode().toLowerCase().replaceAll(" ", "").contains(searchCriteria)) {
                    filteredList.add(venue);
                }
            }
        }
        populateVenuesTable(filteredList);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        populateVenuesTable(venueList);
        txtSearchVenues.setText("");
    }//GEN-LAST:event_btnResetActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddTier;
    private javax.swing.JButton btnAddVenue;
    private javax.swing.JButton btnDeleteTier;
    private javax.swing.JButton btnDeleteVenue;
    private javax.swing.JButton btnEditTier;
    private javax.swing.JButton btnEditVenue;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox cbxCriteria;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblSearchVenues;
    private javax.swing.JLabel lblVenues;
    private javax.swing.JScrollPane scpTiers;
    private javax.swing.JScrollPane scpVenues;
    private javax.swing.JTable tblTiers;
    private javax.swing.JTable tblVenues;
    private javax.swing.JTextField txtSearchVenues;
    private javax.swing.JLabel txtTiers;
    private javax.swing.JLabel txtVenues;
    // End of variables declaration//GEN-END:variables
}
