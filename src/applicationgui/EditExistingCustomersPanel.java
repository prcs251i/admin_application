package applicationgui;

import AdministrationApplication.Customer;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */

public class EditExistingCustomersPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private HTTPRequestHandler httpHandler;
    private Customer selectedCustomer;
    private final String loginData;

    public EditExistingCustomersPanel(CardLayout layoutCard, JPanel panel, Customer customer, String data) {
        cardLayout = layoutCard;
        mainPanel = panel;
        selectedCustomer = customer;
        httpHandler = new HTTPRequestHandler();
        initComponents();
        fillExistingCustomerFields();
        loginData = data;
    }

    private void fillExistingCustomerFields() {
        txtID.setText(String.valueOf(selectedCustomer.getID()));
        txtFirstName.setText(selectedCustomer.getForename());
        txtLastName.setText(selectedCustomer.getSurname());
        txtEmailAddress.setText(selectedCustomer.getEmail());
        txtAddressLine1.setText(selectedCustomer.getAddress().getAddressLine1());
        txtAddressLine2.setText(selectedCustomer.getAddress().getAddressLine2());
        txtCity.setText(selectedCustomer.getAddress().getCity());
        txtCounty.setText(selectedCustomer.getAddress().getCounty());
        txtPostcode.setText(selectedCustomer.getAddress().getPostcode());
        txtHomeNo.setText(selectedCustomer.getHomeNumber());
        txtMobileNo.setText(selectedCustomer.getMobNumber());
        txtCardNo.setText(selectedCustomer.getCardNumber());
        cbxCardType.setSelectedItem(selectedCustomer.getCardType());
        dtpExpiryDate.setDate(selectedCustomer.getExpiryDate());
        txtSecCode.setText(String.valueOf(selectedCustomer.getSecurityCode()));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlRegisterCustomer = new javax.swing.JPanel();
        lblFirstName = new javax.swing.JLabel();
        lblLastName = new javax.swing.JLabel();
        lblEmailAddress = new javax.swing.JLabel();
        lblPassword = new javax.swing.JLabel();
        txtEmailAddress = new javax.swing.JTextField();
        txtLastName = new javax.swing.JTextField();
        txtFirstName = new javax.swing.JTextField();
        txtPassword = new javax.swing.JTextField();
        lblBillingAddr = new javax.swing.JLabel();
        lblAddressLine1 = new javax.swing.JLabel();
        lblAddressLine2 = new javax.swing.JLabel();
        lblCity = new javax.swing.JLabel();
        lblCounty = new javax.swing.JLabel();
        lblPostCode = new javax.swing.JLabel();
        txtAddressLine1 = new javax.swing.JTextField();
        txtAddressLine2 = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        txtCounty = new javax.swing.JTextField();
        lblAccountInfo = new javax.swing.JLabel();
        txtPostcode = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblPaymentInfo = new javax.swing.JLabel();
        lblCardType = new javax.swing.JLabel();
        lblCardNo = new javax.swing.JLabel();
        txtCardNo = new javax.swing.JTextField();
        cbxCardType = new javax.swing.JComboBox();
        txtExpDate = new javax.swing.JLabel();
        lblSecCode = new javax.swing.JLabel();
        txtSecCode = new javax.swing.JTextField();
        lblHomeNo = new javax.swing.JLabel();
        lblMobNo = new javax.swing.JLabel();
        txtMobileNo = new javax.swing.JTextField();
        txtHomeNo = new javax.swing.JTextField();
        dtpExpiryDate = new org.jdesktop.swingx.JXDatePicker();
        lblID = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        lblConfirmEmailAddress = new javax.swing.JLabel();
        txtConfirmEmailAddress = new javax.swing.JTextField();
        lblConfirmPassword = new javax.swing.JLabel();
        txtConfirmPass = new javax.swing.JTextField();
        btnClear = new javax.swing.JButton();
        lblEditExistingCust = new javax.swing.JLabel();

        setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        lblFirstName.setText("First Name: *");

        lblLastName.setText("Last Name: *");

        lblEmailAddress.setText("Email Address: *");

        lblPassword.setText("Password: *");

        lblBillingAddr.setText("Contact Information");

        lblAddressLine1.setText("Address Line 1: *");

        lblAddressLine2.setText("Address Line 2:");

        lblCity.setText("City: *");

        lblCounty.setText("County: *");

        lblPostCode.setText("Postcode: *");

        lblAccountInfo.setText("Account Information");

        btnUpdate.setText("Update Customer");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblPaymentInfo.setText("Payment Information");

        lblCardType.setText("Card Type: *");

        lblCardNo.setText("Card Number: *");

        cbxCardType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "VISA", "VISA Electron", "MasterCard", "Maestro", "American Express" }));

        txtExpDate.setText("Expiry Date: *");

        lblSecCode.setText("Security Code: *");

        lblHomeNo.setText("Home Phone Number:");

        lblMobNo.setText("Mobile Phone Number:");

        lblID.setText("ID:");

        txtID.setEditable(false);

        lblConfirmEmailAddress.setText("Confirm Email Address: *");

        lblConfirmPassword.setText("Confirm Password: *");

        btnClear.setText("Clear");

        javax.swing.GroupLayout pnlRegisterCustomerLayout = new javax.swing.GroupLayout(pnlRegisterCustomer);
        pnlRegisterCustomer.setLayout(pnlRegisterCustomerLayout);
        pnlRegisterCustomerLayout.setHorizontalGroup(
            pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGap(205, 205, 205)
                        .addComponent(lblAccountInfo)
                        .addGap(266, 266, 266)
                        .addComponent(lblBillingAddr)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 138, Short.MAX_VALUE))
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblFirstName)
                            .addComponent(lblLastName)
                            .addComponent(lblEmailAddress)
                            .addComponent(lblPassword)
                            .addComponent(lblID)
                            .addComponent(lblConfirmEmailAddress)
                            .addComponent(lblConfirmPassword))
                        .addGap(18, 18, 18)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtPassword)
                            .addComponent(txtEmailAddress)
                            .addComponent(txtLastName)
                            .addComponent(txtFirstName)
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtConfirmEmailAddress)
                            .addComponent(txtConfirmPass, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                        .addComponent(lblAddressLine1)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(lblCity)
                                            .addComponent(lblAddressLine2)
                                            .addComponent(lblCounty)
                                            .addComponent(lblPostCode))
                                        .addGap(18, 18, 18)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtCounty, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtAddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtPostcode, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblMobNo)
                                    .addComponent(lblHomeNo))
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(txtMobileNo, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlRegisterCustomerLayout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(txtHomeNo, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(32, 32, 32)))
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGap(170, 170, 170)
                        .addComponent(lblPaymentInfo)
                        .addGap(78, 78, 78))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblCardNo)
                            .addComponent(txtExpDate)
                            .addComponent(lblSecCode)
                            .addComponent(lblCardType))
                        .addGap(18, 18, 18)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCardNo, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxCardType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSecCode, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dtpExpiryDate, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlRegisterCustomerLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(258, 258, 258))
        );
        pnlRegisterCustomerLayout.setVerticalGroup(
            pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addComponent(lblPaymentInfo)
                        .addGap(0, 309, Short.MAX_VALUE))
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblBillingAddr)
                                    .addComponent(lblAccountInfo))
                                .addGap(18, 18, 18)
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblHomeNo)
                                            .addComponent(txtHomeNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblMobNo)
                                            .addComponent(txtMobileNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblAddressLine1)
                                            .addComponent(txtAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblAddressLine2)
                                            .addComponent(txtAddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblCity)
                                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblCounty)
                                            .addComponent(txtCounty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblPostCode)
                                            .addComponent(txtPostcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblID)
                                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblFirstName)
                                            .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblLastName)
                                            .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblEmailAddress)
                                            .addComponent(txtEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblConfirmEmailAddress)
                                            .addComponent(txtConfirmEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblPassword)
                                            .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblConfirmPassword)
                                            .addComponent(txtConfirmPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cbxCardType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblCardType))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblCardNo)
                                    .addComponent(txtCardNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtExpDate)
                                    .addComponent(dtpExpiryDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblSecCode)
                                    .addComponent(txtSecCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(96, 96, 96)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnUpdate)
                            .addComponent(btnCancel)
                            .addComponent(btnClear))))
                .addGap(91, 91, 91))
        );

        lblEditExistingCust.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblEditExistingCust.setText("Edit Existing Customer");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblEditExistingCust)
                    .addComponent(pnlRegisterCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblEditExistingCust)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlRegisterCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private String constructCustomerJsonString(Customer newCustomer) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("ID", newCustomer.getID());
        j.put("FORENAME", newCustomer.getForename());
        j.put("SURNAME", newCustomer.getSurname());
        j.put("EMAIL", newCustomer.getEmail());
        j.put("PASSWORD_HASH", newCustomer.getPassword());
        j.put("ADDRESS_LINE_1", newCustomer.getAddress().getAddressLine1());
        j.put("ADDRESS_LINE_2", newCustomer.getAddress().getAddressLine2());
        j.put("CITY", newCustomer.getAddress().getCity());
        j.put("COUNTY", newCustomer.getAddress().getCounty());
        j.put("POSTCODE", newCustomer.getAddress().getPostcode());
        j.put("HOME_PHONE", newCustomer.getHomeNumber());
        j.put("MOB_PHONE", newCustomer.getMobNumber());
        j.put("CARD_TYPE", newCustomer.getCardType());
        j.put("CARD_NUMBER", newCustomer.getCardNumber());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String formattedDate = formatter.format(newCustomer.getExpiryDate());
        j.put("EXPIRY_DATE", formattedDate);
        j.put("SECURITY_CODE", newCustomer.getSecurityCode());
        return j.toString();
    }

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        if (!txtFirstName.getText().isEmpty()) {
            if (!txtLastName.getText().isEmpty()) {
                if (!txtEmailAddress.getText().isEmpty()) {
                    if (!txtConfirmEmailAddress.getText().isEmpty() && txtConfirmEmailAddress.getText().equals(txtEmailAddress.getText())) {
                        if (!txtPassword.getText().isEmpty()) {
                            if (!txtConfirmPass.getText().isEmpty() && txtConfirmPass.getText().equals(txtPassword.getText())) {
                                if (!txtAddressLine1.getText().isEmpty()) {
                                    if (!txtCity.getText().isEmpty()) {
                                        if (!txtCounty.getText().isEmpty()) {
                                            if (!txtPostcode.getText().isEmpty()) {
                                                if (cbxCardType.getSelectedIndex() != -1) {
                                                    if (!txtCardNo.getText().isEmpty()) {
                                                        if (dtpExpiryDate.getDate() != null) {
                                                            if (!txtSecCode.getText().isEmpty()) {
                                                                try {
                                                                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                                                                        selectedCustomer.setForename(txtFirstName.getText());
                                                                        selectedCustomer.setSurname(txtLastName.getText());
                                                                        selectedCustomer.setEmail(txtEmailAddress.getText());
                                                                        selectedCustomer.setPassword(txtPassword.getText());
                                                                        selectedCustomer.setHomeNumber(txtHomeNo.getText());
                                                                        selectedCustomer.setMobNumber(txtMobileNo.getText());
                                                                        selectedCustomer.getAddress().setAddressLine1(txtAddressLine1.getText());
                                                                        selectedCustomer.getAddress().setAddressLine2(txtAddressLine2.getText());
                                                                        selectedCustomer.getAddress().setCity(txtCity.getText());
                                                                        selectedCustomer.getAddress().setCounty(txtCounty.getText());
                                                                        selectedCustomer.getAddress().setPostcode(txtPostcode.getText().replaceAll(" ", "").toUpperCase());
                                                                        selectedCustomer.setCardType(cbxCardType.getSelectedItem().toString());
                                                                        selectedCustomer.setCardNumber(txtCardNo.getText());
                                                                        selectedCustomer.setExpiryDate(dtpExpiryDate.getDate());
                                                                        selectedCustomer.setSecurityCode(txtSecCode.getText());
                                                                        String data = constructCustomerJsonString(selectedCustomer);
                                                                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/Customers/" + selectedCustomer.getID());
                                                                        String httpInfo = httpHandler.sendPUTRequest(data, url);
                                                                        if (httpInfo.contains("20")) {
                                                                            JOptionPane.showMessageDialog(this, httpInfo + " Customer successfully updated");
                                                                            mainPanel.remove(this);
                                                                            mainPanel.add("ViewExistingCustomers", new ViewExistingCustomersPanel(cardLayout, mainPanel, loginData));
                                                                            cardLayout.show(mainPanel, "ViewExistingCustomers");
                                                                        } else {
                                                                            JOptionPane.showMessageDialog(this, httpInfo + " Customer NOT updated");
                                                                        }
                                                                    } else {
                                                                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                                                                        mainPanel.removeAll();
                                                                        new LogIn().setVisible(true);
                                                                    }
                                                                } catch (MalformedURLException ex) {
                                                                    JOptionPane.showMessageDialog(this, "ERROR, Customer NOT updated \n " + ex);
                                                                } catch (IOException ex) {
                                                                    JOptionPane.showMessageDialog(this, "ERROR, Customer NOT updated \n " + ex);
                                                                } catch (JSONException ex) {
                                                                    JOptionPane.showMessageDialog(this, "ERROR, Customer NOT updated \n " + ex);
                                                                }
                                                            } else {
                                                                JOptionPane.showMessageDialog(this, "Please enter a Payment Card Security Code for the new Customer");
                                                            }
                                                        } else {
                                                            JOptionPane.showMessageDialog(this, "Please select an Payment Card Expiry Date for the new Customer");
                                                        }
                                                    } else {
                                                        JOptionPane.showMessageDialog(this, "Please enter a Payment Card Number for the new Customer");
                                                    }
                                                } else {
                                                    JOptionPane.showMessageDialog(this, "Please select a Payment Card Type for the new Customer");
                                                }
                                            } else {
                                                JOptionPane.showMessageDialog(this, "Please enter a Postcode for the new Customer");
                                            }
                                        } else {
                                            JOptionPane.showMessageDialog(this, "Please enter a County of Residence for the new Customer");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(this, "Please enter a City of Residence for the new Customer");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(this, "Please enter an Address Line 1 for the new Customer");
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "The confirmation password you have entered does not match");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Please enter a Password for the new Customer");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "The confirmation email you have entered does not match");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please enter an Email Address for the new Customer");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please enter a Surname for the new Customer");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please enter a Forename for the new Customer");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel these changes?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            try {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingCustomers", new ViewExistingCustomersPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingCustomers");
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
            }
        }
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox cbxCardType;
    private org.jdesktop.swingx.JXDatePicker dtpExpiryDate;
    private javax.swing.JLabel lblAccountInfo;
    private javax.swing.JLabel lblAddressLine1;
    private javax.swing.JLabel lblAddressLine2;
    private javax.swing.JLabel lblBillingAddr;
    private javax.swing.JLabel lblCardNo;
    private javax.swing.JLabel lblCardType;
    private javax.swing.JLabel lblCity;
    private javax.swing.JLabel lblConfirmEmailAddress;
    private javax.swing.JLabel lblConfirmPassword;
    private javax.swing.JLabel lblCounty;
    private javax.swing.JLabel lblEditExistingCust;
    private javax.swing.JLabel lblEmailAddress;
    private javax.swing.JLabel lblFirstName;
    private javax.swing.JLabel lblHomeNo;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblLastName;
    private javax.swing.JLabel lblMobNo;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblPaymentInfo;
    private javax.swing.JLabel lblPostCode;
    private javax.swing.JLabel lblSecCode;
    private javax.swing.JPanel pnlRegisterCustomer;
    private javax.swing.JTextField txtAddressLine1;
    private javax.swing.JTextField txtAddressLine2;
    private javax.swing.JTextField txtCardNo;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtConfirmEmailAddress;
    private javax.swing.JTextField txtConfirmPass;
    private javax.swing.JTextField txtCounty;
    private javax.swing.JTextField txtEmailAddress;
    private javax.swing.JLabel txtExpDate;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtHomeNo;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtMobileNo;
    private javax.swing.JTextField txtPassword;
    private javax.swing.JTextField txtPostcode;
    private javax.swing.JTextField txtSecCode;
    // End of variables declaration//GEN-END:variables
}
