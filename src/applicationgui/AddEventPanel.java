package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Event;
import AdministrationApplication.EventRun;
import AdministrationApplication.EventType;
import AdministrationApplication.Price;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.awt.Component;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class AddEventPanel extends javax.swing.JPanel {

    private JsonClassGenerator generator;
    private HTTPRequestHandler httpHandler;
    private DefaultComboBoxModel eventTypesComboModel = new DefaultComboBoxModel();
    private ArrayList<AdditionalRunsPanel> runsForms = new ArrayList<>();
    private HashMap<Integer, EventType> eventTypeHash = new HashMap<>();
    private CardLayout cardLayout;
    private JPanel mainPanel;
    private Event newEvent;
    private int totalRuns;
    private final String loginData;
    private String[] eventHttpInfo;
    private int runCount;

    public AddEventPanel(CardLayout layoutCard, JPanel panel, String data) throws IOException, JSONException {
        cardLayout = layoutCard;
        mainPanel = panel;
        initComponents();
        httpHandler = new HTTPRequestHandler();
        generator = new JsonClassGenerator();
        fillEventTypeComboBox();
        btnAddRun.setVisible(false);
        btnCancelRun.setVisible(false);
        loginData = data;
        txtEventDesc.setLineWrap(true);
        runCount = 1;
        
    }

    private void fillEventTypeComboBox() throws IOException, JSONException {
        List<EventType> eventTypes = generator.generateEventTypes();
        for (EventType type : eventTypes) {
            eventTypesComboModel.addElement(type.getType());
            eventTypeHash.put(type.getID(), type);
        }
    }

    private String constructEventJsonString(Event newEvent) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("ID", newEvent.getID());
        j.put("EVENT_TYPE", newEvent.getEventType().getID());
        j.put("TITLE", newEvent.getEventTitle());
        j.put("AGE_RATING", newEvent.getAgeRating());
        j.put("DESCRIPTION", newEvent.getEventDescription());
        j.put("IMAGE", newEvent.getImageString());
        return j.toString();
    }

    private void refreshPage() {
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("AddNewEvents", new AddEventPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "AddNewEvents");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblAddNewVenue = new javax.swing.JLabel();
        pnlAddNewEvent = new javax.swing.JPanel();
        pnlEventDetails = new javax.swing.JPanel();
        tbpAdditionalRuns = new javax.swing.JTabbedPane();
        lblEventTitle = new javax.swing.JLabel();
        txtEventTitle = new javax.swing.JTextField();
        lblEventType = new javax.swing.JLabel();
        cbxEventType = new javax.swing.JComboBox();
        lblNoOfRuns = new javax.swing.JLabel();
        spnNoOfRuns = new javax.swing.JSpinner();
        btnAddEvent = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblDescription = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtEventDesc = new javax.swing.JTextArea();
        jSeparator1 = new javax.swing.JSeparator();
        lblImageRating = new javax.swing.JLabel();
        cbxAgeRating = new javax.swing.JComboBox();
        lblAgeRating = new javax.swing.JLabel();
        txtImageURL = new javax.swing.JTextField();
        lblEventDetails = new javax.swing.JLabel();
        btnAddRun = new javax.swing.JButton();
        btnCancelRun = new javax.swing.JButton();

        lblAddNewVenue.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblAddNewVenue.setText("Add New Event");

        lblEventTitle.setText("Title:");

        lblEventType.setText("Event Type:");

        cbxEventType.setModel(eventTypesComboModel);

        lblNoOfRuns.setText("No. of Runs:");

        spnNoOfRuns.setModel(new javax.swing.SpinnerNumberModel(1, 1, 50, 1));

        btnAddEvent.setText("Add Event");
        btnAddEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddEventActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblDescription.setText("Description:");

        txtEventDesc.setColumns(20);
        txtEventDesc.setRows(5);
        jScrollPane1.setViewportView(txtEventDesc);

        lblImageRating.setText("Image URL:");

        cbxAgeRating.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "PG", "U", "12A", "15", "18+" }));

        lblAgeRating.setText("Age Rating:");

        javax.swing.GroupLayout pnlEventDetailsLayout = new javax.swing.GroupLayout(pnlEventDetails);
        pnlEventDetails.setLayout(pnlEventDetailsLayout);
        pnlEventDetailsLayout.setHorizontalGroup(
            pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEventDetailsLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(485, 485, 485))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEventDetailsLayout.createSequentialGroup()
                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addComponent(lblEventType)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbxEventType, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(lblNoOfRuns)
                                .addGap(18, 18, 18)
                                .addComponent(spnNoOfRuns, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                                .addGap(71, 71, 71)
                                .addComponent(lblEventTitle)
                                .addGap(18, 18, 18)
                                .addComponent(txtEventTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(lblDescription)))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblImageRating)
                            .addComponent(lblAgeRating))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbxAgeRating, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtImageURL, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addGap(360, 360, 360)
                        .addComponent(btnAddEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(146, 146, 146)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(200, 200, 200))
            .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tbpAdditionalRuns, javax.swing.GroupLayout.PREFERRED_SIZE, 1382, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlEventDetailsLayout.setVerticalGroup(
            pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblEventTitle)
                                .addComponent(txtEventTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblDescription))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbxEventType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblEventType))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNoOfRuns)
                            .addComponent(spnNoOfRuns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblImageRating)
                            .addComponent(txtImageURL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAgeRating)
                            .addComponent(cbxAgeRating, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnAddEvent))
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tbpAdditionalRuns, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblEventDetails.setText("Event Details");

        javax.swing.GroupLayout pnlAddNewEventLayout = new javax.swing.GroupLayout(pnlAddNewEvent);
        pnlAddNewEvent.setLayout(pnlAddNewEventLayout);
        pnlAddNewEventLayout.setHorizontalGroup(
            pnlAddNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAddNewEventLayout.createSequentialGroup()
                .addGap(643, 643, 643)
                .addComponent(lblEventDetails)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAddNewEventLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlEventDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 1401, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );
        pnlAddNewEventLayout.setVerticalGroup(
            pnlAddNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAddNewEventLayout.createSequentialGroup()
                .addComponent(lblEventDetails)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlEventDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnAddRun.setText("Add Run");
        btnAddRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddRunActionPerformed(evt);
            }
        });

        btnCancelRun.setText("Cancel Run");
        btnCancelRun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelRunActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlAddNewEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(lblAddNewVenue))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(374, 374, 374)
                        .addComponent(btnAddRun, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(159, 159, 159)
                        .addComponent(btnCancelRun, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddNewVenue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlAddNewEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddRun)
                    .addComponent(btnCancelRun))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddEventActionPerformed
        try {
            if (!txtEventTitle.getText().isEmpty()) {
                if (!txtEventDesc.getText().isEmpty()) {
                    if (cbxEventType.getSelectedIndex() != -1) {
                        if (cbxAgeRating.getSelectedIndex() != -1) {
                            if (!txtImageURL.getText().isEmpty()) {
                                if ((Integer) spnNoOfRuns.getValue() > 0 | (Integer) spnNoOfRuns.getValue() < 51) {
                                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                                        newEvent = new Event(txtEventTitle.getText(), txtEventDesc.getText(), eventTypeHash.get(cbxEventType.getSelectedIndex()), cbxAgeRating.getSelectedItem().toString(), txtImageURL.getText());
                                        String data = constructEventJsonString(newEvent);
                                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/Events");
                                        eventHttpInfo = httpHandler.sendPOSTRequest(data, url);
                                        if (eventHttpInfo[0].contains("20")) {
                                            JOptionPane.showMessageDialog(this, eventHttpInfo[0] + "\n Event Added, Now Please Fill Run Information");
                                            btnAddRun.setVisible(true);
                                            btnCancelRun.setVisible(true);
                                            for (Component component : pnlEventDetails.getComponents()) {
                                                component.setEnabled(false);
                                            }

                                            AdditionalRunsPanel runPanel;
                                            totalRuns = (Integer) spnNoOfRuns.getValue();
                                            for (int i = 1; i <= totalRuns; i++) {
                                                runPanel = new AdditionalRunsPanel(i, totalRuns);
                                                runsForms.add(i - 1, runPanel);
                                                tbpAdditionalRuns.addTab("Run " + i, runPanel);
                                            }

                                        } else {
                                            JOptionPane.showMessageDialog(this, eventHttpInfo[0] + "\n ERROR, Event NOT Added ");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                                        mainPanel.removeAll();
                                        new LogIn().setVisible(true);
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(this, "Number of Runs must be between 1 and 50");
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Please enter an image URL for the new Event");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Please select an age rating for the new Event");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Please select an event type for the new Event");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please select a description for the new Event");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please select a title for the new Event");
            }

        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR Event NOT Added \n" + ex);
        }

    }//GEN-LAST:event_btnAddEventActionPerformed

    private void btnAddRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddRunActionPerformed
        try {
            if (httpHandler.authenticate(loginData)[0].contains("20")) {
                AdditionalRunsPanel selectedForm = (AdditionalRunsPanel) tbpAdditionalRuns.getSelectedComponent();
                int selectedIndex = tbpAdditionalRuns.getSelectedIndex();
                URL runUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/Runs");
                JSONObject venueInfo = new JSONObject(eventHttpInfo[1]);
                int eventId = venueInfo.getInt("ID");
                newEvent.setID(eventId);
                EventRun newRun = selectedForm.constructEventRun();
                String runData = selectedForm.constructRunJsonString(newRun, newEvent);
                String[] runHttpInfo = httpHandler.sendPOSTRequest(runData, runUrl);
                if (runHttpInfo[0].contains("20")) {
                    URL performancesUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/Performances");
                    JSONObject performerInfo = new JSONObject(runHttpInfo[1]);
                    int runId = performerInfo.getInt("ID");
                    newRun.setID(runId);
                    String[] performancesData = selectedForm.constructPerformanceJsonStrings(newRun);
                    for (int i = 0; i < performancesData.length; i++) {
                        String[] performancesHttpInfo = httpHandler.sendPOSTRequest(performancesData[i], performancesUrl);
                        if (performancesHttpInfo[0].contains("20")) {

                        } else {
                            JOptionPane.showMessageDialog(this, performancesHttpInfo[0] + "\n ERROR, A performer has not been added to the run");
                        }
                    }
                    URL priceUrl = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/Prices");
                    List<Price> priceList = selectedForm.constructPrices(newRun);
                    String[] pricesData = selectedForm.constructPriceJsonStrings(newRun, priceList);
                    for (int i = 0; i < pricesData.length; i++) {
                        String[] priceHttpInfo = httpHandler.sendPOSTRequest(pricesData[i], priceUrl);
                        if (priceHttpInfo[0].contains("20")) {
                            if (selectedIndex + 1 < totalRuns) {
                                tbpAdditionalRuns.setSelectedIndex(selectedIndex + 1);
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, priceHttpInfo[0] + "\n ERROR, A tier Price has not been added to the run");
                        }
                    }
                    JOptionPane.showMessageDialog(this, runHttpInfo[0] + "\n Run was successfully added!");
                    for (Component comp : selectedForm.getComponents()) {
                        comp.setEnabled(false);
                    }
                    runCount++;
                    if (runCount > totalRuns) {
                        cardLayout.removeLayoutComponent(this);
                        mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                        cardLayout.show(mainPanel, "ViewExistingEvents");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, runHttpInfo[0] + "\n ERROR, Run has not been added");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                mainPanel.removeAll();
                new LogIn().setVisible(true);
            }
        } catch (ParseException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        } catch (MalformedURLException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }

    }//GEN-LAST:event_btnAddRunActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingEvents");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnCancelRunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelRunActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel adding runs?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingEvents");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnCancelRunActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddEvent;
    private javax.swing.JButton btnAddRun;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCancelRun;
    private javax.swing.JComboBox cbxAgeRating;
    private javax.swing.JComboBox cbxEventType;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblAddNewVenue;
    private javax.swing.JLabel lblAgeRating;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblEventDetails;
    private javax.swing.JLabel lblEventTitle;
    private javax.swing.JLabel lblEventType;
    private javax.swing.JLabel lblImageRating;
    private javax.swing.JLabel lblNoOfRuns;
    private javax.swing.JPanel pnlAddNewEvent;
    private javax.swing.JPanel pnlEventDetails;
    private javax.swing.JSpinner spnNoOfRuns;
    private javax.swing.JTabbedPane tbpAdditionalRuns;
    private javax.swing.JTextArea txtEventDesc;
    private javax.swing.JTextField txtEventTitle;
    private javax.swing.JTextField txtImageURL;
    // End of variables declaration//GEN-END:variables
}
