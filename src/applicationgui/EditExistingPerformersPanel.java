package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Act;
import AdministrationApplication.EventType;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class EditExistingPerformersPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private DefaultComboBoxModel PerformerTypeComboBoxModel;
    private List<EventType> eventTypes;
    private HashMap<String, EventType> eventTypesHash;
    private JsonClassGenerator generator;
    private HTTPRequestHandler httpHandler;
    private Act selectedPerformer;
    private final String loginData;

    /**
     * Creates new form EditExistingPerformersPanel
     */
    public EditExistingPerformersPanel(CardLayout layoutCard, JPanel panel, Act performer, String data) throws IOException, JSONException {
        selectedPerformer = performer;
        PerformerTypeComboBoxModel = new DefaultComboBoxModel();
        eventTypesHash = new HashMap<>();
        cardLayout = layoutCard;
        mainPanel = panel;
        initComponents();
        generator = new JsonClassGenerator();
        httpHandler = new HTTPRequestHandler();
        initComboModel();
        fillExistingPerformersFields();
        loginData = data;
        txtDescription.setLineWrap(true);
    }

    private void fillExistingPerformersFields() {
        txtID.setText(String.valueOf(selectedPerformer.getID()));
        txtName.setText(selectedPerformer.getActName());
        txtDescription.setText(selectedPerformer.getActDesc());
        cbxGenre.setSelectedItem(selectedPerformer.getActType().getType());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtID = new javax.swing.JTextField();
        txtName = new javax.swing.JTextField();
        cbxGenre = new javax.swing.JComboBox();
        btnUpdate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblID = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblGenre = new javax.swing.JLabel();
        lblAddNewPerformer = new javax.swing.JLabel();
        lblDesc = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDescription = new javax.swing.JTextArea();

        txtID.setEditable(false);

        cbxGenre.setModel(PerformerTypeComboBoxModel);

        btnUpdate.setText("Update Performer");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblID.setText("ID:");

        lblName.setText("Name:");

        lblGenre.setText("Genre:");

        lblAddNewPerformer.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblAddNewPerformer.setText("Edit Existing Performer");

        lblDesc.setText("Desc:");

        txtDescription.setColumns(20);
        txtDescription.setRows(5);
        jScrollPane2.setViewportView(txtDescription);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblAddNewPerformer)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(36, 36, 36)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblID)
                                    .addComponent(lblName)
                                    .addComponent(lblDesc))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
                                    .addComponent(txtID, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addComponent(lblGenre)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbxGenre, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddNewPerformer)
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDesc)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblGenre)
                    .addComponent(cbxGenre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate)
                    .addComponent(btnCancel))
                .addGap(30, 30, 30))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void initComboModel() throws IOException {
        eventTypes = generator.generateEventTypes();
        for (EventType eventType : eventTypes) {
            eventTypesHash.put(eventType.getType(), eventType);
            PerformerTypeComboBoxModel.addElement(eventType.getType());
        }
    }

    private String constructActJsonString(Act newAct) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("ID", newAct.getID());
        j.put("TITLE", newAct.getActName());
        j.put("DESCRIPTION", newAct.getActDesc());
        j.put("ACT_TYPE", newAct.getActType().getID());
        return j.toString();
    }

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        if (!txtName.getText().isEmpty()) {
            if (!txtDescription.getText().isEmpty()) {
                if (cbxGenre.getSelectedIndex() != -1) {
                    try {
                        if (httpHandler.authenticate(loginData)[0].contains("20")) {
                            selectedPerformer.setActName(txtName.getText());
                            selectedPerformer.setActDesc(txtDescription.getText());
                            selectedPerformer.setActType(eventTypesHash.get(cbxGenre.getSelectedItem().toString()));
                            String data = constructActJsonString(selectedPerformer);
                            URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/acts/" + selectedPerformer.getID());
                            String httpInfo = httpHandler.sendPUTRequest(data, url);
                            if (httpInfo.contains("20")) {
                                JOptionPane.showMessageDialog(this, httpInfo + "\n \"Performer was successfully updated!\"");
                                cardLayout.removeLayoutComponent(this);
                                mainPanel.add("ViewExistingPerformers", new ViewExistingPerformersPanel(cardLayout, mainPanel, loginData));
                                cardLayout.show(mainPanel, "ViewExistingPerformers");
                            } else {
                                JOptionPane.showMessageDialog(this, httpInfo + "\n \"ERROR Performer NOT Updated!\"");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                            mainPanel.removeAll();
                            new LogIn().setVisible(true);
                        }
                    } catch (MalformedURLException ex) {
                        JOptionPane.showMessageDialog(this, "ERROR, Performer NOT updated \n " + ex);
                    } catch (IOException | JSONException ex) {
                        JOptionPane.showMessageDialog(this, "ERROR, Performer NOT updated \n " + ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please select a genre for the new Performer");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please enter a description for the new Performer");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please enter a name for the new Performer");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel these changes?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            try {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingPerformers", new ViewExistingPerformersPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingPerformers");
            } catch (IOException | JSONException ex) {
                JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
            }
        }
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox cbxGenre;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblAddNewPerformer;
    private javax.swing.JLabel lblDesc;
    private javax.swing.JLabel lblGenre;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblName;
    private javax.swing.JTextArea txtDescription;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables
}
