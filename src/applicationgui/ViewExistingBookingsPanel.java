package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Booking;
import AdministrationApplication.SeatedTicket;
import AdministrationApplication.Ticket;
import java.awt.CardLayout;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;

/**
 *
 * @author Joel Gooch
 */
public class ViewExistingBookingsPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewExistingBookingsPanel
     */
    private CardLayout cardLayout;
    private JPanel mainPanel;
    private HashMap<Integer, Booking> bookingsHashMap;
    private HashMap<Integer, Ticket> ticketsHashMap;
    private List<Booking> bookingsList;
    private List<Booking> filteredList;
    private DefaultTableModel bookingsModel;
    private DefaultTableModel ticketsModel;
    private Booking selectedBooking;
    private Ticket selectedTicket;
    private final String loginData;
    
    public ViewExistingBookingsPanel(CardLayout layoutCard, JPanel main, String data) throws IOException, JSONException {
        cardLayout = layoutCard;
        mainPanel = main;
        initComponents();
        initialiseTables();
        bookingsHashMap = new HashMap<>();
        ticketsHashMap = new HashMap<>();
        JsonClassGenerator generator = new JsonClassGenerator();
        filteredList = new ArrayList();
        bookingsList = generator.generateBookings();
        for (Booking booking : bookingsList) {
            bookingsHashMap.put(booking.getBookingID(), booking);
        }
        populateBookingsTable(bookingsList);
        loginData = data;
    }
    
    private void initialiseTables() {
        bookingsModel = (DefaultTableModel) tblBookings.getModel();
        ticketsModel = (DefaultTableModel) tblTickets.getModel();
        ListSelectionModel bookingSelectionModel = tblBookings.getSelectionModel();
        bookingSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    ticketsModel.setNumRows(0);
                    if (tblBookings.getSelectedRow() != -1) {
                        int selectedBookingID = (int) tblBookings.getValueAt(tblBookings.getSelectedRow(), 0);
                        selectedBooking = bookingsHashMap.get(selectedBookingID);
                        for (Ticket ticket : selectedBooking.getTicketList()) {
                            ticketsHashMap.put(ticket.getTicketRef(), ticket);
                        }
                        populateTicketsTable(selectedBooking);
                    }
                }
            }
        });
        
        ListSelectionModel ticketSelectionModel = tblTickets.getSelectionModel();
        ticketSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (tblTickets.getSelectedRow() != -1) {
                        int selectedTicketID = (int) tblTickets.getValueAt(tblTickets.getSelectedRow(), 0);
                        selectedTicket = ticketsHashMap.get(selectedTicketID);
                    }
                }
            }
        });
        setTableColumnWidths();
    }
    
    private void populateBookingsTable(List<Booking> bookingsList) {
        bookingsModel.setNumRows(0);
        Object[] row = new Object[6];
        for (int i = 0; i < bookingsList.size(); i++) {
            row[0] = bookingsList.get(i).getBookingID();
            row[1] = bookingsList.get(i).getRun().getID();
            row[2] = bookingsList.get(i).getCustomer().getID();
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(bookingsList.get(i).getDatePlaced());
            row[3] = formattedDate;
            row[4] = bookingsList.get(i).getNoOfTickets();
            row[5] = bookingsList.get(i).getTotalCost();
            bookingsModel.addRow(row);
        }
    }
    
    private void populateTicketsTable(Booking selectedBooking) {
        ticketsModel.setNumRows(0);
        List<Ticket> ticketList = selectedBooking.getTicketList();
        Object[] row = new Object[5];
        for (int i = 0; i < ticketList.size(); i++) {
            row[0] = ticketList.get(i).getTicketRef();
            row[1] = selectedBooking.getBookingID();
            row[2] = ticketList.get(i).getAllocatedTier().getTierName();
            if (ticketList.get(i) instanceof SeatedTicket) {
                SeatedTicket ticket = (SeatedTicket) (ticketList.get(i));
                row[3] = ticket.getSeatRow();
                row[4] = ticket.getSeatNo();
            }
            ticketsModel.addRow(row);
        }
    }
    
    private void setTableColumnWidths() {
        tblBookings.getColumnModel().getColumn(0).setPreferredWidth(80);
        tblBookings.getColumnModel().getColumn(1).setPreferredWidth(80);
        tblBookings.getColumnModel().getColumn(2).setPreferredWidth(80);
        tblBookings.getColumnModel().getColumn(3).setPreferredWidth(220);
        tblBookings.getColumnModel().getColumn(4).setPreferredWidth(120);
        tblBookings.getColumnModel().getColumn(5).setPreferredWidth(100);
        tblTickets.getColumnModel().getColumn(0).setPreferredWidth(80);
        tblTickets.getColumnModel().getColumn(1).setPreferredWidth(140);
        tblTickets.getColumnModel().getColumn(2).setPreferredWidth(300);
        tblTickets.getColumnModel().getColumn(3).setPreferredWidth(100);
        tblTickets.getColumnModel().getColumn(4).setPreferredWidth(100);
    }
    
    private void refreshPage() {
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("ViewCurrentBookings", new ViewExistingBookingsPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "ViewCurrentBookings");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblViewBookings = new javax.swing.JLabel();
        lblSearchBookings = new javax.swing.JLabel();
        txtSearchBooking = new javax.swing.JTextField();
        cbxCriteria = new javax.swing.JComboBox();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblBookings = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTickets = new javax.swing.JTable();
        lblTickets = new javax.swing.JLabel();
        lblBookings = new javax.swing.JLabel();
        btnAddBooking = new javax.swing.JButton();
        lblRunInformation = new javax.swing.JLabel();
        btnSearch = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();

        lblViewBookings.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblViewBookings.setText("View Bookings");

        lblSearchBookings.setText("Search");

        cbxCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "RUN ID", "PERSON ID", "ORDER DATE" }));

        tblBookings.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Run ID", "Person ID", "Order Date", "Ticket Quantity", "Total Cost (£)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane8.setViewportView(tblBookings);

        tblTickets.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Booking_ID", "Tier Name", "Seat Row", "Seat No"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblTickets);

        lblTickets.setText("Tickets in Selected Booking:");

        lblBookings.setText("Bookings:");

        btnAddBooking.setText("Add Booking");
        btnAddBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddBookingActionPerformed(evt);
            }
        });

        lblRunInformation.setText("Select Booking to View Ticket Information ");

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/refresh-icon-2.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnReset.setText("Reset Search");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblBookings)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(lblViewBookings)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                            .addGap(45, 45, 45)
                                            .addComponent(lblSearchBookings)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtSearchBooking, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(18, 18, 18)
                                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(548, 548, 548)
                                    .addComponent(btnRefresh))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 632, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(194, 194, 194)
                                            .addComponent(lblTickets))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(151, 151, 151)
                                            .addComponent(lblRunInformation)))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(242, 242, 242)
                        .addComponent(btnAddBooking, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnReset)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblViewBookings)
                                .addGap(24, 24, 24)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSearchBookings)
                            .addComponent(txtSearchBooking, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearch)))
                    .addComponent(btnRefresh))
                .addGap(12, 12, 12)
                .addComponent(lblBookings)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblRunInformation)
                        .addGap(25, 25, 25)
                        .addComponent(lblTickets)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAddBooking)
                .addContainerGap(23, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshPage();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnAddBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddBookingActionPerformed
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("NewBooking", new AddBookingPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "NewBooking");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_btnAddBookingActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        filteredList.clear();
        String searchCriteria = txtSearchBooking.getText().toLowerCase();
        for (Booking booking : bookingsList) {
            if (cbxCriteria.getSelectedItem().toString().equals("ID")) {
                try {
                    if (searchCriteria.isEmpty() || booking.getBookingID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(booking);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Booking ID");
                    populateBookingsTable(bookingsList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("RUN ID")) {
                try {
                    if (searchCriteria.isEmpty() || booking.getRun().getID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(booking);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Run ID");
                    populateBookingsTable(bookingsList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("PERSON ID")) {
                try {
                    if (searchCriteria.isEmpty() || booking.getCustomer().getID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(booking);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Person ID");
                    populateBookingsTable(bookingsList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("ORDER DATE")) {
                String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(booking.getDatePlaced());
                if (formattedDate.toLowerCase().contains(searchCriteria)) {
                    filteredList.add(booking);
                }
            }
        }
        populateBookingsTable(filteredList);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        populateBookingsTable(bookingsList);
        txtSearchBooking.setText("");
    }//GEN-LAST:event_btnResetActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddBooking;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox cbxCriteria;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JLabel lblBookings;
    private javax.swing.JLabel lblRunInformation;
    private javax.swing.JLabel lblSearchBookings;
    private javax.swing.JLabel lblTickets;
    private javax.swing.JLabel lblViewBookings;
    private javax.swing.JTable tblBookings;
    private javax.swing.JTable tblTickets;
    private javax.swing.JTextField txtSearchBooking;
    // End of variables declaration//GEN-END:variables
}
