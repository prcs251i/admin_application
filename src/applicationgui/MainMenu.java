package applicationgui;

import java.awt.CardLayout;
import java.awt.ComponentOrientation;
import java.io.IOException;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import org.json.JSONException;

public class MainMenu extends javax.swing.JFrame {

    private CardLayout layoutCard;
    private final String loginData;

    public MainMenu(String data) throws IOException, JSONException {
        initComponents();
        initialiseGUI();
        layoutCard = new CardLayout();
        mainPanel.setLayout(layoutCard);
        mainPanel.add("Home", new HomePanel(layoutCard, mainPanel, data));
        loginData = data;
    }

    public final void initialiseGUI() {
        JMenuItem menuLogOut = new JMenuItem("Log out");
        menuLogOut.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        mainPanel = new javax.swing.JPanel();
        mnbMainMenu = new javax.swing.JMenuBar();
        menuHome = new javax.swing.JMenu();
        menuBooking = new javax.swing.JMenu();
        menuCreateNewBooking = new javax.swing.JMenuItem();
        menuCreateNewGuestBooking = new javax.swing.JMenuItem();
        menuViewCurrentBookings = new javax.swing.JMenuItem();
        menuViewGuestBookings = new javax.swing.JMenuItem();
        menuCustomers = new javax.swing.JMenu();
        menuRegisterCustomers = new javax.swing.JMenuItem();
        menuViewCustomers = new javax.swing.JMenuItem();
        menuEvents = new javax.swing.JMenu();
        menuAddEvents = new javax.swing.JMenuItem();
        menuViewCurrentEvents = new javax.swing.JMenuItem();
        menuPerformers = new javax.swing.JMenu();
        menuAddPerformers = new javax.swing.JMenuItem();
        menuViewExistingPerformer = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        menuAddGenre = new javax.swing.JMenuItem();
        menuViewExistingGenres = new javax.swing.JMenuItem();
        menuVenues = new javax.swing.JMenu();
        menuAddVenues = new javax.swing.JMenuItem();
        menuViewExistingVenues = new javax.swing.JMenuItem();
        menuReviews = new javax.swing.JMenu();
        menuViewExistingReviews = new javax.swing.JMenuItem();
        menuAdmin = new javax.swing.JMenu();
        menuRegisterAdmin = new javax.swing.JMenuItem();
        menuViewExistingAdmin = new javax.swing.JMenuItem();
        menuLogOut = new javax.swing.JMenu();

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mainPanel.setLayout(new java.awt.CardLayout());

        mnbMainMenu.setBackground(java.awt.Color.darkGray);
        mnbMainMenu.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        mnbMainMenu.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        mnbMainMenu.setOpaque(false);

        menuHome.setBorder(null);
        menuHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Home-icon.png"))); // NOI18N
        menuHome.setText("    Home    ");
        menuHome.setToolTipText("");
        menuHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menuHomeMouseClicked(evt);
            }
        });
        mnbMainMenu.add(menuHome);

        menuBooking.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/booking-icon.png"))); // NOI18N
        menuBooking.setText("      Booking    ");

        menuCreateNewBooking.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Add-icon.png"))); // NOI18N
        menuCreateNewBooking.setText("Create Bookings");
        menuCreateNewBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreateNewBookingActionPerformed(evt);
            }
        });
        menuBooking.add(menuCreateNewBooking);

        menuCreateNewGuestBooking.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Add-icon.png"))); // NOI18N
        menuCreateNewGuestBooking.setText("Create Guest Booking");
        menuCreateNewGuestBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCreateNewGuestBookingActionPerformed(evt);
            }
        });
        menuBooking.add(menuCreateNewGuestBooking);

        menuViewCurrentBookings.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewCurrentBookings.setText("View Current Bookings");
        menuViewCurrentBookings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewCurrentBookingsActionPerformed(evt);
            }
        });
        menuBooking.add(menuViewCurrentBookings);

        menuViewGuestBookings.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewGuestBookings.setText("View Guest Bookings");
        menuViewGuestBookings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewGuestBookingsActionPerformed(evt);
            }
        });
        menuBooking.add(menuViewGuestBookings);

        mnbMainMenu.add(menuBooking);

        menuCustomers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Business-Conference-Call-icon.png"))); // NOI18N
        menuCustomers.setText("  Customers   ");

        menuRegisterCustomers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Add-icon.png"))); // NOI18N
        menuRegisterCustomers.setText("Register Customers");
        menuRegisterCustomers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRegisterCustomersActionPerformed(evt);
            }
        });
        menuCustomers.add(menuRegisterCustomers);

        menuViewCustomers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewCustomers.setText("View Existing Customer");
        menuViewCustomers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewCustomersActionPerformed(evt);
            }
        });
        menuCustomers.add(menuViewCustomers);

        mnbMainMenu.add(menuCustomers);

        menuEvents.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Events-icon.png"))); // NOI18N
        menuEvents.setText("     Events       ");

        menuAddEvents.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Add-icon.png"))); // NOI18N
        menuAddEvents.setText("Add Events");
        menuAddEvents.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddEventsActionPerformed(evt);
            }
        });
        menuEvents.add(menuAddEvents);

        menuViewCurrentEvents.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewCurrentEvents.setText("View Current Events");
        menuViewCurrentEvents.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewCurrentEventsActionPerformed(evt);
            }
        });
        menuEvents.add(menuViewCurrentEvents);

        mnbMainMenu.add(menuEvents);

        menuPerformers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Music-Microphone-2-icon.png"))); // NOI18N
        menuPerformers.setText("     Performers     ");

        menuAddPerformers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Add-icon.png"))); // NOI18N
        menuAddPerformers.setText("Add Performers");
        menuAddPerformers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddPerformersActionPerformed(evt);
            }
        });
        menuPerformers.add(menuAddPerformers);

        menuViewExistingPerformer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewExistingPerformer.setText("View Existing Performers");
        menuViewExistingPerformer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewExistingPerformerActionPerformed(evt);
            }
        });
        menuPerformers.add(menuViewExistingPerformer);

        mnbMainMenu.add(menuPerformers);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/performers-icon.png"))); // NOI18N
        jMenu1.setText("     Genres    ");

        menuAddGenre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Add-icon.png"))); // NOI18N
        menuAddGenre.setText("Add Genre");
        menuAddGenre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddGenreActionPerformed(evt);
            }
        });
        jMenu1.add(menuAddGenre);

        menuViewExistingGenres.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewExistingGenres.setText("View Existing Genres");
        menuViewExistingGenres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewExistingGenresActionPerformed(evt);
            }
        });
        jMenu1.add(menuViewExistingGenres);

        mnbMainMenu.add(jMenu1);

        menuVenues.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Venue-icon.png"))); // NOI18N
        menuVenues.setText("      Venues      ");

        menuAddVenues.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Add-icon.png"))); // NOI18N
        menuAddVenues.setText("Add Venues");
        menuAddVenues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAddVenuesActionPerformed(evt);
            }
        });
        menuVenues.add(menuAddVenues);

        menuViewExistingVenues.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewExistingVenues.setText("View Existing Venues");
        menuViewExistingVenues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewExistingVenuesActionPerformed(evt);
            }
        });
        menuVenues.add(menuViewExistingVenues);

        mnbMainMenu.add(menuVenues);

        menuReviews.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Very-Basic-News-icon.png"))); // NOI18N
        menuReviews.setText("      Reviews    ");

        menuViewExistingReviews.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewExistingReviews.setText("View Existing Reviews");
        menuViewExistingReviews.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewExistingReviewsActionPerformed(evt);
            }
        });
        menuReviews.add(menuViewExistingReviews);

        mnbMainMenu.add(menuReviews);

        menuAdmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/users-icon.png"))); // NOI18N
        menuAdmin.setText("     Admin      ");

        menuRegisterAdmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/Add-icon.png"))); // NOI18N
        menuRegisterAdmin.setText("Register Admin");
        menuRegisterAdmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRegisterAdminActionPerformed(evt);
            }
        });
        menuAdmin.add(menuRegisterAdmin);

        menuViewExistingAdmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/view-icon.png"))); // NOI18N
        menuViewExistingAdmin.setText("View Existing Admin");
        menuViewExistingAdmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuViewExistingAdminActionPerformed(evt);
            }
        });
        menuAdmin.add(menuViewExistingAdmin);

        mnbMainMenu.add(menuAdmin);

        menuLogOut.setText("       Log Out       ");
        menuLogOut.setAlignmentX(20.0F);
        menuLogOut.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        menuLogOut.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        menuLogOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menuLogOutMouseClicked(evt);
            }
        });
        mnbMainMenu.add(menuLogOut);

        setJMenuBar(mnbMainMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 1518, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 820, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuCreateNewBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreateNewBookingActionPerformed
        try {
            mainPanel.add("NewBooking", new AddBookingPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "NewBooking");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuCreateNewBookingActionPerformed

    private void menuViewCurrentBookingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewCurrentBookingsActionPerformed
        try {
            mainPanel.add("ViewBookings", new ViewExistingBookingsPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewBookings");
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewCurrentBookingsActionPerformed

    private void menuRegisterCustomersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRegisterCustomersActionPerformed
        mainPanel.add("RegisterNewCustomer", new AddCustomerPanel(layoutCard, mainPanel, loginData));
        layoutCard.show(mainPanel, "RegisterNewCustomer");
    }//GEN-LAST:event_menuRegisterCustomersActionPerformed

    private void menuViewGuestBookingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewGuestBookingsActionPerformed
        try {
            mainPanel.add("ViewGuestBookings", new ViewGuestBookingsPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewGuestBookings");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewGuestBookingsActionPerformed

    private void menuAddVenuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddVenuesActionPerformed
        mainPanel.add("AddNewVenue", new AddVenuePanel(layoutCard, mainPanel, loginData));
        layoutCard.show(mainPanel, "AddNewVenue");
    }//GEN-LAST:event_menuAddVenuesActionPerformed

    private void menuViewExistingVenuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewExistingVenuesActionPerformed
        try {
            mainPanel.add("ViewExistingVenues", new ViewExistingVenuesPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewExistingVenues");
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewExistingVenuesActionPerformed

    private void menuAddPerformersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddPerformersActionPerformed
        try {
            mainPanel.add("AddNewPerformers", new AddPerformerPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "AddNewPerformers");
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuAddPerformersActionPerformed

    private void menuViewExistingPerformerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewExistingPerformerActionPerformed
        try {
            mainPanel.add("ViewExistingPerformers", new ViewExistingPerformersPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewExistingPerformers");
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewExistingPerformerActionPerformed

    private void menuViewCustomersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewCustomersActionPerformed
        try {
            mainPanel.add("ViewExistingCustomers", new ViewExistingCustomersPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewExistingCustomers");
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewCustomersActionPerformed

    private void menuViewCurrentEventsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewCurrentEventsActionPerformed
        try {
            mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewExistingEvents");
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewCurrentEventsActionPerformed

    private void menuAddEventsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddEventsActionPerformed
        try {
            mainPanel.add("AddNewEvents", new AddEventPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "AddNewEvents");
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuAddEventsActionPerformed

    private void menuHomeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuHomeMouseClicked
        
        try {
            mainPanel.add("Home", new HomePanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "Home");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuHomeMouseClicked

    private void menuViewExistingReviewsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewExistingReviewsActionPerformed
        try {
            mainPanel.add("ViewExistingReviews", new ViewExistingReviewsPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewExistingReviews");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewExistingReviewsActionPerformed

    private void menuViewExistingAdminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewExistingAdminActionPerformed
        try {
            mainPanel.add("ViewExistingAdministrators", new ViewExistingAdministratorsPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewExistingAdministrators");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewExistingAdminActionPerformed

    private void menuLogOutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuLogOutMouseClicked
        int response = JOptionPane.showConfirmDialog(null, "Are you sure you would like to log out? All unsaved changes will be lost", "Confirm",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.YES_OPTION) {
            this.dispose();
            new LogIn().setVisible(true);
        }
    }//GEN-LAST:event_menuLogOutMouseClicked

    private void menuRegisterAdminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRegisterAdminActionPerformed
        mainPanel.add("RegisterNewAdministrator", new AddAdministratorPanel(layoutCard, mainPanel, loginData));
        layoutCard.show(mainPanel, "RegisterNewAdministrator");
    }//GEN-LAST:event_menuRegisterAdminActionPerformed

    private void menuCreateNewGuestBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCreateNewGuestBookingActionPerformed
        try {
            mainPanel.add("NewGuestBooking", new AddGuestBookingPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "NewGuestBooking");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuCreateNewGuestBookingActionPerformed

    private void menuAddGenreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAddGenreActionPerformed
        mainPanel.add("NewGenre", new AddGenrePanel(layoutCard, mainPanel, loginData));
        layoutCard.show(mainPanel, "NewGenre");
    }//GEN-LAST:event_menuAddGenreActionPerformed

    private void menuViewExistingGenresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuViewExistingGenresActionPerformed
        try {
            mainPanel.add("ViewExistingGenres", new ViewExistingGenresPanel(layoutCard, mainPanel, loginData));
            layoutCard.show(mainPanel, "ViewExistingGenres");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_menuViewExistingGenresActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuItem menuAddEvents;
    private javax.swing.JMenuItem menuAddGenre;
    private javax.swing.JMenuItem menuAddPerformers;
    private javax.swing.JMenuItem menuAddVenues;
    private javax.swing.JMenu menuAdmin;
    private javax.swing.JMenu menuBooking;
    private javax.swing.JMenuItem menuCreateNewBooking;
    private javax.swing.JMenuItem menuCreateNewGuestBooking;
    private javax.swing.JMenu menuCustomers;
    private javax.swing.JMenu menuEvents;
    private javax.swing.JMenu menuHome;
    private javax.swing.JMenu menuLogOut;
    private javax.swing.JMenu menuPerformers;
    private javax.swing.JMenuItem menuRegisterAdmin;
    private javax.swing.JMenuItem menuRegisterCustomers;
    private javax.swing.JMenu menuReviews;
    private javax.swing.JMenu menuVenues;
    private javax.swing.JMenuItem menuViewCurrentBookings;
    private javax.swing.JMenuItem menuViewCurrentEvents;
    private javax.swing.JMenuItem menuViewCustomers;
    private javax.swing.JMenuItem menuViewExistingAdmin;
    private javax.swing.JMenuItem menuViewExistingGenres;
    private javax.swing.JMenuItem menuViewExistingPerformer;
    private javax.swing.JMenuItem menuViewExistingReviews;
    private javax.swing.JMenuItem menuViewExistingVenues;
    private javax.swing.JMenuItem menuViewGuestBookings;
    private javax.swing.JMenuBar mnbMainMenu;
    // End of variables declaration//GEN-END:variables
}
