package applicationgui;

import AdministrationApplication.Address;
import AdministrationApplication.Customer;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import AdministrationApplication.Utilities.Validator;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */

public class AddCustomerPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private HTTPRequestHandler httpHandler;
    private final String loginData;
    String[] httpInfo;
    private Validator validator;

    public AddCustomerPanel(CardLayout layoutCard, JPanel panel, String data) {
        cardLayout = layoutCard;
        mainPanel = panel;
        httpHandler = new HTTPRequestHandler();
        initComponents();
        resetGUI();
        loginData = data;
        validator = new Validator();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblRegisterCustomer = new javax.swing.JLabel();
        pnlRegisterCustomer = new javax.swing.JPanel();
        lblFirstName = new javax.swing.JLabel();
        lblLastName = new javax.swing.JLabel();
        lblEmailAddress = new javax.swing.JLabel();
        lblConfirmEmailAddress = new javax.swing.JLabel();
        lblPassword = new javax.swing.JLabel();
        lblConfirmPass = new javax.swing.JLabel();
        txtConfirmEmailAddress = new javax.swing.JTextField();
        txtEmailAddress = new javax.swing.JTextField();
        txtLastName = new javax.swing.JTextField();
        txtFirstName = new javax.swing.JTextField();
        txtPassword = new javax.swing.JTextField();
        txtConfirmPass = new javax.swing.JTextField();
        lblBillingAddr = new javax.swing.JLabel();
        lblAddressLine1 = new javax.swing.JLabel();
        lblAddressLine2 = new javax.swing.JLabel();
        lblCity = new javax.swing.JLabel();
        lblCounty = new javax.swing.JLabel();
        lblPostCode = new javax.swing.JLabel();
        txtAddressLine1 = new javax.swing.JTextField();
        txtAddressLine2 = new javax.swing.JTextField();
        txtCity = new javax.swing.JTextField();
        txtCounty = new javax.swing.JTextField();
        lblAccountInfo = new javax.swing.JLabel();
        txtPostcode = new javax.swing.JTextField();
        btnConfirm = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        lblPaymentInfo = new javax.swing.JLabel();
        lblCardType = new javax.swing.JLabel();
        lblCardNo = new javax.swing.JLabel();
        txtCardNo = new javax.swing.JTextField();
        cbxCardType = new javax.swing.JComboBox();
        txtExpDate = new javax.swing.JLabel();
        lblSecCode = new javax.swing.JLabel();
        txtSecCode = new javax.swing.JTextField();
        lblHomeNo = new javax.swing.JLabel();
        lblMobNo = new javax.swing.JLabel();
        txtMobileNo = new javax.swing.JTextField();
        txtHomeNo = new javax.swing.JTextField();
        dtpExpiryDate = new org.jdesktop.swingx.JXDatePicker();
        lblReqField = new javax.swing.JLabel();
        lblAsterisk = new javax.swing.JLabel();

        lblRegisterCustomer.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblRegisterCustomer.setText("Register New Customer");

        lblFirstName.setText("First Name: *");

        lblLastName.setText("Last Name: *");

        lblEmailAddress.setText("Email Address: *");

        lblConfirmEmailAddress.setText("Confirm Email Address: *");

        lblPassword.setText("Password: *");

        lblConfirmPass.setText("Confirm Password: *");

        lblBillingAddr.setText("Contact Information");

        lblAddressLine1.setText("Address Line 1: *");

        lblAddressLine2.setText("Address Line 2:");

        lblCity.setText("City: *");

        lblCounty.setText("County: *");

        lblPostCode.setText("Postcode: *");

        lblAccountInfo.setText("Account Information");

        btnConfirm.setText("Register Customer");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        lblPaymentInfo.setText("Payment Information");

        lblCardType.setText("Card Type: *");

        lblCardNo.setText("Card Number: *");

        cbxCardType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "VISA", "VISA Electron", "MasterCard", "Maestro", "American Express" }));

        txtExpDate.setText("Expiry Date: *");

        lblSecCode.setText("Security Code: *");

        lblHomeNo.setText("Home Phone Number:");

        lblMobNo.setText("Mobile Phone Number:");

        javax.swing.GroupLayout pnlRegisterCustomerLayout = new javax.swing.GroupLayout(pnlRegisterCustomer);
        pnlRegisterCustomer.setLayout(pnlRegisterCustomerLayout);
        pnlRegisterCustomerLayout.setHorizontalGroup(
            pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblConfirmEmailAddress)
                    .addComponent(lblFirstName, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblLastName, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblEmailAddress, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblPassword, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblConfirmPass, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(lblAccountInfo))
                    .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtPassword, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                        .addComponent(txtConfirmEmailAddress)
                        .addComponent(txtEmailAddress)
                        .addComponent(txtLastName)
                        .addComponent(txtFirstName)
                        .addComponent(txtConfirmPass)))
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                        .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(31, 31, 31)
                                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(26, 26, 26)
                                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                        .addGap(0, 33, Short.MAX_VALUE)
                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(lblCardNo)
                                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                                        .addComponent(lblAddressLine1)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(txtAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(lblMobNo)
                                                            .addComponent(lblHomeNo))
                                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                                                .addGap(18, 18, 18)
                                                                .addComponent(txtMobileNo, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlRegisterCustomerLayout.createSequentialGroup()
                                                                .addGap(18, 18, 18)
                                                                .addComponent(txtHomeNo, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGap(81, 81, 81))
                                                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                            .addComponent(lblCity)
                                                            .addComponent(lblAddressLine2)
                                                            .addComponent(lblCounty)
                                                            .addComponent(lblPostCode))
                                                        .addGap(18, 18, 18)
                                                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(txtCounty, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(txtAddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(txtPostcode, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGap(40, 40, 40)
                                                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txtExpDate, javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(lblSecCode, javax.swing.GroupLayout.Alignment.TRAILING)))))))
                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblCardType)))
                        .addGap(18, 18, 18)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                                .addGap(52, 52, 52)
                                .addComponent(lblPaymentInfo))
                            .addComponent(txtCardNo, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxCardType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSecCode, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dtpExpiryDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18))
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGap(225, 225, 225)
                        .addComponent(lblBillingAddr)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        pnlRegisterCustomerLayout.setVerticalGroup(
            pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBillingAddr)
                    .addComponent(lblAccountInfo)
                    .addComponent(lblPaymentInfo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFirstName)
                    .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxCardType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCardType)
                    .addComponent(lblHomeNo)
                    .addComponent(txtHomeNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblLastName)
                    .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCardNo)
                    .addComponent(txtCardNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMobNo)
                    .addComponent(txtMobileNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblEmailAddress)
                            .addComponent(txtEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtExpDate)
                            .addComponent(dtpExpiryDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblConfirmEmailAddress)
                            .addComponent(txtConfirmEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSecCode)
                            .addComponent(txtSecCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPassword)
                            .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblConfirmPass)
                            .addComponent(txtConfirmPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlRegisterCustomerLayout.createSequentialGroup()
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAddressLine1)
                            .addComponent(txtAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAddressLine2)
                            .addComponent(txtAddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCity)
                            .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCounty)
                            .addComponent(txtCounty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPostCode)
                            .addComponent(txtPostcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                        .addGroup(pnlRegisterCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnClear)
                            .addComponent(btnConfirm)
                            .addComponent(btnCancel))
                        .addGap(24, 24, 24))))
        );

        lblReqField.setForeground(new java.awt.Color(255, 51, 51));
        lblReqField.setText("= Required Field");

        lblAsterisk.setText("* ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblRegisterCustomer)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(222, 222, 222)
                        .addComponent(lblAsterisk)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblReqField))
                    .addComponent(pnlRegisterCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(244, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblRegisterCustomer)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblReqField)
                    .addComponent(lblAsterisk))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlRegisterCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(212, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void resetGUI() {
        txtFirstName.setText("");
        txtLastName.setText("");
        txtEmailAddress.setText("");
        txtConfirmEmailAddress.setText("");
        txtPassword.setText("");
        txtConfirmPass.setText("");
        txtAddressLine1.setText("");
        txtAddressLine2.setText("");
        txtCity.setText("");
        txtCounty.setText("");
        txtPostcode.setText("");
        txtHomeNo.setText("");
        txtMobileNo.setText("");
        txtCardNo.setText("");
        cbxCardType.setSelectedIndex(0);
        dtpExpiryDate.setDate(new Date());
        txtSecCode.setText("");
    }

    private String constructCustomerJsonString(Customer newCustomer) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("FORENAME", newCustomer.getSurname());
        j.put("SURNAME", newCustomer.getForename());
        j.put("EMAIL", newCustomer.getEmail());
        j.put("PASSWORD_HASH", newCustomer.getPassword());
        j.put("ADDRESS_LINE_1", newCustomer.getAddress().getAddressLine1());
        j.put("ADDRESS_LINE_2", newCustomer.getAddress().getAddressLine2());
        j.put("CITY", newCustomer.getAddress().getCity());
        j.put("COUNTY", newCustomer.getAddress().getCounty());
        j.put("POSTCODE", newCustomer.getAddress().getPostcode());
        j.put("HOME_PHONE", newCustomer.getHomeNumber());
        j.put("MOB_PHONE", newCustomer.getMobNumber());
        j.put("CARD_TYPE", newCustomer.getCardType());
        j.put("CARD_NUMBER", newCustomer.getCardNumber());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String formattedDate = formatter.format(newCustomer.getExpiryDate());
        j.put("EXPIRY_DATE", formattedDate);
        j.put("SECURITY_CODE", newCustomer.getSecurityCode());
        return j.toString();
    }

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        if (!txtFirstName.getText().isEmpty()) {
            if (!txtLastName.getText().isEmpty()) {
                if (!txtEmailAddress.getText().isEmpty()) {
                    if (validator.validateEmail(txtEmailAddress.getText()) == true) {
                        if (!txtConfirmEmailAddress.getText().isEmpty() && txtConfirmEmailAddress.getText().equals(txtEmailAddress.getText())) {
                            if (!txtPassword.getText().isEmpty()) {
                                if (txtPassword.getText().length() > 5) {
                                    if (!txtConfirmPass.getText().isEmpty() && txtConfirmPass.getText().equals(txtPassword.getText())) {
                                        if (txtHomeNo.getText().isEmpty()) {
                                        } else if (validator.validateHomeNumber(txtHomeNo.getText()) != true) {
                                            JOptionPane.showMessageDialog(this, "Please enter a valid Home Phone number");
                                            return;
                                        }
                                        if (!txtAddressLine1.getText().isEmpty()) {
                                            if (!txtCity.getText().isEmpty()) {
                                                if (!txtCounty.getText().isEmpty()) {
                                                    if (!txtPostcode.getText().isEmpty()) {
                                                        if (validator.validateUKPostcode(txtPostcode.getText()) == true) {
                                                            if (cbxCardType.getSelectedIndex() != -1) {
                                                                if (!txtCardNo.getText().isEmpty()) {
                                                                    if (validator.validateCardNumber(txtCardNo.getText()) == true) {
                                                                        if (dtpExpiryDate.getDate() != null) {
                                                                            if (!txtSecCode.getText().isEmpty()) {
                                                                                if (validator.validateSecurityNo(txtSecCode.getText()) == true) {
                                                                                    try {
                                                                                        if (httpHandler.authenticate(loginData)[0].contains("20")) {
                                                                                            Address newAddress = new Address(txtAddressLine1.getText(), txtAddressLine2.getText(),
                                                                                                    txtCity.getText(), txtCounty.getText(), txtPostcode.getText().replaceAll(" ", "").toUpperCase());
                                                                                            Customer newCustomer = new Customer(txtFirstName.getText(), txtLastName.getText(),
                                                                                                    txtEmailAddress.getText(), txtHomeNo.getText(), txtMobileNo.getText(), newAddress,
                                                                                                    cbxCardType.getSelectedItem().toString(), txtCardNo.getText(), dtpExpiryDate.getDate(), txtSecCode.getText(), txtPassword.getText());
                                                                                            String data = constructCustomerJsonString(newCustomer);
                                                                                            URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/customers");
                                                                                            httpInfo = httpHandler.sendPOSTRequest(data, url);
                                                                                            if (httpInfo[0].contains("20")) {
                                                                                                int confirmationMessage = JOptionPane.showConfirmDialog(this, httpInfo[0]
                                                                                                        + "Customer successfully Registered, Would you like to make a new booking for this Customer? ", "Registration Successful", JOptionPane.OK_CANCEL_OPTION);
                                                                                                if (confirmationMessage == JOptionPane.YES_OPTION) {
                                                                                                    cardLayout.removeLayoutComponent(this);
                                                                                                    mainPanel.add("AddNewBooking", new AddBookingPanel(cardLayout, mainPanel, loginData));
                                                                                                    cardLayout.show(mainPanel, "AddNewBooking");
                                                                                                } else {
                                                                                                    cardLayout.removeLayoutComponent(this);
                                                                                                    mainPanel.add("ViewExistingCustomers", new ViewExistingCustomersPanel(cardLayout, mainPanel, loginData));
                                                                                                    cardLayout.show(mainPanel, "ViewExistingCustomers");
                                                                                                }
                                                                                            } else {
                                                                                                JOptionPane.showMessageDialog(this, httpInfo[0] + "\n Customer NOT added");
                                                                                            }
                                                                                        } else {
                                                                                            JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                                                                                            mainPanel.removeAll();
                                                                                            new LogIn().setVisible(true);
                                                                                        }
                                                                                    } catch (MalformedURLException ex) {
                                                                                        JOptionPane.showMessageDialog(this, httpInfo[0] + "\n Customer NOT added");
                                                                                    } catch (IOException | JSONException ex) {
                                                                                        JOptionPane.showMessageDialog(this, httpInfo[0] + "\n Customer NOT added");
                                                                                    }
                                                                                } else {
                                                                                    JOptionPane.showMessageDialog(this, "Please enter a valid Payment Card Security Code for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                                                                }
                                                                            } else {
                                                                                JOptionPane.showMessageDialog(this, "Please enter a Payment Card Security Code for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                                                            }
                                                                        } else {
                                                                            JOptionPane.showMessageDialog(this, "Please select an Payment Card Expiry Date for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                                                        }
                                                                    } else {
                                                                        JOptionPane.showMessageDialog(this, "Please enter a valid Payment Card Number for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                                                    }
                                                                } else {
                                                                    JOptionPane.showMessageDialog(this, "Please enter a Payment Card Number for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                                                }
                                                            } else {
                                                                JOptionPane.showMessageDialog(this, "Please select a Payment Card Type for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                                            }
                                                        } else {
                                                            JOptionPane.showMessageDialog(this, "Please enter a valid Postcode", "Warning", JOptionPane.OK_OPTION);
                                                        }
                                                    } else {
                                                        JOptionPane.showMessageDialog(this, "Please enter a Postcode for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                                    }
                                                } else {
                                                    JOptionPane.showMessageDialog(this, "Please enter a County of Residence for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                                }
                                            } else {
                                                JOptionPane.showMessageDialog(this, "Please enter a City of Residence for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                            }
                                        } else {
                                            JOptionPane.showMessageDialog(this, "Please enter an Address Line 1 for the new Customer", "Warning", JOptionPane.OK_OPTION);
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(this, "The confirmation password you have entered does not match", "Warning", JOptionPane.OK_OPTION);
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(this, "Password must be atleast 6 characters long", "Warning", JOptionPane.OK_OPTION);
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Please enter a Password for the new Customer", "Warning", JOptionPane.OK_OPTION);
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "The confirmation email you have entered does not match", "Warning", JOptionPane.OK_OPTION);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Please enter a valid email address", "Warning", JOptionPane.OK_OPTION);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please enter an Email Address for the new Customer", "Warning", JOptionPane.OK_OPTION);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please enter a Surname for the new Customer", "Warning", JOptionPane.OK_OPTION);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please enter a Forename for the new Customer", "Warning", JOptionPane.OK_OPTION);
        }
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel these changes?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingCustomers", new ViewExistingCustomersPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingCustomers");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        resetGUI();
    }//GEN-LAST:event_btnClearActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JComboBox cbxCardType;
    private org.jdesktop.swingx.JXDatePicker dtpExpiryDate;
    private javax.swing.JLabel lblAccountInfo;
    private javax.swing.JLabel lblAddressLine1;
    private javax.swing.JLabel lblAddressLine2;
    private javax.swing.JLabel lblAsterisk;
    private javax.swing.JLabel lblBillingAddr;
    private javax.swing.JLabel lblCardNo;
    private javax.swing.JLabel lblCardType;
    private javax.swing.JLabel lblCity;
    private javax.swing.JLabel lblConfirmEmailAddress;
    private javax.swing.JLabel lblConfirmPass;
    private javax.swing.JLabel lblCounty;
    private javax.swing.JLabel lblEmailAddress;
    private javax.swing.JLabel lblFirstName;
    private javax.swing.JLabel lblHomeNo;
    private javax.swing.JLabel lblLastName;
    private javax.swing.JLabel lblMobNo;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblPaymentInfo;
    private javax.swing.JLabel lblPostCode;
    private javax.swing.JLabel lblRegisterCustomer;
    private javax.swing.JLabel lblReqField;
    private javax.swing.JLabel lblSecCode;
    private javax.swing.JPanel pnlRegisterCustomer;
    private javax.swing.JTextField txtAddressLine1;
    private javax.swing.JTextField txtAddressLine2;
    private javax.swing.JTextField txtCardNo;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtConfirmEmailAddress;
    private javax.swing.JTextField txtConfirmPass;
    private javax.swing.JTextField txtCounty;
    private javax.swing.JTextField txtEmailAddress;
    private javax.swing.JLabel txtExpDate;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtHomeNo;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtMobileNo;
    private javax.swing.JTextField txtPassword;
    private javax.swing.JTextField txtPostcode;
    private javax.swing.JTextField txtSecCode;
    // End of variables declaration//GEN-END:variables
}
