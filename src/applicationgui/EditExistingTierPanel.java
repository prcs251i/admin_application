package applicationgui;

import AdministrationApplication.Tier;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */

public class EditExistingTierPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private Tier selectedTier;
    private HTTPRequestHandler httpHandler;
    private final String loginData;

    public EditExistingTierPanel(CardLayout layoutCard, JPanel panel, Tier tier, String data) {
        selectedTier = tier;
        cardLayout = layoutCard;
        mainPanel = panel;
        initComponents();
        httpHandler = new HTTPRequestHandler();
        fillExistingTierFields();
        loginData = data;
    }

    private void fillExistingTierFields() {
        txtID.setText(String.valueOf(selectedTier.getTierId()));
        if (selectedTier.getIsSeated()) {
            cbxTierType.setSelectedItem("Seating");
        } else {
            cbxTierType.setSelectedItem("Standing");
        }
        txtTierName.setText(selectedTier.getTierName());
        spnNoOfRows.setValue(selectedTier.getSeatRows());
        spnNoOfColumns.setValue(selectedTier.getSeatColumns());
        txtTierCapacity.setText(String.valueOf(selectedTier.getCapacity()));
    }

    private String constructTierJsonString(Tier newTier) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("ID", newTier.getTierId());
        j.put("TIER_NAME", newTier.getTierName());
        j.put("VENUE_ID", newTier.getVenueId());
        if (newTier.getIsSeated() == true) {
            j.put("SEATED", "Y");
        } else {
            j.put("SEATED", "N");
        }
        j.put("CAPACITY", newTier.getCapacity());
        j.put("SEAT_ROWS", newTier.getSeatRows());
        j.put("SEAT_COLUMNS", newTier.getSeatColumns());
        return j.toString();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTier = new javax.swing.JPanel();
        lblTierName = new javax.swing.JLabel();
        txtTierName = new javax.swing.JTextField();
        cbxTierType = new javax.swing.JComboBox<String>();
        lblTierType = new javax.swing.JLabel();
        txtTierCapacity = new javax.swing.JTextField();
        lblTierCapacity = new javax.swing.JLabel();
        lblNoOfRows = new javax.swing.JLabel();
        spnNoOfRows = new javax.swing.JSpinner();
        lblNoOfColumns = new javax.swing.JLabel();
        spnNoOfColumns = new javax.swing.JSpinner();
        lblID = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        lblEditExistingTier = new javax.swing.JLabel();
        btnUpdate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        lblTierName.setText("Tier Name:");

        cbxTierType.setEditable(true);
        cbxTierType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seating", "Standing" }));
        cbxTierType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxTierTypeActionPerformed(evt);
            }
        });

        lblTierType.setText("Tier Type:");

        txtTierCapacity.setEditable(false);

        lblTierCapacity.setText("Tier Capacity:");

        lblNoOfRows.setText("No. of Rows:");

        spnNoOfRows.setModel(new javax.swing.SpinnerNumberModel(1, 1, 250, 1));
        spnNoOfRows.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spnNoOfRowsStateChanged(evt);
            }
        });

        lblNoOfColumns.setText("No. of Columns :");

        spnNoOfColumns.setModel(new javax.swing.SpinnerNumberModel(1, 1, 250, 1));
        spnNoOfColumns.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spnNoOfColumnsStateChanged(evt);
            }
        });

        lblID.setText("ID:");

        txtID.setEditable(false);

        javax.swing.GroupLayout pnlTierLayout = new javax.swing.GroupLayout(pnlTier);
        pnlTier.setLayout(pnlTierLayout);
        pnlTierLayout.setHorizontalGroup(
            pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTierLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlTierLayout.createSequentialGroup()
                        .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNoOfColumns)
                            .addComponent(lblTierCapacity, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblNoOfRows, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(12, 12, 12)
                        .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtTierCapacity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(spnNoOfRows, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                                .addComponent(spnNoOfColumns, javax.swing.GroupLayout.Alignment.LEADING))))
                    .addGroup(pnlTierLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblTierType)
                            .addComponent(lblTierName)
                            .addComponent(lblID))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbxTierType, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTierName, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlTierLayout.setVerticalGroup(
            pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTierLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblID)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTierType)
                    .addComponent(cbxTierType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTierName)
                    .addComponent(txtTierName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNoOfRows)
                    .addComponent(spnNoOfRows, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNoOfColumns)
                    .addComponent(spnNoOfColumns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(pnlTierLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTierCapacity)
                    .addComponent(txtTierCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblEditExistingTier.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblEditExistingTier.setText("Edit Existing Tier");

        btnUpdate.setText("Update Tier");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblEditExistingTier))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(pnlTier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(97, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblEditExistingTier)
                .addGap(23, 23, 23)
                .addComponent(pnlTier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate)
                    .addComponent(btnCancel))
                .addGap(31, 31, 31))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cbxTierTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxTierTypeActionPerformed
        if (cbxTierType.getSelectedIndex() == 0) {
            txtTierCapacity.setEditable(false);
            lblNoOfRows.setVisible(true);
            spnNoOfRows.setVisible(true);
            lblNoOfColumns.setVisible(true);
            spnNoOfColumns.setVisible(true);
        } else {
            txtTierCapacity.setEditable(true);
            lblNoOfRows.setVisible(false);
            spnNoOfRows.setVisible(false);
            lblNoOfColumns.setVisible(false);
            spnNoOfColumns.setVisible(false);
            txtTierCapacity.setEnabled(true);
        }
    }//GEN-LAST:event_cbxTierTypeActionPerformed

    private void spnNoOfRowsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnNoOfRowsStateChanged
        int noOfRows = (Integer) spnNoOfRows.getValue();
        int seatsPerRow = (Integer) spnNoOfColumns.getValue();
        int tierCapacity = noOfRows * seatsPerRow;
        txtTierCapacity.setText(String.valueOf(tierCapacity));
    }//GEN-LAST:event_spnNoOfRowsStateChanged

    private void spnNoOfColumnsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnNoOfColumnsStateChanged
        int noOfRows = (Integer) spnNoOfRows.getValue();
        int seatsPerRow = (Integer) spnNoOfColumns.getValue();
        int tierCapacity = noOfRows * seatsPerRow;
        txtTierCapacity.setText(String.valueOf(tierCapacity));
    }//GEN-LAST:event_spnNoOfColumnsStateChanged

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        if (cbxTierType.getSelectedIndex() != -1) {
            if (!txtTierName.getText().isEmpty()) {
                if ((int) spnNoOfRows.getValue() > 0 | (int) spnNoOfRows.getValue() < 251) {
                    if ((int) spnNoOfColumns.getValue() > 0 | (int) spnNoOfColumns.getValue() < 251) {
                        try {
                            if (httpHandler.authenticate(loginData)[0].contains("20")) {
                                boolean seated = false;
                                if (cbxTierType.getSelectedItem().toString().equals("Seating")) {
                                    seated = true;
                                    selectedTier.setSeatRows((int) spnNoOfRows.getValue());
                                    selectedTier.setSeatColumns((int) spnNoOfColumns.getValue());
                                } else {
                                    selectedTier.setSeatRows(0);
                                    selectedTier.setSeatColumns(0);
                                }
                                selectedTier.setTierName(txtTierName.getText());
                                selectedTier.setIsSeated(seated);
                                selectedTier.setCapacity(Integer.parseInt(txtTierCapacity.getText()));
                                String data = constructTierJsonString(selectedTier);
                                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/tiers/" + selectedTier.getTierId());
                                String httpInfo = httpHandler.sendPUTRequest(data, url);
                                if (httpInfo.contains("20")) {
                                    JOptionPane.showMessageDialog(this, httpInfo + "\n Tier successfully updated");
                                    cardLayout.removeLayoutComponent(this);
                                    mainPanel.add("ViewExistingVenues", new ViewExistingVenuesPanel(cardLayout, mainPanel, loginData));
                                    cardLayout.show(mainPanel, "ViewExistingVenues");
                                } else {
                                    JOptionPane.showMessageDialog(this, httpInfo + "\n ERROR, Tier NOT Updated");
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                                mainPanel.removeAll();
                                new LogIn().setVisible(true);
                            }
                        } catch (MalformedURLException ex) {
                            JOptionPane.showMessageDialog(this, "ERROR Tier NOT Updated \n " + ex);
                        } catch (IOException ex) {
                            JOptionPane.showMessageDialog(this, "ERROR Tier NOT Updated \n " + ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Number of Seat Columns must be between 1 and 250");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Number of Seat Rows must be between 1 and 250");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please enter a Tier name");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Tier type");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel these changes?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            try {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingVenues", new ViewExistingVenuesPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingVenues");
            } catch (IOException | JSONException ex) {
                JOptionPane.showMessageDialog(this, " ERROR \n" + ex);
            }
        }
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> cbxTierType;
    private javax.swing.JLabel lblEditExistingTier;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblNoOfColumns;
    private javax.swing.JLabel lblNoOfRows;
    private javax.swing.JLabel lblTierCapacity;
    private javax.swing.JLabel lblTierName;
    private javax.swing.JLabel lblTierType;
    private javax.swing.JPanel pnlTier;
    private javax.swing.JSpinner spnNoOfColumns;
    private javax.swing.JSpinner spnNoOfRows;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtTierCapacity;
    private javax.swing.JTextField txtTierName;
    // End of variables declaration//GEN-END:variables
}
