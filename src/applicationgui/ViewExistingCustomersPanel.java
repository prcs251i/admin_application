package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Customer;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;

/**
 *
 * @author Joel Gooch
 */

public class ViewExistingCustomersPanel extends javax.swing.JPanel {

    private DefaultTableModel customersModel;
    private CardLayout cardLayout;
    private JPanel mainPanel;
    private HashMap<Integer, Customer> customerHashMap;
    private List<Customer> customersList;
    private List<Customer> filteredList;
    private Customer selectedCustomer;
    private final String loginData;
    private HTTPRequestHandler httpHandler;

    public ViewExistingCustomersPanel(CardLayout layoutCard, JPanel main, String data) throws MalformedURLException, IOException, JSONException {
        cardLayout = layoutCard;
        mainPanel = main;
        customerHashMap = new HashMap<>();
        initComponents();
        initialiseTable();
        JsonClassGenerator generator = new JsonClassGenerator();
        filteredList = new ArrayList();
        customersList = generator.generateCustomers();
        for (Customer customer : customersList) {
            customerHashMap.put(customer.getID(), customer);
        }
        populateCustomersTable(customersList);
        loginData = data;
        httpHandler = new HTTPRequestHandler();
    }

    private void initialiseTable() {
        customersModel = (DefaultTableModel) tblCustomers.getModel();
        ListSelectionModel customerSelectionModel = tblCustomers.getSelectionModel();
        customerSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    if (tblCustomers.getSelectedRow() != -1) {
                        int selectedCustomerID = (int) tblCustomers.getValueAt(tblCustomers.getSelectedRow(), 0);
                        selectedCustomer = customerHashMap.get(selectedCustomerID);
                    }
                }
            }
        });
        setTableColumnWidths();
    }

    private void setTableColumnWidths() {
        tblCustomers.getColumnModel().getColumn(0).setPreferredWidth(60);
        tblCustomers.getColumnModel().getColumn(1).setPreferredWidth(100);
        tblCustomers.getColumnModel().getColumn(2).setPreferredWidth(100);
        tblCustomers.getColumnModel().getColumn(3).setPreferredWidth(200);
        tblCustomers.getColumnModel().getColumn(4).setPreferredWidth(130);
        tblCustomers.getColumnModel().getColumn(5).setPreferredWidth(130);
        tblCustomers.getColumnModel().getColumn(6).setPreferredWidth(120);
        tblCustomers.getColumnModel().getColumn(7).setPreferredWidth(130);
        tblCustomers.getColumnModel().getColumn(8).setPreferredWidth(80);
        tblCustomers.getColumnModel().getColumn(9).setPreferredWidth(60);
        tblCustomers.getColumnModel().getColumn(10).setPreferredWidth(160);
        tblCustomers.getColumnModel().getColumn(11).setPreferredWidth(100);
        tblCustomers.getColumnModel().getColumn(12).setPreferredWidth(100);
        tblCustomers.getColumnModel().getColumn(13).setPreferredWidth(100);
    }

    private void populateCustomersTable(List<Customer> customersList) {
        customersModel.setNumRows(0);
        Object[] row = new Object[14];
        for (int i = 0; i < customersList.size(); i++) {
            row[0] = customersList.get(i).getID();
            row[1] = customersList.get(i).getForename();
            row[2] = customersList.get(i).getSurname();
            row[3] = customersList.get(i).getEmail();
            row[4] = customersList.get(i).getHomeNumber();
            row[5] = customersList.get(i).getMobNumber();
            row[6] = customersList.get(i).getCardType();
            row[7] = customersList.get(i).getCardNumber();
            String formattedDate = new SimpleDateFormat("MM/yyyy").format(customersList.get(i).getExpiryDate());
            row[8] = formattedDate;
            row[9] = customersList.get(i).getSecurityCode();
            row[10] = customersList.get(i).getAddress().getAddressLine1();
            row[11] = customersList.get(i).getAddress().getCity();
            row[12] = customersList.get(i).getAddress().getCounty();
            row[13] = customersList.get(i).getAddress().getPostcode();
            customersModel.addRow(row);
        }
    }

    private void refreshPage() {
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("ViewExistingCustomers", new ViewExistingCustomersPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "ViewExistingCustomers");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblViewExistingCustomers = new javax.swing.JLabel();
        lblSearchCustomer = new javax.swing.JLabel();
        txtSearchCustomer = new javax.swing.JTextField();
        cbxCriteria = new javax.swing.JComboBox();
        scpCustomers = new javax.swing.JScrollPane();
        tblCustomers = new javax.swing.JTable();
        btnAddNewCustomer = new javax.swing.JButton();
        btnEditCustomer = new javax.swing.JButton();
        btnDeleteCustomer = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        lblCustomers = new javax.swing.JLabel();
        btnReset = new javax.swing.JButton();

        lblViewExistingCustomers.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblViewExistingCustomers.setText("View Customers");

        lblSearchCustomer.setText("Search");

        cbxCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "FIRST NAME", "LAST NAME", "EMAIL", "ADDRESS LINE 1", "CITY ", "COUNTY", "POSTCODE" }));

        tblCustomers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "First Name", "Last Name", "Email", "Home No", "Mob No", "Card Type", "Card Number", "Exp Date", "Sec Code", "Address Line 1", "City", "County", "Postcode"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        scpCustomers.setViewportView(tblCustomers);

        btnAddNewCustomer.setText("Register New Customer");
        btnAddNewCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddNewCustomerActionPerformed(evt);
            }
        });

        btnEditCustomer.setText("Edit Selected Customer");
        btnEditCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditCustomerActionPerformed(evt);
            }
        });

        btnDeleteCustomer.setText("Delete Selected Customer");
        btnDeleteCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteCustomerActionPerformed(evt);
            }
        });

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/applicationgui/Icons/refresh-icon-2.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        lblCustomers.setText("Customers:");

        btnReset.setText("Reset Search");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnRefresh)
                    .addComponent(scpCustomers, javax.swing.GroupLayout.PREFERRED_SIZE, 1413, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCustomers)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(lblSearchCustomer)
                                        .addGap(18, 18, 18)
                                        .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(lblViewExistingCustomers))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtSearchCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(219, 219, 219)
                        .addComponent(btnAddNewCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(58, 58, 58)
                        .addComponent(btnEditCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(btnDeleteCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblViewExistingCustomers)
                                .addGap(18, 18, 18))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnReset)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSearchCustomer)
                            .addComponent(txtSearchCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearch)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCustomers)
                .addGap(7, 7, 7)
                .addComponent(scpCustomers, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditCustomer)
                    .addComponent(btnDeleteCustomer)
                    .addComponent(btnAddNewCustomer))
                .addContainerGap(37, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshPage();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnEditCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditCustomerActionPerformed
        if (selectedCustomer != null) {
            mainPanel.add("EditExistingCustomer", new EditExistingCustomersPanel(cardLayout, mainPanel, selectedCustomer, loginData));
            cardLayout.show(mainPanel, "EditExistingCustomer");
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Customer to Edit");
        }
    }//GEN-LAST:event_btnEditCustomerActionPerformed

    private void btnAddNewCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddNewCustomerActionPerformed
        cardLayout.removeLayoutComponent(this);
        mainPanel.add("RegisterNewCustomer", new AddCustomerPanel(cardLayout, mainPanel, loginData));
        cardLayout.show(mainPanel, "RegisterNewCustomer");
    }//GEN-LAST:event_btnAddNewCustomerActionPerformed

    private void btnDeleteCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteCustomerActionPerformed
        if (selectedCustomer != null) {
            try {
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete " + selectedCustomer.getEmail() + "?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (response == JOptionPane.YES_OPTION) {
                    if (httpHandler.authenticate(loginData)[0].contains("20")) {
                        URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/customers/" + selectedCustomer.getID());
                        String httpInfo = httpHandler.sendDELETERequest(url);
                        if (httpInfo.contains("20")) {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n Customer was successfully deleted! \n"
                                    + "Please check there is no bookings for this customer ");
                            refreshPage();
                        } else {
                            JOptionPane.showMessageDialog(this, httpInfo + "\n ERROR Customer was not deleted!");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                        mainPanel.removeAll();
                        new LogIn().setVisible(true);
                    }
                }
            } catch (JSONException ex) {
                JOptionPane.showMessageDialog(this, "ERROR Customer NOT Deleted \n " + ex);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "ERROR Customer NOT Deleted \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please select a Customer to Delete");
        }
    }//GEN-LAST:event_btnDeleteCustomerActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        filteredList.clear();
        String searchCriteria = txtSearchCustomer.getText().toLowerCase().replaceAll(" ", "");
        for (Customer customer : customersList) {
            if (cbxCriteria.getSelectedItem().toString().equals("FIRST NAME")) {
                if (customer.getForename().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(customer);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("LAST NAME")) {
                if (customer.getSurname().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(customer);
                }
            } else if (searchCriteria.isEmpty() || cbxCriteria.getSelectedItem().toString().equals("ID")) {
                try {
                    if (customer.getID() == Integer.parseInt(searchCriteria)) {
                        filteredList.add(customer);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(this, "Please enter a number to search Performer ID");
                    populateCustomersTable(customersList);
                    break;
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("EMAIL")) {
                if (customer.getEmail().toLowerCase().contains(searchCriteria)) {
                    filteredList.add(customer);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("ADDRESS LINE 1")) {
                if (customer.getAddress().getAddressLine1().toLowerCase().replaceAll(" ", "").contains(searchCriteria)) {
                    filteredList.add(customer);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("CITY")) {
                if (customer.getAddress().getCity().toLowerCase().replaceAll(" ", "").contains(searchCriteria)) {
                    filteredList.add(customer);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("COUNTY")) {
                if (customer.getAddress().getCounty().toLowerCase().replaceAll(" ", "").contains(searchCriteria)) {
                    filteredList.add(customer);
                }
            } else if (cbxCriteria.getSelectedItem().toString().equals("POSTCODE")) {
                if (customer.getAddress().getPostcode().toLowerCase().replaceAll(" ", "").contains(searchCriteria)) {
                    filteredList.add(customer);
                }
            }
        }
        populateCustomersTable(filteredList);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        populateCustomersTable(customersList);
        txtSearchCustomer.setText("");
    }//GEN-LAST:event_btnResetActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddNewCustomer;
    private javax.swing.JButton btnDeleteCustomer;
    private javax.swing.JButton btnEditCustomer;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox cbxCriteria;
    private javax.swing.JLabel lblCustomers;
    private javax.swing.JLabel lblSearchCustomer;
    private javax.swing.JLabel lblViewExistingCustomers;
    private javax.swing.JScrollPane scpCustomers;
    private javax.swing.JTable tblCustomers;
    private javax.swing.JTextField txtSearchCustomer;
    // End of variables declaration//GEN-END:variables
}
