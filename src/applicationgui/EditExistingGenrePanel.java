package applicationgui;

import AdministrationApplication.EventType;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class EditExistingGenrePanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private HTTPRequestHandler httpHandler;
    private final String loginData;
    private EventType selectedGenre;

    public EditExistingGenrePanel(CardLayout layoutCard, JPanel panel, EventType genre, String data) {
        initComponents();
        cardLayout = layoutCard;
        mainPanel = panel;
        loginData = data;
        selectedGenre = genre;
        httpHandler = new HTTPRequestHandler();
        fillExistingGenreFields();
    }

    private void fillExistingGenreFields() {
        txtID.setText(String.valueOf(selectedGenre.getID()));
        txtGenreName.setText(selectedGenre.getType());
    }

    private String constructGenreJsonString(EventType genre) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("ID", genre.getID());
        j.put("NAME", genre.getType());
        return j.toString();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblAddNewPerformer = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        txtGenreName = new javax.swing.JTextField();
        btnEditGenre = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();

        lblAddNewPerformer.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblAddNewPerformer.setText("Edit Existing Genre");

        lblName.setText("Genre Name:");

        btnEditGenre.setText("Edit Genre");
        btnEditGenre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditGenreActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel1.setText("ID:");

        txtID.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(btnEditGenre, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblAddNewPerformer))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(lblName))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtGenreName, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddNewPerformer)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txtGenreName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(91, 91, 91)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditGenre)
                    .addComponent(btnCancel))
                .addGap(70, 70, 70))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEditGenreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditGenreActionPerformed
        if (!txtGenreName.getText().isEmpty()) {
            try {
                if (httpHandler.authenticate(loginData)[0].contains("20")) {
                    selectedGenre.setType(txtGenreName.getText());
                    String data = constructGenreJsonString(selectedGenre);
                    URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/event_types/" + selectedGenre.getID());
                    String httpInfo = httpHandler.sendPUTRequest(data, url);
                    if (httpInfo.contains("20")) {
                        JOptionPane.showMessageDialog(this, httpInfo + "\n \"Genre was successfully updated!\"");
                        cardLayout.removeLayoutComponent(this);
                        mainPanel.add("ViewExistingGenres", new ViewExistingGenresPanel(cardLayout, mainPanel, loginData));
                        cardLayout.show(mainPanel, "ViewExistingGenres");
                    } else {
                        JOptionPane.showMessageDialog(this, httpInfo + "\n \"ERROR Genre NOT Updated!\"");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                    mainPanel.removeAll();
                    new LogIn().setVisible(true);
                }
            } catch (MalformedURLException ex) {
                JOptionPane.showMessageDialog(this, "ERROR, Genre NOT updated \n " + ex);
            } catch (IOException | JSONException ex) {
                JOptionPane.showMessageDialog(this, "ERROR, Genre NOT updated \n " + ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please enter a Genre name for the new Genre");
        }
    }//GEN-LAST:event_btnEditGenreActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel these changes?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingGenres", new ViewExistingGenresPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingGenres");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n " + ex);
        }
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEditGenre;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblAddNewPerformer;
    private javax.swing.JLabel lblName;
    private javax.swing.JTextField txtGenreName;
    private javax.swing.JTextField txtID;
    // End of variables declaration//GEN-END:variables
}
