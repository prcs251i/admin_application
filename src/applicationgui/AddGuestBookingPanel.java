package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Act;
import AdministrationApplication.Booking;
import AdministrationApplication.Customer;
import AdministrationApplication.Event;
import AdministrationApplication.EventRun;
import AdministrationApplication.SeatedTicket;
import AdministrationApplication.StandingTicket;
import AdministrationApplication.Tier;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import AdministrationApplication.Utilities.Validator;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class AddGuestBookingPanel extends javax.swing.JPanel {

    private CardLayout cardLayout;
    private JPanel mainPanel;
    private DefaultComboBoxModel eventComboBoxModel;
    private DefaultComboBoxModel runComboBoxModel;
    private DefaultComboBoxModel tierComboBoxModel;
    private HashMap<Integer, Event> eventsHash;
    private HashMap<Integer, EventRun> runHash;
    private HashMap<Integer, Tier> tierHash;
    private List<Event> eventsList;
    private List<Event> filteredEventList;
    private EventRun selectedRun;
    private Tier selectedTier;
    private JsonClassGenerator generator;
    private final String loginData;
    private HTTPRequestHandler httpHandler;
    private Validator validator;

    public AddGuestBookingPanel(CardLayout layoutCard, JPanel panel, String data) throws IOException {
        cardLayout = layoutCard;
        mainPanel = panel;
        eventComboBoxModel = new DefaultComboBoxModel();
        runComboBoxModel = new DefaultComboBoxModel();
        tierComboBoxModel = new DefaultComboBoxModel();
        filteredEventList = new ArrayList();
        eventsHash = new HashMap<>();
        runHash = new HashMap<>();
        tierHash = new HashMap<>();
        initComponents();
        generator = new JsonClassGenerator();
        eventsList = generator.generateEvents();
        fillEventsComboBox(eventsList);
        loginData = data;
        httpHandler = new HTTPRequestHandler();
        txtPerformers.setLineWrap(true);
        validator = new Validator();
    }

    private void fillEventsComboBox(List<Event> eventsList) throws IOException {
        eventComboBoxModel.removeAllElements();
        for (Event event : eventsList) {
            eventsHash.put(event.getID(), event);
            eventComboBoxModel.addElement(event.getID() + " - " + event.getEventTitle());
        }
    }

    private String constructBookingJsonString(Booking newBooking) {
        JSONObject j = new JSONObject();
        j.put("runID", newBooking.getRun().getID());
        j.put("forename", newBooking.getCustomer().getForename());
        j.put("surname", newBooking.getCustomer().getForename());
        j.put("email", newBooking.getCustomer().getEmail());
        j.put("tierID", selectedTier.getTierId());
        j.put("quantity", newBooking.getNoOfTickets());
        j.put("totalCost", newBooking.getTotalCost());
        return j.toString();
    }

    private void refresh() {
        try {
            cardLayout.removeLayoutComponent(this);
            mainPanel.add("AddGuestBooking", new AddGuestBookingPanel(cardLayout, mainPanel, loginData));
            cardLayout.show(mainPanel, "AddGuestBooking");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        spnQuantity = new javax.swing.JSpinner();
        cbxEvent = new javax.swing.JComboBox();
        cbxTier = new javax.swing.JComboBox();
        lblTierType = new javax.swing.JLabel();
        lblRun = new javax.swing.JLabel();
        lblEvent = new javax.swing.JLabel();
        lblQuantity = new javax.swing.JLabel();
        lblTotalPrice = new javax.swing.JLabel();
        txtCost = new javax.swing.JTextField();
        lblTier = new javax.swing.JLabel();
        txtSearchEvent = new javax.swing.JTextField();
        cbxRun = new javax.swing.JComboBox();
        pnlRunDetail = new javax.swing.JPanel();
        txtVenue = new javax.swing.JTextField();
        lblVenue = new javax.swing.JLabel();
        lblPerformances = new javax.swing.JLabel();
        scpPerformances = new javax.swing.JScrollPane();
        txtPerformers = new javax.swing.JTextArea();
        lblCreateBooking = new javax.swing.JLabel();
        lblEmail = new javax.swing.JLabel();
        txtGuestEmail = new javax.swing.JTextField();
        lblFirstName = new javax.swing.JLabel();
        lblLastName = new javax.swing.JLabel();
        txtFirstName = new javax.swing.JTextField();
        txtLastName = new javax.swing.JTextField();
        lblCardType = new javax.swing.JLabel();
        cbxCardType = new javax.swing.JComboBox();
        txtCardNo = new javax.swing.JTextField();
        lblCardNo = new javax.swing.JLabel();
        txtExpDate = new javax.swing.JLabel();
        dtpExpiryDate = new org.jdesktop.swingx.JXDatePicker();
        txtSecCode = new javax.swing.JTextField();
        lblSecCode = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnMakeBooking = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblConfirmEmailAddress = new javax.swing.JLabel();
        txtConfirmEmailAddress = new javax.swing.JTextField();
        btnEventSearch1 = new javax.swing.JButton();
        cbxEventCriteria = new javax.swing.JComboBox();
        btnResetEventSearch = new javax.swing.JButton();

        spnQuantity.setModel(new javax.swing.SpinnerNumberModel(0, 0, 9, 1));
        spnQuantity.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spnQuantityStateChanged(evt);
            }
        });

        cbxEvent.setModel(eventComboBoxModel);
        cbxEvent.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxEventItemStateChanged(evt);
            }
        });

        cbxTier.setModel(tierComboBoxModel);
        cbxTier.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxTierItemStateChanged(evt);
            }
        });

        lblTierType.setText("This Tier is Seated/Standing");

        lblRun.setText("Run:");

        lblEvent.setText("Event:");

        lblQuantity.setText("Quantity:");

        lblTotalPrice.setText("Total Cost:");

        txtCost.setEditable(false);
        txtCost.setBackground(new java.awt.Color(238, 238, 238));
        txtCost.setText("£0.00");

        lblTier.setText("Tier:");

        cbxRun.setModel(runComboBoxModel);
        cbxRun.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxRunItemStateChanged(evt);
            }
        });

        pnlRunDetail.setBorder(javax.swing.BorderFactory.createTitledBorder("Run Detail"));

        txtVenue.setEditable(false);
        txtVenue.setBackground(new java.awt.Color(238, 238, 238));

        lblVenue.setText("Venue:");

        lblPerformances.setText("Performances:");

        txtPerformers.setBackground(new java.awt.Color(238, 238, 238));
        txtPerformers.setColumns(20);
        txtPerformers.setRows(5);
        scpPerformances.setViewportView(txtPerformers);

        javax.swing.GroupLayout pnlRunDetailLayout = new javax.swing.GroupLayout(pnlRunDetail);
        pnlRunDetail.setLayout(pnlRunDetailLayout);
        pnlRunDetailLayout.setHorizontalGroup(
            pnlRunDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRunDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblVenue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblPerformances)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scpPerformances, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlRunDetailLayout.setVerticalGroup(
            pnlRunDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRunDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRunDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRunDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtVenue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblVenue)
                        .addComponent(lblPerformances))
                    .addComponent(scpPerformances, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(9, Short.MAX_VALUE))
        );

        lblCreateBooking.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCreateBooking.setText("Create Guest Booking");

        lblEmail.setText("Email Address:");

        lblFirstName.setText("First Name:");

        lblLastName.setText("Last Name:");

        lblCardType.setText("Card Type:");

        cbxCardType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Visa ", "MasterCard", "American Express" }));

        lblCardNo.setText("Card Number:");

        txtExpDate.setText("Expiry Date:");

        lblSecCode.setText("Security Code:");

        jLabel4.setText("Personal Information:");

        jLabel5.setText("Payment Information:");

        btnMakeBooking.setText("Make Booking");
        btnMakeBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMakeBookingActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblConfirmEmailAddress.setText("Confirm Email Address:");

        btnEventSearch1.setText("Search");
        btnEventSearch1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEventSearch1ActionPerformed(evt);
            }
        });

        cbxEventCriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ID", "EVENT TITLE" }));

        btnResetEventSearch.setText("Reset Search");
        btnResetEventSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetEventSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblQuantity)
                            .addComponent(lblTier))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbxTier, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(lblTierType))
                            .addComponent(pnlRunDetail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnMakeBooking, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(spnQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(51, 51, 51)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblTotalPrice)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(88, 88, 88)
                                                .addComponent(txtCost, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(92, 92, 92)
                                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbxEvent, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbxRun, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblEvent)
                                    .addComponent(lblRun))))
                        .addGap(18, 18, 18)
                        .addComponent(txtSearchEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbxEventCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnEventSearch1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(btnResetEventSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 72, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(lblConfirmEmailAddress)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtConfirmEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(56, 56, 56)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(lblEmail)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(lblLastName)
                                                .addComponent(lblFirstName)))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtGuestEmail)
                                            .addComponent(txtFirstName)
                                            .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(83, 83, 83)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblCardNo)
                                    .addComponent(txtExpDate)
                                    .addComponent(lblSecCode)
                                    .addComponent(lblCardType)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(222, 222, 222)
                                .addComponent(jLabel4)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCardNo, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxCardType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSecCode, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dtpExpiryDate, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jLabel5))))
                    .addComponent(lblCreateBooking))
                .addGap(123, 123, 123))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(lblCreateBooking)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbxCardType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblCardType))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCardNo)
                            .addComponent(txtCardNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtExpDate)
                            .addComponent(dtpExpiryDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSecCode)
                            .addComponent(txtSecCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblFirstName)
                            .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblLastName)
                            .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblEmail)
                            .addComponent(txtGuestEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtConfirmEmailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblConfirmEmailAddress))))
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblEvent)
                            .addComponent(cbxEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSearchEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblRun)
                            .addComponent(cbxRun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pnlRunDetail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTier)
                            .addComponent(cbxTier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTierType))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblTotalPrice)
                                .addComponent(txtCost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(spnQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnMakeBooking)
                            .addComponent(btnCancel))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbxEventCriteria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnEventSearch1))
                        .addGap(6, 6, 6)
                        .addComponent(btnResetEventSearch)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void spnQuantityStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnQuantityStateChanged
        txtCost.setText("£0.00");
        double totalCost = (int) spnQuantity.getValue() * selectedTier.getTierTicketCost().getPrice();
        txtCost.setText("£" + totalCost);
    }//GEN-LAST:event_spnQuantityStateChanged

    private void cbxEventItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxEventItemStateChanged
        lblTierType.setText("This Tier is Seated");
        txtPerformers.setText("");
        txtVenue.setText("");
        spnQuantity.setValue(0);
        runComboBoxModel.removeAllElements();
        tierComboBoxModel.removeAllElements();
        runHash.clear();
        if (eventComboBoxModel.getSize() != 0) {
            String selectedEventString = cbxEvent.getSelectedItem().toString();
            int eventId = Integer.parseInt(selectedEventString.split(" - ", 2)[0]);
            Event selectedEvent = eventsHash.get(eventId);
            for (EventRun run : selectedEvent.getEventRuns()) {
                runHash.put(run.getID(), run);
                runComboBoxModel.addElement(run.getID() + " - " + run.getRunDate());
            }
        }
        if (runComboBoxModel.getSize() == 0) {
            runComboBoxModel.addElement("Sorry there are currently no Runs for this Event");
            spnQuantity.setEnabled(false);
            cbxTier.setEnabled(false);
        }
    }//GEN-LAST:event_cbxEventItemStateChanged

    private void cbxTierItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxTierItemStateChanged
        spnQuantity.setValue(0);
        if (cbxTier.getSelectedIndex() != -1) {
            String selectedTierString = cbxTier.getSelectedItem().toString();
            int tierId = Integer.parseInt(selectedTierString.split(" - ", 2)[0]);
            selectedTier = tierHash.get(tierId);
            if (selectedTier.getIsSeated() == true) {
                lblTierType.setText("This Tier is Seated");
            } else {
                lblTierType.setText("This Tier is Seated");
            }
        }
    }//GEN-LAST:event_cbxTierItemStateChanged

    private void cbxRunItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxRunItemStateChanged
        lblTierType.setText("This Tier is Seated");
        tierComboBoxModel.removeAllElements();
        txtPerformers.setText("");
        txtVenue.setText("");
        cbxTier.setEnabled(true);
        spnQuantity.setEnabled(true);
        spnQuantity.setValue(0);
        if (runComboBoxModel.getSize() != 0) {
            if (!runComboBoxModel.getElementAt(0).toString().equals("Sorry there are currently no Runs for this Event")) {
                String selectedRunString = cbxRun.getSelectedItem().toString();
                int runId = Integer.parseInt(selectedRunString.split(" - ", 2)[0]);
                selectedRun = runHash.get(runId);
                txtVenue.setText(selectedRun.getVenue().getVenueName());
                for (Act act : selectedRun.getActsPerforming()) {
                    txtPerformers.append(act.getActName() + ", ");
                }
                for (Tier tier : selectedRun.getVenue().getTierList()) {
                    tierHash.put(tier.getTierId(), tier);
                    if (tier.getTierTicketCost() != null) {
                        tierComboBoxModel.addElement(tier.getTierId() + " - " + tier.getTierName() + " @ £" + tier.getTierTicketCost().getPrice());
                    }
                }
            }
        }
    }//GEN-LAST:event_cbxRunItemStateChanged

    private void btnMakeBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMakeBookingActionPerformed
        if (!txtFirstName.getText().isEmpty()) {
            if (!txtLastName.getText().isEmpty()) {
                if (!txtGuestEmail.getText().isEmpty()) {
                    if (validator.validateEmail(txtGuestEmail.getText()) == true) {
                        if (!txtConfirmEmailAddress.getText().isEmpty() && txtConfirmEmailAddress.getText().equals(txtGuestEmail.getText())) {
                            if (cbxEvent.getSelectedIndex() != -1) {
                                if (cbxRun.getSelectedIndex() != -1) {
                                    if (cbxTier.getSelectedIndex() != -1) {
                                        if ((int) spnQuantity.getValue() > 0 && (int) spnQuantity.getValue() < 10) {
                                            if (cbxCardType.getSelectedIndex() != -1) {
                                                if (!txtCardNo.getText().isEmpty()) {
                                                    if (validator.validateCardNumber(txtCardNo.getText()) == true) {
                                                        if (dtpExpiryDate.getDate() != null) {
                                                            if (!txtSecCode.getText().isEmpty()) {
                                                                if (validator.validateSecurityNo(txtSecCode.getText()) == true) {
                                                                    try {
                                                                        if (httpHandler.authenticate(loginData)[0].contains("20")) {
                                                                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                                                                            Date date = new Date();
                                                                            double cost = Double.parseDouble(txtCost.getText().replace("£", ""));
                                                                            Customer newCustomer = new Customer(txtFirstName.getText(), txtLastName.getText(), txtGuestEmail.getText(),
                                                                                    cbxCardType.getSelectedItem().toString(), txtCardNo.getText(), dtpExpiryDate.getDate(), txtSecCode.getText());
                                                                            Booking newBooking = new Booking(selectedRun, newCustomer, date, (int) spnQuantity.getValue(), cost);
                                                                            String data = constructBookingJsonString(newBooking);
                                                                            URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/bookings/CreateGuestBooking");
                                                                            String[] httpInfo = httpHandler.sendPOSTRequest(data, url);
                                                                            if (httpInfo[0].contains("20")) {

                                                                                JSONObject booking = new JSONObject(httpInfo[1]);

                                                                                List<String> ticketDetails = new ArrayList();
                                                                                for (int i = 0; i < booking.getJSONArray("TICKETs").length(); i++) {
                                                                                    JSONObject ticketsObject = booking.getJSONArray("TICKETs").getJSONObject(i);
                                                                                    if (selectedTier.getIsSeated() == true) {
                                                                                        SeatedTicket ticket = new SeatedTicket();
                                                                                        ticket.setTicketRef(ticketsObject.getInt("ID"));
                                                                                        try {
                                                                                            ticket.setBookingID(ticketsObject.getInt("GUEST_BOOKING_ID"));
                                                                                        } catch (JSONException ex) {
                                                                                        }
                                                                                        ticket.setAllocatedTier(selectedTier);
                                                                                        ticket.setSeatRow(ticketsObject.getString("SEAT_ROW"));
                                                                                        ticket.setSeatNo(ticketsObject.getInt("SEAT_NUM"));
                                                                                        ticketDetails.add("Ticket Ref: " + ticket.getTicketRef() + ", Seat Row: " + ticket.getSeatRow()
                                                                                                + ", Seat Number: " + ticket.getSeatNo() + ", in Tier: " + ticket.getAllocatedTier().getTierName() + "\n");
                                                                                    } else {
                                                                                        StandingTicket ticket = new StandingTicket();
                                                                                        ticket.setTicketRef(ticketsObject.getInt("ID"));
                                                                                        try {
                                                                                            ticket.setBookingID(ticketsObject.getInt("BOOKING_ID"));
                                                                                        } catch (JSONException ex) {
                                                                                        }
                                                                                        ticket.setAllocatedTier(selectedTier);
                                                                                        ticketDetails.add("Ticket Ref: " + ticket.getTicketRef()
                                                                                                + " in Tier: " + ticket.getAllocatedTier().getTierName() + "\n");
                                                                                    }
                                                                                }
                                                                                StringBuilder ticketOverview = new StringBuilder();
                                                                                for (String ticket : ticketDetails) {
                                                                                    ticketOverview.append(ticket);
                                                                                }
                                                                                JOptionPane.showMessageDialog(this, ticketOverview);
                                                                                int confirmationMessage = JOptionPane.showConfirmDialog(this, httpInfo[0]
                                                                                        + "Booking successfully Made, Would you like to make another? ", "Booking Successful", JOptionPane.OK_CANCEL_OPTION);
                                                                                if (confirmationMessage == JOptionPane.YES_OPTION) {
                                                                                    refresh();
                                                                                } else {
                                                                                    cardLayout.removeLayoutComponent(this);
                                                                                    mainPanel.add("ViewGuestBookings", new ViewGuestBookingsPanel(cardLayout, mainPanel, loginData));
                                                                                    cardLayout.show(mainPanel, "ViewGuestBookings");
                                                                                }

                                                                            } else {
                                                                                JOptionPane.showMessageDialog(this, httpInfo[0] + "\n Booking NOT added");
                                                                            }
                                                                        } else {
                                                                            JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                                                                            mainPanel.removeAll();
                                                                            new LogIn().setVisible(true);
                                                                        }
                                                                    } catch (IOException ex) {
                                                                        JOptionPane.showMessageDialog(this, "ERROR Booking NOT Made \n" + ex);
                                                                    }
                                                                } else {
                                                                    JOptionPane.showMessageDialog(this, "Please enter a valid Payment Card Security Code for the new Customer");
                                                                }
                                                            } else {
                                                                JOptionPane.showMessageDialog(this, "Please enter a Payment Card Security Code for the new Customer");
                                                            }
                                                        } else {
                                                            JOptionPane.showMessageDialog(this, "Please select an Payment Card Expiry Date for the new Customer");
                                                        }
                                                    } else {
                                                        JOptionPane.showMessageDialog(this, "Please enter a valid Payment Card Number for the new Customer");
                                                    }
                                                } else {
                                                    JOptionPane.showMessageDialog(this, "Please enter a Payment Card Number for the new Customer");
                                                }
                                            } else {
                                                JOptionPane.showMessageDialog(this, "Please select a Payment Card Type for the new Customer");
                                            }
                                        } else {
                                            JOptionPane.showMessageDialog(this, "Must have a ticket quantity between 1 and 9");
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(this, "Please select a Tier for the new Booking");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(this, "Please select a Run for the new Booking");
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Please select an Event for the new Booking");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "The confirmation email you have entered does not match");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Please enter a valid email address");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please enter an Email Address for the new Customer");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please enter a Surname for the new Customer");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please enter a Forename for the new Customer");
        }

    }//GEN-LAST:event_btnMakeBookingActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        try {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel?", "WARNING",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewGuestBookings", new ViewGuestBookingsPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewGuestBookings");
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEventSearch1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEventSearch1ActionPerformed
        try {
            filteredEventList.clear();
            String searchCriteria = txtSearchEvent.getText().toLowerCase();
            for (Event event : eventsList) {
                if (cbxEventCriteria.getSelectedItem().toString().equals("ID")) {
                    if (searchCriteria.isEmpty() || event.getID() == Integer.parseInt(searchCriteria)) {
                        filteredEventList.add(event);
                    }
                } else if (cbxEventCriteria.getSelectedItem().toString().equals("EVENT TITLE")) {
                    if (event.getEventTitle().toLowerCase().contains(searchCriteria)) {
                        filteredEventList.add(event);
                    }
                }
            }
            fillEventsComboBox(filteredEventList);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnEventSearch1ActionPerformed

    private void btnResetEventSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetEventSearchActionPerformed
        try {
            fillEventsComboBox(eventsList);
            txtSearchEvent.setText("");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
        }
    }//GEN-LAST:event_btnResetEventSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEventSearch1;
    private javax.swing.JButton btnMakeBooking;
    private javax.swing.JButton btnResetEventSearch;
    private javax.swing.JComboBox cbxCardType;
    private javax.swing.JComboBox cbxEvent;
    private javax.swing.JComboBox cbxEventCriteria;
    private javax.swing.JComboBox cbxRun;
    private javax.swing.JComboBox cbxTier;
    private org.jdesktop.swingx.JXDatePicker dtpExpiryDate;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel lblCardNo;
    private javax.swing.JLabel lblCardType;
    private javax.swing.JLabel lblConfirmEmailAddress;
    private javax.swing.JLabel lblCreateBooking;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblEvent;
    private javax.swing.JLabel lblFirstName;
    private javax.swing.JLabel lblLastName;
    private javax.swing.JLabel lblPerformances;
    private javax.swing.JLabel lblQuantity;
    private javax.swing.JLabel lblRun;
    private javax.swing.JLabel lblSecCode;
    private javax.swing.JLabel lblTier;
    private javax.swing.JLabel lblTierType;
    private javax.swing.JLabel lblTotalPrice;
    private javax.swing.JLabel lblVenue;
    private javax.swing.JPanel pnlRunDetail;
    private javax.swing.JScrollPane scpPerformances;
    private javax.swing.JSpinner spnQuantity;
    private javax.swing.JTextField txtCardNo;
    private javax.swing.JTextField txtConfirmEmailAddress;
    private javax.swing.JTextField txtCost;
    private javax.swing.JLabel txtExpDate;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtGuestEmail;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextArea txtPerformers;
    private javax.swing.JTextField txtSearchEvent;
    private javax.swing.JTextField txtSecCode;
    private javax.swing.JTextField txtVenue;
    // End of variables declaration//GEN-END:variables
}
