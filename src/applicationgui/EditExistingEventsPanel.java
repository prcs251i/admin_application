package applicationgui;

import AdministrationApplication.Utilities.JsonClassGenerator;
import AdministrationApplication.Event;
import AdministrationApplication.EventType;
import AdministrationApplication.Utilities.HTTPRequestHandler;
import java.awt.CardLayout;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Joel Gooch
 */
public class EditExistingEventsPanel extends javax.swing.JPanel {

    private JsonClassGenerator generator = new JsonClassGenerator();
    private HTTPRequestHandler httpHandler;
    private DefaultComboBoxModel eventTypesComboModel = new DefaultComboBoxModel();
    private CardLayout cardLayout;
    private JPanel mainPanel;
    private int totalRuns;
    private Event selectedEvent;
    private final String loginData;
    private HashMap<String, EventType> eventTypesHashMap;

    /**
     * Creates new form EditExistingEventsPanel
     */
    public EditExistingEventsPanel(CardLayout layoutCard, JPanel panel, Event event, String data) throws IOException, JSONException {
        selectedEvent = event;
        initComponents();
        httpHandler = new HTTPRequestHandler();
        eventTypesHashMap = new HashMap<>();
        cardLayout = layoutCard;
        mainPanel = panel;
        fillEventTypeComboBox();
        fillExistingEventFields();
        loginData = data;

    }

    private void fillExistingEventFields() {
        txtID.setText(String.valueOf(selectedEvent.getID()));
        txtEventTitle.setText(selectedEvent.getEventTitle());
        txtEventDesc.setText(selectedEvent.getEventDescription());
        cbxEventType.setSelectedItem(selectedEvent.getEventType().getType());
        cbxAgeRating.setSelectedItem(selectedEvent.getAgeRating());
        txtImageURL.setText(selectedEvent.getImageString());
    }

    private void fillEventTypeComboBox() throws IOException, JSONException {
        List<EventType> eventTypes = generator.generateEventTypes();
        for (EventType type : eventTypes) {
            eventTypesHashMap.put(type.getType(), type);
            eventTypesComboModel.addElement(type.getType());
        }
    }

    private String constructEventJsonString(Event newEvent) throws JSONException {
        JSONObject j = new JSONObject();
        j.put("ID", newEvent.getID());
        j.put("EVENT_TYPE", newEvent.getEventType().getID());
        j.put("TITLE", newEvent.getEventTitle());
        j.put("AGE_RATING", newEvent.getAgeRating());
        j.put("DESCRIPTION", newEvent.getEventDescription());
        j.put("IMAGE", newEvent.getImageString());
        return j.toString();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblAddNewVenue = new javax.swing.JLabel();
        pnlEventDetails = new javax.swing.JPanel();
        lblEventTitle = new javax.swing.JLabel();
        txtEventTitle = new javax.swing.JTextField();
        lblDescription = new javax.swing.JLabel();
        scpDesc = new javax.swing.JScrollPane();
        txtEventDesc = new javax.swing.JTextArea();
        lblEventType = new javax.swing.JLabel();
        lblAgeRating = new javax.swing.JLabel();
        cbxAgeRating = new javax.swing.JComboBox();
        lblImageRating = new javax.swing.JLabel();
        txtImageURL = new javax.swing.JTextField();
        cbxEventType = new javax.swing.JComboBox();
        btnUpdateEvent = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        lblID = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        lblEventDetails = new javax.swing.JLabel();

        lblAddNewVenue.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblAddNewVenue.setText("Edit Existing Event");

        lblEventTitle.setText("Title:");

        lblDescription.setText("Description:");

        txtEventDesc.setColumns(20);
        txtEventDesc.setRows(5);
        scpDesc.setViewportView(txtEventDesc);

        lblEventType.setText("Event Type:");

        lblAgeRating.setText("Age Rating:");

        cbxAgeRating.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "PG", "U", "12A", "15", "18+" }));

        lblImageRating.setText("Image URL:");

        cbxEventType.setModel(eventTypesComboModel);

        btnUpdateEvent.setText("Update Event");
        btnUpdateEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateEventActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblID.setText("ID:");

        txtID.setEditable(false);

        javax.swing.GroupLayout pnlEventDetailsLayout = new javax.swing.GroupLayout(pnlEventDetails);
        pnlEventDetails.setLayout(pnlEventDetailsLayout);
        pnlEventDetailsLayout.setHorizontalGroup(
            pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblID)
                                    .addComponent(lblEventTitle))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtEventTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                                .addComponent(lblDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(scpDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(39, 39, 39)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblImageRating)
                            .addComponent(lblAgeRating)
                            .addComponent(lblEventType))
                        .addGap(18, 18, 18)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbxEventType, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbxAgeRating, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtImageURL, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addGap(316, 316, 316)
                        .addComponent(btnUpdateEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 1039, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        pnlEventDetailsLayout.setVerticalGroup(
            pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEventDetailsLayout.createSequentialGroup()
                .addContainerGap(10, Short.MAX_VALUE)
                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblID)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblEventTitle)
                            .addComponent(txtEventTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDescription)
                            .addComponent(scpDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlEventDetailsLayout.createSequentialGroup()
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbxEventType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblEventType))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAgeRating)
                            .addComponent(cbxAgeRating, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblImageRating)
                            .addComponent(txtImageURL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(19, 19, 19)
                .addGroup(pnlEventDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdateEvent)
                    .addComponent(btnCancel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        lblEventDetails.setText("Event Details");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlEventDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblEventDetails)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(lblAddNewVenue)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddNewVenue)
                .addGap(12, 12, 12)
                .addComponent(lblEventDetails)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlEventDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(58, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateEventActionPerformed
        try {
            if (!txtEventTitle.getText().isEmpty()) {
                if (!txtEventDesc.getText().isEmpty()) {
                    if (cbxEventType.getSelectedIndex() != -1) {
                        if (cbxAgeRating.getSelectedIndex() != -1) {
                            if (!txtImageURL.getText().isEmpty()) {
                                if (httpHandler.authenticate(loginData)[0].contains("20")) {
                                    selectedEvent.setEventTitle(txtEventTitle.getText());
                                    selectedEvent.setEventDescription(txtEventDesc.getText());
                                    EventType type = eventTypesHashMap.get(cbxEventType.getSelectedItem().toString());
                                    selectedEvent.setEventType(type);
                                    selectedEvent.setAgeRating(cbxAgeRating.getSelectedItem().toString());
                                    selectedEvent.setImageString(txtImageURL.getText());
                                    String data = constructEventJsonString(selectedEvent);
                                    URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251I/api/Events/" + selectedEvent.getID());
                                    String httpInfo = httpHandler.sendPUTRequest(data, url);
                                    if (httpInfo.contains("20")) {
                                        JOptionPane.showMessageDialog(this, httpInfo + "\n Event Updated");
                                        cardLayout.removeLayoutComponent(this);
                                        mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                                        cardLayout.show(mainPanel, "ViewExistingEvents");
                                    } else {
                                        JOptionPane.showMessageDialog(this, httpInfo + "\n ERROR, Event NOT Updated");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(this, "Authentication FAILED, returning to Log In Page");
                                    mainPanel.removeAll();
                                    new LogIn().setVisible(true);
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Please enter an image URL for the new Event");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Please select an age rating for the new Event");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Please select an event type for the new Event");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please select a description for the new Event");
                }
            } else {
                JOptionPane.showMessageDialog(this, "Please select a title for the new Event");
            }
        } catch (IOException | JSONException ex) {
            JOptionPane.showMessageDialog(this, "ERROR, Event NOT updated \n " + ex);
        }
    }//GEN-LAST:event_btnUpdateEventActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to cancel these changes?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            try {
                cardLayout.removeLayoutComponent(this);
                mainPanel.add("ViewExistingEvents", new ViewExistingEventsPanel(cardLayout, mainPanel, loginData));
                cardLayout.show(mainPanel, "ViewExistingEvents");
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "ERROR \n" + ex);
            }
        }
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnUpdateEvent;
    private javax.swing.JComboBox cbxAgeRating;
    private javax.swing.JComboBox cbxEventType;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblAddNewVenue;
    private javax.swing.JLabel lblAgeRating;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblEventDetails;
    private javax.swing.JLabel lblEventTitle;
    private javax.swing.JLabel lblEventType;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblImageRating;
    private javax.swing.JPanel pnlEventDetails;
    private javax.swing.JScrollPane scpDesc;
    private javax.swing.JTextArea txtEventDesc;
    private javax.swing.JTextField txtEventTitle;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtImageURL;
    // End of variables declaration//GEN-END:variables
}
